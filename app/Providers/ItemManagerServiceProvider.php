<?php

namespace App\Providers;

use App\Services\BrainManagerService;
use App\Services\ItemManagerService;
use Illuminate\Support\ServiceProvider;

class ItemManagerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->singleton( 'item_manager', function ($app) {
            return new ItemManagerService();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
