<?php

namespace App\Providers;

use App\Services\BrainManagerService;
use App\Services\ClassificationManagerService;
use Illuminate\Support\ServiceProvider;

class ClassificationManagerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->singleton( 'classification_manager', function ($app) {
            return new ClassificationManagerService();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
