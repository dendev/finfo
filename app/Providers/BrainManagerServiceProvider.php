<?php

namespace App\Providers;

use App\Services\BrainManagerService;
use Illuminate\Support\ServiceProvider;

class BrainManagerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->singleton( 'brain_manager', function ($app) {
            return new BrainManagerService();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
