<?php

namespace App\Providers;

use App\Services\BrainManagerService;
use App\Services\QuizManagerService;
use Illuminate\Support\ServiceProvider;

class QuizManagerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->singleton( 'quiz_manager', function ($app) {
            return new QuizManagerService();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
