<?php

namespace App\Providers;

use App\Services\BrainManagerService;
use App\Services\ItemExtraManagerService;
use Illuminate\Support\ServiceProvider;

class ItemExtraManagerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->singleton( 'item_extra_manager', function ($app) {
            return new ItemExtraManagerService();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
