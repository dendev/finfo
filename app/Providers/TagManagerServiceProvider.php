<?php

namespace App\Providers;

use App\Services\BrainManagerService;
use App\Services\TagManagerService;
use Illuminate\Support\ServiceProvider;

class TagManagerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->singleton( 'tag_manager', function ($app) {
            return new TagManagerService();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
