<?php

namespace App\Providers;

use App\Services\BrainManagerService;
use App\Services\ItemTagManagerService;
use Illuminate\Support\ServiceProvider;

class ItemTagManagerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->singleton( 'item_tag_manager', function ($app) {
            return new ItemTagManagerService();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
