<?php

namespace App\Providers;

use App\Services\BrainManagerService;
use App\Services\NameManagerService;
use Illuminate\Support\ServiceProvider;

class NameManagerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->singleton( 'name_manager', function ($app) {
            return new NameManagerService();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
