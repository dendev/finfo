<?php

namespace App\Providers;

use App\Services\VocabularyManagerService;
use Illuminate\Support\ServiceProvider;

class VocabularyManagerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->singleton( 'vocabulary_manager', function ($app) {
            return new VocabularyManagerService();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
