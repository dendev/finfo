<?php

namespace App\Providers;

use App\Services\ComponentManagerService;
use Illuminate\Support\ServiceProvider;

class ComponentManagerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->singleton( 'component_manager', function ($app) {
            return new ComponentManagerService();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
