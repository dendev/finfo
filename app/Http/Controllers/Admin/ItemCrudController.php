<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ItemRequest;
use App\Models\Item;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Support\Facades\Request;

/**
 * Class ItemCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ItemCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation { store as storeTrait;}
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Item::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/item');
        CRUD::setEntityNameStrings('Item', 'Items');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::addColumn([
            'label' => 'Nom',
            'name' => 'label',
            'type' => 'text',
        ]);

        CRUD::addColumn([
            'label' => 'Type',
            'name' => 'type',
            'type' => 'relationship',
            'attribute' => 'label',
        ]);
    }

    protected function setupShowOperation()
    {
        $this->crud->set('show.setFromDb', false);

        CRUD::addColumn([
            'label' => 'Nom',
            'name' => 'label',
            'type' => 'text',
        ]);

        CRUD::addColumn([
            'label' => 'Autres noms',
            'name' => 'names',
            'type' => 'names',
        ]);

        CRUD::addColumn([
            'label' => 'Type',
            'name' => 'type',
            'type' => 'relationship',
            'attribute' => 'label',
        ]);

        CRUD::addColumn([
            'label' => 'Classification',
            'name' => 'classification',
            'type' => 'classifications',
        ]);

        CRUD::addColumn([
            'label' => 'Images',
            'name' => 'images',
            'type' => 'images',
        ]);

        CRUD::addColumn([
            'label' => 'Extras',
            'name' => 'extras',
            'type' => 'extras',
        ]);
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(ItemRequest::class);

        CRUD::addField([
            'name' => 'label',
            'label' => 'Nom',
            'type' => 'text',
        ]);

        CRUD::addField([
            'name' => 'names',
            'label' => 'Autres noms',
            'type' => 'repeatable',
            'fields' => [
                [
                    'name'    => 'label',
                    'type'    => 'text',
                    'label'   => 'Nom',
                    'wrapper' => ['class' => 'form-group col-12'],
                ],
            ],
            'new_item_label'  => 'Autre', // customize the text of the button
        ]);


        CRUD::addField([
            'name' => 'images',
            'label' => 'Images',
            'type' => 'repeatable',
            'fields' => [
                [
                    'name'    => 'image',
                    'type'    => 'text',
                    'label'   => 'Image',
                    'wrapper' => ['class' => 'form-group col-12'],
                ],
            ],
            'new_item_label'  => 'Autre', // customize the text of the button
        ]);

        CRUD::addField([
            'name' => 'description',
            'label' => 'Description',
            'type' => 'wysiwyg',
        ]);

        CRUD::addField([
            'name' => 'classification_id', // field name or relation name !?
            'label' => 'Classification',
            'type' => 'relationship',
            'attribute' => 'label',
            'entity' => 'classification',
            'model' => 'App\Models\Classification',
        ]);

    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    public function store()
    {
        $this->crud->hasAccessOrFail('create');
        $request = $this->crud->validateRequest();

        // get all datas
        $datas = $request->all();

        // get embeded datas
        $names = json_decode($datas['names'], true, 512, JSON_OBJECT_AS_ARRAY);
        $images = json_decode($datas['images'], true, 512, JSON_OBJECT_AS_ARRAY);
        // TODO parse text to find vocalbularies && update item_vocabularies

        // clean datas
        unset($datas['names']);
        unset($datas['images']);
        unset($datas['_token']);
        unset($datas['http_referrer']);
        unset($datas['save_action']);

        // debug
        // dd($datas);

        // save item
        $item = new Item($datas);
        $item->save();

        // save names

        // TODO

        // saves images
        // TODO

        // insert item in the db
        //$item = $this->crud->create($this->crud->getStrippedSaveRequest());
        //$plant =
        $this->data['entry'] = $this->crud->entry = $item;


        // show a success message
        \Alert::success(trans('backpack::crud.insert_success'))->flash();

        // save the redirect choice for next time
        $this->crud->setSaveAction();

        return $this->crud->performSaveAction($item->getKey());
    }
}
