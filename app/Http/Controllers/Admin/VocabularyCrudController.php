<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\VocabularyRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class VocabularyCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class VocabularyCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Vocabulary::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/vocabulary');
        CRUD::setEntityNameStrings('vocabulary', 'vocabularies');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
         /*
          // too long
        CRUD::addColumn([
            'label' => 'Description',
            'name' => 'description_html_link',
            'type' => 'textarea',
            'limit' => 110
        ]);

         // !? break display !?
        CRUD::addColumn([
            'label' => 'Type',
            'name' => 'label',
            'type' => 'model_function',
            'function_name' => 'type_label',
        ]);
         */

        CRUD::addColumn([
            'label' => 'Label',
            'name' => 'label',
            'type' => 'text'
        ]);
    }

    protected function setupShowOperation()
    {
        $this->crud->set('show.setFromDb', false);

        /*
        CRUD::addColumn([
            'label' => 'Label',
            'name' => 'label',
            'type' => 'text'
        ]);

        CRUD::addColumn([
            'label' => 'Description',
            'name' => 'description',
            'type' => 'text'
        ]);

        CRUD::addColumn([
            'label' => 'Image',
            'name' => 'image',
            'type' => 'text'
        ]);
        */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(VocabularyRequest::class);

        CRUD::addField([
            'label' => 'Label',
            'name' => 'label',
            'type' => 'text'
        ]);

        CRUD::addField([
            'label' => 'Description',
            'name' => 'description',
            'type' => 'textarea'
        ]);

        CRUD::addField([
            'label' => 'Image',
            'name' => 'image',
            'type' => 'image',
            'crop' => true,
        ]);
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
