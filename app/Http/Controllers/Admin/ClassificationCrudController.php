<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ClassificationRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use GuzzleHttp\Psr7\Request;

/**
 * Class ClassificationCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ClassificationCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Classification::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/classification');
        CRUD::setEntityNameStrings('classification', 'classifications');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::addColumn('label');

        CRUD::addColumn('slug');

        CRUD::addColumn([
            'label' => 'Parent',
            'name' => 'parent',
            'type' => 'relationship',
            'attribute' => 'label',
        ]);

        CRUD::addColumn([
            'label' => 'Taxon',
            'name' => 'level_fr',
            'type' => 'text'
        ]);

        CRUD::addColumn([   // select_multiple: n-n relationship (with pivot table)
            'label'     => 'Fiches', // Table column heading
            'type'      => 'relationship_count',
            'name'      => 'items', // the method that defines the relationship in your Model
            'wrapper'   => [
                'href' => function ($crud, $column, $entry, $related_key) {
                    return backpack_url('items?classification_id='.$entry->getKey());
                },
            ],
        ]);
    }


    protected function setupShowOperation()
    {
        $this->crud->set('show.setFromDb', false);

        //$this->setupListOperation();

    //    CRUD::addColumn('created_at');
     //   CRUD::addColumn('updated_at');
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {

        CRUD::setValidation(ClassificationRequest::class);

        CRUD::addField([
            'name' => 'label',
            'label' => 'Label',
            'type'  => 'text'
        ]);

        CRUD::addField([
            'name' => 'slug',
            'label' => 'Slug (URL)',
            'type' => 'text',
            'hint' => 'Will be automatically generated from your name, if left empty.',
            // 'disabled' => 'disabled'
        ]);

        CRUD::addField([
            'name' => 'description',
            'label' => 'Description',
            'type' => 'textarea',
        ]);

        $classifications = [ 'species' => 'Especes', 'genus' => 'Genre', 'family' => 'Famille', 'order' => 'Ordre', 'class' => 'Classe', 'phylum' => 'Division', 'kingdom' => 'Règne'];
        CRUD::addField([
            'name'        => 'level',
            'label'       => "Niveau",
            'type'        => 'select2_from_array',
            'options'     => $classifications,
            'allows_null' => false,
            'default'     => 'species',
        ]);

        CRUD::addField([
            'label' => 'Parent',
            'type' => 'select',
            'name' => 'parent_id',
            'entity' => 'parent',
            'attribute' => 'name',
        ]);
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

        protected function setupReorderOperation()
    {
        CRUD::set('reorder.label', 'name');
        CRUD::set('reorder.max_level', 2);
    }
}
