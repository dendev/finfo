<?php

namespace App\Http\Controllers\Api;

use App\Models\Vocabulary;
use Illuminate\Http\Request;


/**
 * Class VocabularyApiController
 * @package App\Http\Controllers\Api
 */
class VocabularyApiController extends ApiController
{
    /**
     * @OA\Get(
     *     path="/api/vocabulary",
     *     description="list all vocabulary",
     *     @OA\Response(response=200, description="success: get list of words"),
     *     @OA\Response(response=202, description="empty: items not found"),
     * )
     */
    public function index(Request $request)
    {
        $vocabularies = Vocabulary::all()->sortBy('slug');
        return $this->sendResponse($vocabularies);
    }

    /**
     * @OA\Get(
     *     path="/api/vocabulary/{slug}",
     *     description="definition of one word",
     *     @OA\Parameter(
     *      name="slug",
     *      description="Slug of word",
     *      required=true,
     *      in="path",
     *      @OA\Schema( type="string" )
     *     ),
     *     @OA\Response(response=200, description="success: word definition"),
     *     @OA\Response(response=202, description="empty: item not found"),
     * )
     * @param Request $request
     * @param $slug
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, $slug)
    {
        $vocabulary = Vocabulary::where('slug', $slug)->first();
        return $this->sendResponse($vocabulary);
    }
}
