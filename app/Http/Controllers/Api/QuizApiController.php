<?php

namespace App\Http\Controllers\Api;

use App\Models\Vocabulary;
use Illuminate\Http\Request;


/**
 * Class VocabularyApiController
 * @package App\Http\Controllers\Api
 */
class QuizApiController extends ApiController
{
    /**
     * @OA\Get(
     *     path="/api/quiz",
     *     description="list all quiz",
     *     @OA\Response(response=200, description="success: get list of quiz"),
     *     @OA\Response(response=202, description="empty: items not found"),
     * )
     */
    public function index(Request $request)
    {
        $vocabularies = Vocabulary::all()->sortBy('slug');
        return $this->sendResponse($vocabularies);
    }

    /**
     * @OA\Get(
     *     path="/api/quiz/{entity}/{type}",
     *     description="Start a quiz about entity and type",
     *     @OA\Parameter(
     *      name="entity",
     *      description="The Subjet of quiz ( plant, vocabulary )",
     *      required=true,
     *      in="path",
     *      @OA\Schema( type="string" )
     *     ),
     *    @OA\Parameter(
     *      name="type",
     *      description="Type of quiz ( random, learn )",
     *      required=true,
     *      in="path",
     *      @OA\Schema( type="string" )
     *     ),
     *     @OA\Response(response=200, description="success: one question and n possible responses"),
     *     @OA\Response(response=202, description="empty: item not found"),
     * )
     * @param Request $request
     * @param $slug
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, $entity, $type)
    {
        $quiz = \QuizManager::get_question_of($entity, $type);
        return $this->sendResponse($quiz);
    }
}
