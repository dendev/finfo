<?php

namespace App\Http\Controllers\Api;

use App\Models\Vocabulary;
use App\Traits\FormatExtras;
use Illuminate\Http\Request;


/**
 * Class VocabularyApiController
 * @package App\Http\Controllers\Api
 */
class ComponentApiController extends ApiController
{
    use FormatExtras;

    /**
     * @OA\Get(
     *     path="/api/front",
     *     description="provid front datas",
     *     @OA\Response(response=200, description="success: get datas"),
     *     @OA\Response(response=202, description="empty: no datas"),
     * )
     */
    public function action(Request $request, $identity, $action, $extras = [])
    {
        $extras = $this->_format_extras($extras, $request, $identity, $action);

        $datas = \ComponentManager::action($identity, $action, $extras);

        return $this->sendResponse($datas);
    }

    /**
     * @OA\Get(
     *     path="/api/vocabulary/{slug}",
     *     description="definition of one word",
     *     @OA\Parameter(
     *      name="slug",
     *      description="Slug of word",
     *      required=true,
     *      in="path",
     *      @OA\Schema( type="string" )
     *     ),
     *     @OA\Response(response=200, description="success: word definition"),
     *     @OA\Response(response=202, description="empty: item not found"),
     * )
     * @param Request $request
     * @param $slug
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, $slug)
    {
        $vocabulary = Vocabulary::where('slug', $slug)->first();
        return $this->sendResponse($vocabulary);
    }
}
