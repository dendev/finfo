<?php


namespace App\Http\Controllers;


use Illuminate\Support\Facades\Request;

class PageController extends Controller
{
    public function home(Request $request)
    {
        return view('pages.home');
    }

    public function plant(Request $request)
    {
        return view('pages.plant');
    }

    public function quiz_vocabulary(Request $request)
    {
        return view('pages.quiz.vocabulary');
    }

    public function quiz_name(Request $request)
    {
        return view('pages.quiz.name');
    }

    public function quiz_image(Request $request)
    {
        return view('pages.quiz.image');
    }

    public function quiz_family(Request $request)
    {
        return view('pages.quiz.family');
    }

    public function vocabulary(Request $request)
    {
        return view('pages.vocabulary');
    }
}
