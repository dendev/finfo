<?php


namespace App\Traits;


trait VocabularyParser
{
    private $_parser_separator = '@';
    private $_parser_delimiter_open = '[';
    private $_parser_delimiter_close = ']';
    private $_parser_vocabulary_url = '/test/';
    private $_vocabulary_words = [];
   // private $_parser_default_type = 'html_link';

    public function parse_vocabularies_to_html_link($text)
    {
        return $this->_parse_vocabularies_to_type($text, 'html_link');
    }

    public function parse_vocabularies_to_html_tooltip($text)
    {
        return $this->_parse_vocabularies_to_type($text, 'html_tooltip');
    }

    public function parse_vocabularies_to_raw_word($text)
    {
        return $this->_parse_vocabularies_to_type($text, 'raw_word');
    }

    public function parse_vocabularies_to_raw_slug($text)
    {
        return $this->_parse_vocabularies_to_type($text, 'raw_slug');
    }

    public function get_vocabulary_words()
    {
        return $this->_vocabulary_words;
    }

    // -
    private function _parse_vocabularies_to_type($text, $type )
    {
        $available_types = [
            'html_link' => '_to_html_link',
            'raw_word' => '_to_raw_word',
            'raw_slug' => '_to_raw_slug',
        ];

        $this->_vocabulary_words = []; // keep list of vocabulary words in text

        if( array_key_exists($type, $available_types) )
        {
            // set formater
            $format_mth = $available_types[$type];

            // search vocabulary format
            $re = '/(?<=\[)(.*?)(?=\])/m'; // TODO user delimiter var
            preg_match_all($re, $text, $matches, PREG_SET_ORDER, 0);

            // found
            if( $matches && count( $matches) > 0 )
            {
                // iterate
                foreach ($matches as $match)
                {
                    $found = $match[0]; // TODO iterate

                    // make html tag
                    $tmp = explode($this->_parser_separator, $found);
                    if (count($tmp) == 2)
                    {
                        $word = $tmp[0];
                        $slug = $tmp[1];

                        // change text to another format
                        $word_formated = $this->$format_mth($slug, $word);

                        // replace
                        $found = $this->_parser_delimiter_open . $found . $this->_parser_delimiter_close;
                        $text = str_replace($found, $word_formated, $text);

                        // keep
                        if( ! in_array($word_formated, $this->_vocabulary_words) )
                            $this->_vocabulary_words[] = $word_formated;
                    }
                    else
                    {
                        $format = $this->_parser_delimiter_open . 'text' . $this->_parser_separator . 'slug' . $this->_parser_delimiter_close;
                        \Log::error("[VocabularyParser:parse_vocabulary_to_type] VPpvtt:02 Erreur dans le format $format du vocabulaire à parser", [
                            'text' => $text,
                            'format' => $format
                        ]);
                    }
                }
            }
        }
        else
        {
            \Log::error("[VocabularyParser:parse_vocabulary_to_type] VPpvtt:01 Erreur le type demandé ($type) n'existe pas",[
                'available_types' => $available_types,
                'type' => $type
            ]);
        }

        return $text;
    }


    private function _to_html_link($slug, $word)
    {
        $access_key = $slug[0];
        $word_url = $this->_parser_vocabulary_url . $slug;
        $word_html = "<a href='$word_url' accesskey='$access_key'>$word</a>";

        return $word_html;
    }

    private function _to_raw_word($slug, $word)
    {
        return $word;
    }

    private function _to_raw_slug($slug, $word)
    {
        return $slug;
    }
}
