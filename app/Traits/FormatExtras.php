<?php


namespace App\Traits;


use App\Models\Visitor;

trait FormatExtras
{
    private function _format_extras($extras, $request, $identity, $action)
    {
        $item_type = $request->header('Item-Type', 'botanic');
        if( $extras ) // form get mth
        {
            if( is_string($extras) ) // it's json
            {
                if( $this->is_json($extras) )
                {
                    $extras = json_decode($extras, true);
                }
                else
                {
                    $slug = $extras;
                    $extras = [];
                    $extras['slug'] = $slug;
                }
            }
        }
        else // from post mth
        {
            $extras = $request->input('datas');
        }

        // basic
        $extras['identity'] = $identity;
        $extras['action'] = $action;
        $extras['item_type'] = $item_type;

        // auth datas
        $extras['token'] = str_replace('Bearer ', '', $request->headers->get('authorization') );

        // user datas
        $user = $this->tokenToUser($request);
        $extras['user'] = $user;
        $extras['is_auth'] = ( $user ) ? true: false;

        return $extras;
    }

    private function is_json($string)
    {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }
}
