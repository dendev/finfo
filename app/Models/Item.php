<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use App\Models\Name;

class Item extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'items';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    protected $fillable = ['label', 'description', 'classification_id'];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function get_classifications_as_string($order = 'asc')
    {
       return \ClassificationManager::get_classifications_of_item_as_string($this, $order);
    }

    public function get_classifications_as_array($order = 'asc')
    {
        return \ClassificationManager::get_classifications_of_item_as_array($this, $order);
    }

    public function get_extras_as_array($order = 'asc')
    {
        return \ItemExtraManager::get_extras_of_item_as_array($this, $order);
    }

    public function get_type_as_array()
    {
        return $this->type->toArray();
    }

    public function get_tags_as_array()
    {
        return $this->tags->toArray();
    }

    public function get_images_as_html()
    {
        return \ImageManager::get_images_of_item_as_html($this);
    }

    public function get_images_as_array()
    {
        return \ImageManager::get_images_of_item_as_array($this);
    }

    public function get_images_as_gallery()
    {
        return \ImageManager::get_images_of_item_as_gallery($this);
    }

    public function get_names_as_string()
    {
        return \NameManager::get_names_of_item_as_string($this);
    }

    public function get_names_as_array()
    {
        return \NameManager::get_names_of_item_as_array($this);
    }

    public function get_extras()
    {
        return \ItemExtraManager::get_extras_of_item($this);
    }

    public function full() // use it for api output
    {
        // beautify
        $tmp_group = $this->attributes['group'];
        $tmp_group_2 = $this->attributes['group_2'];
        $tmp_created_at = $this->attributes['created_at'];
        $tmp_updated_at = $this->attributes['updated_at'];

        unset( $this->attributes['group']);
        unset( $this->attributes['group_2']);
        unset( $this->attributes['created_at']);
        unset( $this->attributes['updated_at']);

        // add
        $this->attributes['names'] =  $this->get_names_as_array();
        $this->attributes['images'] =  $this->get_images_as_gallery();
        $this->attributes['classifications'] =  $this->get_classifications_as_array('desc');
        $this->attributes['extras'] =  $this->get_extras_as_array('desc');
        $this->attributes['tags'] =  $this->get_tags_as_array();
        $this->attributes['type'] =  $this->get_type_as_array();

        $this->attributes['group'] = $tmp_group;
        $this->attributes['group_2'] = $tmp_group_2;
        $this->attributes['created_at'] = $tmp_created_at;
        $this->attributes['updated_at'] = $tmp_updated_at;

        // remove
        unset($this->classification);
        unset( $this->attributes['classification_id']);
        unset( $this->attributes['item_type_id']);
    }

    public function set_group_2() // bad name TODO group_2 or group_family
    {
        $added = false;

        $family = \ClassificationManager::get_family_of_item_as_array($this);

        if ($family)
        {
            $this->attributes['group_2'] = $family['label'];
            $this->save();
            $added = true;
        }

        return $added;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function names()
    {
        return $this->hasMany('App\Models\Name');
    }

    public function type() // TODO
    {
        return $this->belongsTo('App\Models\ItemType', 'item_type_id', 'id', 'item_types');
    }

    public function classification()
    {
        return $this->belongsTo('App\Models\Classification', 'classification_id', 'id', 'classifications');
    }

    public function tags()
    {
        return $this->hasManyThrough('App\Models\Tag', 'App\Models\ItemTag', 'tag_id', 'id');
    }
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
    public function setLabelAttribute($value)
    {
        $this->attributes['label'] = $value;

        $slug = \Str::slug($value);
        $this->attributes['slug'] = $slug;
        $this->attributes['group'] = ucfirst($slug[0]);
    }
}
