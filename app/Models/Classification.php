<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Classification extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'classifications';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    protected $fillable = ['label', 'slug', 'parent_id'];
    protected $appends = ['level_fr'];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */



    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function parent()
    {
        return $this->belongsTo('App\Models\Classification', 'parent_id');
    }

    public function children()
    {
        return $this->hasMany('App\Models\Classification', 'parent_id');
    }

    public function items() // TODO not good
    {
        return $this->hasMany('App\Models\Item');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */
    public function scopeFirstLevelItems($query)
    {
        return $query->where('depth', '1')
            ->orWhere('depth', null)
            ->orderBy('lft', 'ASC');
    }

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */
    // The slug is created automatically from the "name" field if no slug exists.
    public function getSlugOrLabelAttribute()
    {
        if ($this->slug != '') {
            return $this->slug;
        }

        return $this->label;
    }

    public function getLevelFrAttribute()
    {
        $refs = [
            'kingdom' => 'Règne',
            'phylum' => 'Embranchement',
            'class' => 'Classe',
            'order' => 'Ordre',
            'family' => 'Famille',
            'genus' => 'Genre',
            'species' => 'Espèce',
        ];

        $fr = $refs[$this->level];

        return $fr;
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
    public function setLabelAttribute($value)
    {
        $this->attributes['label'] = $value;
        $this->attributes['slug'] = \Str::slug($value);
    }
}
