<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use App\Traits\VocabularyParser;

class Vocabulary extends Model
{
    use CrudTrait;
    use VocabularyParser;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'vocabularies';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    protected $appends = ['vocabulary_words'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];
    private $_parser_default_type = 'raw_slug';

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function type()
    {
        $type = \DB::table('item_vocabularies')
            ->join('vocabularies', 'item_vocabularies.vocabulary_id', '=', 'vocabularies.id')
            ->join('items', 'item_vocabularies.item_id', '=', 'items.id')
            ->join('item_types', 'items.item_type_id', '=', 'item_types.id')
            ->where('vocabularies.id', '=', 1)
            ->select('item_types.*')
            ->first();
        return $type;
    }

    public function type_label()
    {
        return $this->type()->label;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */
    public function getDescriptionHtmlLinkAttribute()
    {
        return $this->parse_vocabularies_to_html_link($this->description);
    }

    public function getDescriptionRawWordAttribute()
    {
        return $this->parse_vocabularies_to_raw_word($this->description);
    }

    public function getDescriptionRawSlugAttribute()
    {
        return $this->parse_vocabularies_to_raw_slug($this->description);
    }

    public function getVocabularyWordsAttribute()
    {
        return $this->get_vocabulary_words();
    }
    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
    public function setLabelAttribute($value)
    {
        $this->attributes['label'] = $value;

        // make slug without ( ... )
        $begin = strpos($value, '(');
        $end = strpos($value, ')');
        $to_remove = substr($value, $begin, $end);

        $value = str_replace($to_remove, '', $value);
        $value = str_replace(' ', '', $value);

        $slug = \Str::slug($value);
        $this->attributes['slug'] = \Str::slug($slug);

        // set group
        $this->attributes['group'] = ucfirst($slug[0]);
    }

    /*
    |--------------------------------------------------------------------------
    | EVENTS
    |--------------------------------------------------------------------------
    */
    protected static function booted()
    {
        static::retrieved(function ($vocabulary) {
            $parser_default_type = $vocabulary->_parser_default_type;
            if( $parser_default_type  === 'raw_word')
                $vocabulary->descriptionRawWord;
            else if( $parser_default_type  === 'raw_slug')
                $vocabulary->descriptionRawSlug;
            else
                $vocabulary->descriptionHtmlLink;
        });
    }


}
