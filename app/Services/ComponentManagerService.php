<?php
namespace App\Services;


class ComponentManagerService
{
    private $_available_actions;

    public function __construct()
    {
        $this->_available_actions = [
            'plant' => ['\App\Services\Components\Item' => [
                'get_page' => 'get_page_plant',
                'get_plant' => 'get_item_plant',
                'search_plants' => 'search_plants',
            ]],

            'quiz' => ['\App\Services\Components\Quiz' => [
                'get_page' => 'get_page',
                'get_next' => 'get_page',
            ]],

            'vocabulary' => ['\App\Services\Components\Vocabulary' => [
                'get_page' => 'get_page',
                'search' => 'search'
            ]],
        ];
    }

    public function action($identity, $action, $extras)
    {
        $datas = false;

        // find class to do action
        $class_and_method = $this->_get_classname_and_method($identity, $action);

        // instantiate
        $classname = $class_and_method['classname'];
        $object = new $classname;

        // do action
        $method = $class_and_method['method'];
        $datas = $object->$method($extras);

        // end
        return $datas;
    }

    private function _get_classname_and_method($identity, $action)
    {
        $ok = false;

        if( array_key_exists($identity, $this->_available_actions) )
        {
            $actions = $this->_available_actions[$identity];
            $classname = key($actions);

            if( array_key_exists($action, $actions[$classname]))
            {
                $mth = $actions[$classname][$action];
                $ok = [
                    'classname' => $classname,
                    'method' => $mth,
                ];
            }
            else
            {
                \Log::error("[ComponentManagerService] CMSgcam02: L'action '${action}' n'existe pas", [
                    'action' => $action,
                    'available' => $this->_available_actions,
                ]);
                throw new \Exception("Method for $action not found");
            }
        }
        else
        {
            \Log::error("[ComponentManagerService] CMSgcam01: L'identity '${identity}' n'existe pas", [
                'identity' => $identity,
                'available' => $this->_available_actions,
            ]);
            throw new \Exception("Class for $identity not found");
        }

        return $ok;
    }
}
