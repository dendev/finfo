<?php
namespace App\Services;


use App\Models\Item;
use App\Models\ItemExtra;
use App\Traits\UtilService;

class ItemExtraManagerService
{
    use UtilService;

    public function create($label, $slug, $description, $fields, $id_or_model_item )
    {
        $item = $this->_instantiate_if_id($id_or_model_item, Item::class);

        if( $item )
        {
            $item_extra = new ItemExtra();
            $item_extra->label = $label;
            $item_extra->slug = $slug;
            $item_extra->description = $description;
            $item_extra->fields = json_encode($fields);
            $item_extra->item_id = $item->id;
            $item_extra->save();
        }
        else
        {
            \Log::error("[ItemExtraManagerService:create] IEMSc01: L'item n'est pas valide", [
                'item' => $item,
                'id_or_model_item' => $id_or_model_item
            ]);
        }
    }

    public function get_extras_of_item_as_array($id_or_model_item)
    {
        return $this->get_extras_of_item($id_or_model_item, 'array');
    }

    public function get_extras_of_item($id_or_model_item, $format = false)
    {
        $item = $this->_instantiate_if_id($id_or_model_item, Item::class);

        $item_extras = ItemExtra::where('item_id', $item->id)->get();

        if( $format )
        {
            if( $format === 'array' )
                $formated = $item_extras->toArray();
        }


        return ( $format ) ? $formated : $item_extras;
    }


}
