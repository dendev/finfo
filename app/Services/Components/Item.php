<?php
namespace App\Services\Components;

use App\Traits\UtilService;
use Illuminate\Http\Request;

class Item
{
    use UtilService;

    // Services
    public function get_page_plant($extras)
    {
        $items = false;

        $filters = [
            'type' => 'botanic',
        ];

        $group_by = $this->get_extra_value_of( 'group_by', $extras, __CLASS__, __METHOD__, 'gpp01');

        if( $group_by )
        {
            $items = \ItemManager::get_all_grouped($filters, $group_by);
        }

        $datas = $this->_format($items);

        return $datas;
    }

    public function get_item_plant($extras)
    {
        $item = false;

        $slug = $this->get_extra_value_of( 'slug', $extras, __CLASS__, __METHOD__, 'gip01');

        if( $slug )
        {
            $item = \ItemManager::get_item_plant($slug, true);
        }

        $datas = $this->_format($item);
        return $datas;
    }


    public function search_plants($extras)
    {
        $choices = [];
        $slug = $this->get_extra_value_of( 'slug', $extras, __CLASS__, __METHOD__, 'sp01');

        if( $slug )
        {
            $choices = \ItemManager::search_items_plant($slug, 10);
        }


        return $this->_format($choices);
    }

    // -
    private function _format( $datas )
    {
        $items = [];

        $items['items'] = $datas;

        return $items;
    }
}
