<?php
namespace App\Services\Components;

use Illuminate\Http\Request;

class Quiz
{
    // Services
    public function get_page($extras)
    {
        $datas = false;

        $entity_name = false;
        $mode = false;
        $reverse = false;
        $item_type = false;

        if( array_key_exists('entity_name', $extras))
            $entity_name = $extras['entity_name'];
        if( array_key_exists('mode', $extras))
            $mode = $extras['mode'];
        if( array_key_exists('reverse', $extras))
            $reverse = $extras['reverse'];
        if( array_key_exists('item_type', $extras))
            $item_type = $extras['item_type'];

        if( $entity_name && $mode && $item_type )
        {
            // question && responses
            $datas = \QuizManager::get_question_of($entity_name, $item_type, $mode, $reverse);
            $datas = $this->_format($entity_name, $datas, $reverse);
        }
        else
        {
            \Log::error("[Quiz] Qgp01: Manque un argument essentiel", [
                'entity_name' => $entity_name,
                'mode' => $mode,
                'item_type' => $item_type,
                'extras'=> $extras
            ]);
        }

        return $datas;
    }

    private function _format( $entity_name, $datas, $reverse = false )
    {
        $quiz = false;

        // add label
        $label = $this->_get_label_of_entity_name($entity_name);
        $quiz['label'] = $label;
        if( $datas )
        {
            $quiz['question'] = $datas['question'];
            $quiz['responses'] = $datas['responses'];
        }

        /*
        // question
        if( $datas )
        {
            $question = $datas['question'];

            // responses with other
            $responses = $datas['responses'];
            $responses->push($question);
            $responses->shuffle();

            // format question
            $formated_question = [];
            $formated_question['label'] = (!$reverse) ? $question->label : $question->description;
            $formated_question['id'] = $question->id;

            $quiz['question'] = $formated_question;

            // format responses
            $formated_responses = [];
            foreach ($responses as $key => $response) {
                $formated_responses[$key] = [];
                $formated_responses[$key]['label'] = (!$reverse) ? $response->description : $response->label;
                $formated_responses[$key]['id'] = $response->id;

                //  // info about correct response
                $formated_responses[$key]['is_correct_response'] = false;
                if ($response->id === $question->id)
                    $formated_responses[$key]['is_correct_response'] = true;
            }

            $quiz['responses'] = $formated_responses;

        }
        */
        // end url
        $end_url = route('pages.home');
        $quiz['end_url'] = $end_url;

        return $quiz;
    }

    private function _get_label_of_entity_name($entity_name)
    {
        $label = false;

        // refs
        $names = [
            'vocabulary' => 'Vocabulaire',
            'name' => 'Noms',
            'image' => 'Images',
            'family' => 'Famille',
        ];

        // format
        $entity_name = mb_strtolower($entity_name);

        // translate
        if( array_key_exists($entity_name, $names))
        {
            $label = $names[$entity_name];
        }
        else
        {
            \Log::error("[QuizManagerService] QZMgloen01: Aucun nom trouvé pour l'entité ${entity_name}",[
                'entity_name', $entity_name,
                'names', $names
            ]);
        }

        // end
        return $label;
    }
}
