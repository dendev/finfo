<?php
namespace App\Services\Components;

use App\Traits\UtilService;
use Illuminate\Http\Request;
use App\Models\Vocabulary as VocabularyModel;
use Illuminate\Support\Facades\DB;

class Vocabulary
{
    use UtilService;

    // Services
    public function get_page($extras)
    {
        $vocabularies = \VocabularyManager::get_all_grouped();
        $datas = $this->_format($vocabularies);

        return $datas;
    }

    public function search($extras)
    {
        $choices = [];
        $slug = $this->get_extra_value_of( 'slug', $extras, __CLASS__, __METHOD__, 's01');

        if( $slug )
        {
            $choices = VocabularyModel::where('label', 'LIKE', "%$slug%")->limit(10)->get();
            $choices = $choices->toArray();
        }


        return $this->_format($choices);
    }

    // -
    private function _format( $datas )
    {
        $vocabulary = [];

        $vocabulary['vocabularies'] = $datas;

        return $vocabulary;
    }
}
