<?php
namespace App\Services;


use App\Models\Item;
use App\Models\ItemType;
use App\Traits\UtilService;
use Illuminate\Support\Str;

class ItemManagerService
{
    use UtilService;

    // Create
    public function create( $label, $names, $description, $type_slug, $classifications, $images = false, $tags = false )
    {
        $item_type = ItemType::where('slug', $type_slug)->first();
        if( $item_type )
        {
            // check exist
            $already_exist = Item::where('slug', Str::slug($label))
                ->where('item_type_id', $item_type->id)
                ->first();

            // create
            if (! $already_exist)
            {
                //  // classification
                $classification_id = $this->add_classifications($classifications);

                // // get family
                $family =  $classifications[4]['label'];

                // //  basic
                $item_type_id = $item_type->id;
                $description = ( $description ) ? $description : null;

                $item = new Item();
                $item->label = $label;
                $item->description = $description;
                $item->item_type_id = $item_type_id;
                $item->classification_id = $classification_id;
                $item->group_2 = $family;
                $item->save();

                // // group family ( call it after classifications set )
                $item->set_group_2();

                //  // names
                $this->add_names($names, $item);

                //  // images
                $this->add_images($images, $item);

                //  // tags
                $this->add_tags($tags, $item);
            }
            else
            {
                \Log::warning("[ItemManager:create] IMc00: L'item existe déjà", [
                    'already_exist' => $already_exist
                ]);
            }
        }
        else
        {
            \Log::error("[ItemManager:create] IMc01: Le type ${item_type} n'existe pas", [
                'item_type' => $item_type,
            ]);
        }
    }


    // Add
    public function add_classifications($datas)
    {
        $classifications = \ClassificationManager::create($datas);
        $classification_species_id =  $classifications->last()->id;

        // todo family id

        return $classification_species_id;
    }

    public function add_names($datas, $id_or_model_item)
    {
        $item = $this->_instantiate_if_id($id_or_model_item, Item::class);
        $names = collect();

        foreach ($datas as $data )
        {
            $name = \NameManager::create($data['label'], $data['type'], $item);
            $names->push($name);
        }

        return ( $names->count() > 0 ) ? $names : false;
    }

    public function add_images($datas, $id_or_model_item)
    {
        $item = $this->_instantiate_if_id($id_or_model_item, Item::class);
        $images = collect();

        if( $datas )
        {
            $is_first = true;

            foreach ($datas as $data)
            {
                $path = $data['path'];

                $default = explode('/', $path);
                $default = str_replace('_', ' ', last( $default) );
                $default = substr($default, 0, strpos($default, '.'));
                $default = ucfirst($default);

                $label = (array_key_exists('label', $data)) ? $data['label'] : $default;
                $description = (array_key_exists('description', $data)) ? $data['description'] : null;
                $is_principal = (array_key_exists('is_principal', $data)) ? $data['is_principal'] : $is_first;

                $image = \ImageManager::create($label, $path, $description, $is_principal, $item);

                $images->push($image);
                $is_first = false;
            }
        }

        return ( $images->count() > 0 ) ? $images : false;
    }

    public function add_tags($datas, $id_or_model_item)
    {
        $item = $this->_instantiate_if_id($id_or_model_item, Item::class);
        $tags = collect();

        if( $datas )
        {
            foreach ($datas as $data)
            {
                $tag = \ItemTagManager::create($data, $item);

                $tags->push($tag);
            }
        }

        return ( $tags->count() > 0 ) ? $tags : false;
    }

    // Get
    public function get_all_grouped($filters = false, $group_by='group')
    {
        // all or filtered
        if ($filters)
        {
            // filter
            $filter_type = ( array_key_exists('type', $filters) ) ? $filters['type'] : false;

            // basic rq
            $rq = Item::query();

            // where rq
            if ($filter_type)
            {
                $item_type = ItemType::where('slug', $filter_type)->first();
                $items = $rq->where('item_type_id', $item_type->id)
                    ->get();
            }
        }
        else
        {
            $items = Item::all();
        }

        // order
        if ($items->count() > 0)
        {
            $items = $items->sortBy('label')
                ->groupBy($group_by)
                ->sortKeys();
        }

        // end
        return $items;
    }

    public function get_item_plant($item_slug, $full)
    {
        $ref_item_type = 'botanic';
        return $this->get_item_of_type($item_slug, $ref_item_type, $full);
    }

    public function get_item_of_type($item_slug, $type_slug, $full)
    {
        $item = false;

        // get type
        $type = ItemType::where('slug', $type_slug)->first();
        if( $type )
        {
            // get item of type
            $item = Item::where('item_type_id', $type->id)->where('slug', $item_slug)->first();
            if(  $item )
            {
                // populate with extras datas
                if( $full )
                {
                    $item->full();
                }
            }
            else
            {
                \Log::error($this->get_log_msg(__CLASS__, __METHOD__, 'giot02', "Impossible de trouver l'item $item_slug"), [
                    'item_slug' => $item_slug
                ]);
            }
        }
        else
        {
            \Log::error($this->get_log_msg(__CLASS__, __METHOD__, 'giot02', "Impossible de trouver le type $type_slug"), [
                'type_slug' => $type_slug
            ]);
        }

        return $item;
    }

    // Search
    public function search_items_plant($item_slug, $limit = false)
    {
        $ref_item_type = 'botanic';
        return $this->search_items_of_type($item_slug, $ref_item_type, $limit);
    }

    public function search_items_of_type($item_slug, $type_slug, $limit)
    {
        $items = false;

        // get type
        $type = ItemType::where('slug', $type_slug)->first();
        if( $type )
        {
            // get item of type
            if( ! $limit )
                $items = Item::where('item_type_id', $type->id)->where('slug', 'like', "%$item_slug%")->get();
            else
                $items = Item::where('item_type_id', $type->id)->where('slug', 'like', "%$item_slug%")->limit($limit)->get();
        }
        else
        {
            \Log::error($this->get_log_msg(__CLASS__, __METHOD__, 'giot02', "Impossible de trouver le type $type_slug"), [
                'type_slug' => $type_slug
            ]);
        }

        return $items;
    }

}
