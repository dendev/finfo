<?php
namespace App\Services;


use App\Models\ItemType;
use App\Traits\UtilService;

class QuizManagerService
{
    use UtilService;

    public function get_question_of( $entity_name, $item_type_slug, $mode, $reverse = false )
    {
        $question = false;

        if( \BrainManager::quiz_is_available($entity_name, $mode ) )
        {
                if( $mode === 'random')
                    $question = $this->get_random_question( $entity_name, $item_type_slug, $reverse);
                else
                    \Log::error("[QuizManagerService::get_question_of] QMSgqo02: Erreur le mode random n'existe pas pour ce quiz", [
                        'entity_name' => $entity_name,
                        'mode' => $mode,
                    ]);
        }
        else
        {
            \Log::error("[QuizManagerService::get_question_of] QMSgqo01: Erreur le quiz demandé n'est pas acceptable", [
                'entity_name' => $entity_name,
                'mode' => $mode,
            ]);
        }

        return $question;
    }

    public function get_random_question($entity_name, $item_type_slug, $reverse)
    {
        if( $entity_name === 'vocabulary')
            return $this->get_random_vocabulary_question($item_type_slug, $reverse);
        else if( $entity_name === 'image')
            return $this->get_random_image_question($item_type_slug, $reverse);
        else if( $entity_name === 'name')
            return $this->get_random_name_question($item_type_slug, $reverse);
        else if( $entity_name === 'family' )
            return $this->get_random_family_question($item_type_slug, $reverse);
    }
    public function get_random_vocabulary_question($item_type_slug, $reverse)
    {
        $entity_name_question = "Vocabulary";
        $entity_name_response = "Vocabulary";
        $response_key = 'id';
        $question_key = 'id';
        if( ! $reverse )
        {
            $question_field = 'label';
            $response_field = 'description';
        }
        else
        {
            $question_field = 'description';
            $response_field = 'label';
        }

        return $this->_get_random_question($entity_name_question, $entity_name_response, $response_key, $question_key, $question_field, $response_field, $item_type_slug, $reverse );
    }

    public function get_random_image_question($item_type_slug, $reverse)
    {
        $entity_name_question = "Image";
        $entity_name_response = "Item";
        $response_key = 'id';
        $question_key = 'item_id';
        $question_field = 'url';
        $response_field = 'label';

        return $this->_get_random_question($entity_name_question, $entity_name_response, $response_key, $question_key, $question_field, $response_field, $item_type_slug, $reverse );
    }

    public function get_random_name_question($item_type_slug, $reverse)
    {
        if( ! $reverse )
        {
            $entity_name_question = "Item";
            $entity_name_response = "Name";
            $response_key = 'item_id';
            $question_key = 'id';
            $question_field = 'label';
            $response_field = 'label';
        }
        else
        {
            $entity_name_question = "Name";
            $entity_name_response = "Item";
            $response_key = 'id';
            $question_key = 'item_id';
            $question_field = 'label';
            $response_field = 'label';
        }

        return $this->_get_random_question($entity_name_question, $entity_name_response, $response_key, $question_key, $question_field, $response_field, $item_type_slug, $reverse );
    }

    public function get_random_family_question($item_type_slug, $reverse)
    {
        if( $reverse)
        {
            $entity_name_question = "Classification";
            $entity_name_response = "Item";
            $response_key = 'id';
            $question_key = 'id';
            $question_field = 'label';
            $response_field = 'label';
        }
        else
        {
            $entity_name_question = "Item";
            $entity_name_response = "Classification";
            $response_key = 'id';
            $question_key = 'id';
            $question_field = 'label';
            $response_field = 'label';
        }

        return $this->_get_random_family_question($entity_name_question, $entity_name_response, $response_key, $question_key, $question_field, $response_field, $item_type_slug, $reverse );
    }

    // -
    private function _get_random_question( $entity_name_question, $entity_name_response, $response_key, $question_key, $question_field, $response_field, $item_type_slug, $reverse )
    {
        $formated = false;

        // item type
        $item_type = ItemType::where('slug', $item_type_slug)->first();
        if( $item_type )
        {
            // question
            $full_classname_question = "\App\Models\\$entity_name_question";
            $question = $full_classname_question::inRandomOrder()
                ->first();

            // responses
            $full_classname_response = "\App\Models\\$entity_name_response";
            $responses = $full_classname_response::where($response_key, '<>', $question->$question_key)
                ->inRandomOrder()
                ->limit(3)
                ->get();

            // correct
            $correct = $full_classname_response::where($response_key, $question->$question_key)->first();

            // debug
            // dd( $question );

            // format
            $formated = $this->_format($question, $responses, $correct, $response_key, $question_key, $question_field, $response_field, $reverse );
        }
        else
        {
            \Log::error("[QuizManagerService:_get_random_question_of] QMS_grqo00: Pas d'item trouvé pour le slug $item_type_slug", [
                'item_type_slug' => $item_type_slug,
            ]);
        }

        // end
        return $formated;
    }

    private function _get_random_family_question( $entity_name_question, $entity_name_response, $response_key, $question_key, $question_field, $response_field, $item_type_slug, $reverse )
    {
        $formated = false;

        // item type
        $item_type = ItemType::where('slug', $item_type_slug)->first();
        if( $item_type )
        {

            if( ! $reverse)
            {
                // question
                $full_classname_question = "\App\Models\\$entity_name_question";
                $question = $full_classname_question::inRandomOrder()
                    ->first();

                $family = \ClassificationManager::get_family_of_item($question);

                // responses
                $full_classname_response = "\App\Models\\$entity_name_response";
                $responses = $full_classname_response::where($response_key, '<>', $family[$question_key])
                    ->inRandomOrder()
                    ->limit(3)
                    ->get();

                // correct
                $correct = $family;
            }
            else
            {
                // question
                $full_classname_question = "\App\Models\\$entity_name_question";
                $question = $full_classname_question::where('level', 'family')
                    ->inRandomOrder()
                    ->first();

                $items_in_family = \ClassificationManager::get_items_of_family($question->slug);
                // responses
                $full_classname_response = "\App\Models\\$entity_name_response";
                $responses = $full_classname_response::where($response_key, '<>', $question->$question_key)
                    ->inRandomOrder()
                    ->limit(3)
                    ->get();

                // correct
                $correct = $items_in_family->random();
            }

            // debug
            // dd( $question );
            //  dd( $correct);

            // format
            $formated = $this->_format($question, $responses, $correct, $response_key, $question_key, $question_field, $response_field, $reverse );
        }
        else
        {
            \Log::error("[QuizManagerService:_get_random_question_of] QMS_grqo00: Pas d'item trouvé pour le slug $item_type_slug", [
                'item_type_slug' => $item_type_slug,
            ]);
        }

        // end
        return $formated;
    }

    private function _format($question, $responses, $correct, $response_key, $question_key, $question_field, $response_field, $reverse )
    {
        $formated = false;

        if( $question && $responses && $correct )
        {
            // format question
            $formated_question = [];
            $formated_question['label'] = ucfirst($question->$question_field);
            $formated_question['id'] = $question->id;

            $formated['question'] = $formated_question;

            // format responses
            $responses->push($correct);
            $responses->shuffle();

            $formated_responses = [];
            foreach ($responses as $key => $response)
            {
                $formated_responses[$key] = [];
                $formated_responses[$key]['label'] = $response->$response_field;
                $formated_responses[$key]['id'] = $response->id;

                //  // info about correct response
                $formated_responses[$key]['is_correct_response'] = false;
//                if ($response->$response_key === $question->$question_key)
                if ($response->$response_key === $correct->id)
                    $formated_responses[$key]['is_correct_response'] = true;

            }
            $formated['responses'] = $formated_responses;
        }

        return $formated;
    }
}
