<?php
namespace App\Services;


use App\Models\Item;
use App\Models\ItemTag;
use App\Models\Tag;
use App\Traits\UtilService;
use Illuminate\Support\Str;

class ItemTagManagerService
{
    use UtilService;

    public function create($label, $id_or_model_item)
    {
        $item = $this->_instantiate_if_id($id_or_model_item, Item::class);
        $tag_already_exist = Tag::where('slug', Str::slug($label))->first();

        if( ! $tag_already_exist)
            $tag = \TagManager::create($label);
        else
            $tag = $tag_already_exist;

        $item_tag_already_exist = ItemTag::where('tag_id', $tag->id)
            ->where('item_id', $item->id)
            ->first();

        if( ! $item_tag_already_exist )
        {
            $item_tag = new ItemTag();
            $item_tag->item_id = $item->id;
            $item_tag->tag_id = $tag->id;
            $item_tag->save();
        }
        else
        {
            $item_tag = $item_tag_already_exist;
        }

        return $item;
    }
}
