<?php
namespace App\Services;


use App\Models\Image;
use App\Models\Item;
use App\Traits\UtilService;
use Illuminate\Support\Str;

class ImageManagerService
{
    use UtilService;

    public function create($label, $path, $description, $is_principal, $id_or_model_item)
    {
        $item = $this->_instantiate_if_id($id_or_model_item, Item::class);
        $image = false;

        // check exist
        $already_exist = Image::where('slug', Str::slug($label))
            ->first();

        // create
        if (! $already_exist)
        {
            $image = new Image();
            $image->label = $label;
            //$image->slug = $slug; // do by model
            $image->path = $path;
            $image->description = ($description) ? $description : null;
            $image->is_principal = $is_principal;
            $image->item_id = $item->id;
            $image->item_type_id = $item->item_type_id;
            $image->save();
        }
        else
        {
            $image = $already_exist;

            \Log::info("[ImageManager:create] IMc00: L'image existe déjà", [
                'already_exist' => $already_exist
            ]);
        }

        return $image;
    }

    public function get_images_of_item_as_html($id_or_model_item)
    {
        return $this->get_images_of_item($id_or_model_item, 'html');
    }

    public function get_images_of_item_as_array($id_or_model_item)
    {
        return $this->get_images_of_item($id_or_model_item, 'array');
    }

    public function get_images_of_item_as_gallery($id_or_model_item)
    {
        return $this->get_images_of_item($id_or_model_item, 'gallery');
    }

    public function get_images_of_item($id_or_model_item, $format = false)
    {
        $images = false;
        $paths = false;

        $item = $this->_instantiate_if_id($id_or_model_item, Item::class);

        $images = Image::where('item_id', $item->id)->get()->sortByDesc('is_principal');

        if( $format )
        {
            $formated = [];
            foreach ($images as $key => $image)
            {
                if( $format === 'gallery')
                {
                    $formated[] = [ 'original' => $image->url, 'thumbnail' => $image->thumbnail ];
                }
                else if( $format === 'html')
                {
                    $formated[] = "<img src='$image->url' alt='$image->label'>";
                }
                else // basic format
                {
                    $formated[] = $image->toArray();
                }
            }
        }

        return ( $format ) ? $formated : $images;
    }
}
