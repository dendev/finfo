<?php
namespace App\Services;


use App\Models\Classification;
use App\Models\Item;
use App\Traits\UtilService;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class ClassificationManagerService
{
    use UtilService;

    public function create($taxons) // taxa == plural form of taxon
    {
        $classifications = collect();

        if( count( $taxons ) === 7 ) // need 7 levels
        {
            foreach ($taxons as $taxon)
            {
                if( array_key_exists('label', $taxon) && array_key_exists('level', $taxon) && array_key_exists('parent', $taxon)) // basic datas
                {
                    $slug = Str::slug($taxon['label']);

                    // get or create
                    $already_exist = Classification::where('slug', $slug)->first();
                    if( ! $already_exist )
                    {
                        //  // get parent
                        $parent_slug = Str::slug($taxon['parent']);
                        $parent = Classification::where('slug', $parent_slug)->first();
                        if( $parent )
                        {
                            //  // create classification
                            $classification = new Classification();
                            $classification->label = $taxon['label'];
                            // $classification->slug = $taxon['slug']; // do by model
                            $classification->description = ( array_key_exists( 'description', $taxon) ) ? $taxon['description']: null;
                            $classification->level = $taxon['level'];
                            $classification->parent_id = $parent->id;
                            $classification->save();

                            $classifications->push( $classification );
                        }
                        else
                        {
                            \Log::error("[ClassificationManager:create] CMc03: Le parent $parent_slug n'existe pas", [
                                'parent_slug' => $parent_slug,
                                'taxon' => $taxon,
                            ]);
                        }
                    }
                    else
                    {
                        $classification = $already_exist;
                        $classifications->push( $classification );

                        \Log::info("[ClassificationManager:create] CMc03: La classification existe déjà", [
                            'already_exist' => $already_exist
                        ]);
                    }
                }
                else
                {
                    \Log::error("[ClassificationManager:create] CMc00 : Taxos incomplet, manque le label ou le level ou parent", [
                        'taxon' => $taxon
                    ]);

                }
            }
        }
        else
        {
            \Log::error("[ClassificationManager:create] CMc01 : Taxons incomplet 7 niveaux sont attendus ", [
                'taxons' => $taxons
            ]);
        }


        return $classifications;
    }

    public function get_classifications_of_item_as_string($id_or_model_item, $order = 'asc')
    {
        $classifications_string = false;

        $classifications = $this->get_classifications_of_item($id_or_model_item, $order);

        foreach( $classifications as $classification )
        {
            if( $classifications_string != false )
                $classifications_string .= ' -> ';
            $classifications_string .= $classification->label;
        }

        return$classifications_string;
    }

    public function get_classifications_of_item_as_array($id_or_model_item, $order = 'asc')
    {
        $classifications_array = false;

        $classifications = $this->get_classifications_of_item($id_or_model_item, $order);

        foreach( $classifications as $classification )
        {
            if( $classification )
            {
                $classification_array = $classification->toArray();
                unset($classification_array['parent']);
                $classifications_array[] = $classification_array;
            }
            else
            {
                \Log::error("[ClassificationManager:get_classification_of_item_as_array] CMgcoiaa00 : Absence de classification", [
                    'item' => $id_or_model_item,
                    'classification' => $classification,
                    'classifications' => $classifications
                ]);
            }
        }

        return$classifications_array;
    }

    public function get_classifications_of_item($id_or_model_item, $order = 'asc')
    {
        $item = $this->_instantiate_if_id($id_or_model_item, Item::class);

        $classifications = collect();

        $classification_species = $item->classification; // species
        $classification_genus = $classification_species->parent; // genus
        $classification_family = $classification_genus->parent; // family
        $classification_order = $classification_family->parent; // order
        $classification_class = $classification_order->parent; // class
        $classification_phylum = $classification_class->parent; // phylum
        $classification_kingdom = $classification_phylum->parent; // kingdom

        if( $order === 'asc')
        {
            $classifications->push($classification_species);
            $classifications->push($classification_genus);
            $classifications->push($classification_family);
            $classifications->push($classification_order);
            $classifications->push($classification_class);
            $classifications->push($classification_phylum);
            $classifications->push($classification_kingdom);
        }
        else
        {
            $classifications->push($classification_kingdom);
            $classifications->push($classification_phylum);
            $classifications->push($classification_class);
            $classifications->push($classification_order);
            $classifications->push($classification_family);
            $classifications->push($classification_genus);
            $classifications->push($classification_species);
        }

        return $classifications;
    }

    public function get_family_of_item_as_string($id_or_model_item)
    {
        return $this->get_classifications_of_item($id_or_model_item, 'string');
    }

    public function get_family_of_item_as_array($id_or_model_item)
    {
        return $this->get_family_of_item($id_or_model_item, 'array');
    }

    public function get_family_of_item($id_or_model_item, $format = false)
    {
        $formated = false;

        $classifications = $this->get_classifications_of_item_as_array($id_or_model_item, 'asc');

        $nb_classifications = count($classifications);
        if( $nb_classifications > 0 )
        {
            if( $nb_classifications === 7 )
                $family_index = 2;
            else
                $family_index = 1;

            $family = $classifications[$family_index];
        }
        else
        {
            $family = false;
            \Log::error("[ClassificationManager:get_family_of_item] CMgfoi00 : Absence de classifications", [
                'item' => $id_or_model_item,
                'classifications' => $classifications
            ]);
        }

        // format
        if( $family )
        {
            if ($format === 'string')
            {
                $formated = $family['label'];
            }
            else if( $format === 'array' )
            {
                $formated = $family;
            }
            else
            {
                $formated = Classification::find($family['id']);
            }
        }

        return $formated;
    }

    public function get_items_of_family($family_slug, $format = false)
    {
        $formated = false;

        $sql_rez = DB::select("
            SELECT items.* FROM items
            WHERE items.classification_id IN (
                SELECT species.id FROM classifications species
                WHERE species .parent_id  IN  (
                    SELECT id AS genus_id FROM classifications genus
                    WHERE genus.parent_id  = ( SELECT id from classifications family where slug = ?)
                )
            )
        ", [$family_slug]);

        if( $sql_rez )
        {
            $items = Item::hydrate($sql_rez);

            if ($format === 'string')
            {
                $formated = '';
                $nb_item = $items->count();
                foreach( $items as $key => $item )
                {
                    $separator = ( $key < $nb_item - 1 ) ? ', ': '';
                    $formated .= $item->label . $separator;
                }
            }
            else if( $format === 'array')
            {
                $formated = $items->toArray();
            }
            else
            {
                $formated = $items;
            }
        }

        return $formated;
    }
}
