<?php
namespace App\Services;


use App\Models\Vocabulary;

class VocabularyManagerService
{
    public function get_all_grouped()
    {
        $vocabularies = Vocabulary::all()
            ->sortBy('label')
            ->groupBy('group')
            ->sortKeys();

        return $vocabularies;
    }
}
