<?php
namespace App\Services;


use App\Models\Tag;
use App\Traits\UtilService;

class TagManagerService
{
    use UtilService;

    public function create($label, $description = false)
    {
        $tag = false;

        if ($label)
        {
            $tag = new Tag();
            $tag->label = $label;
            //$tag->slug = Str::slug($label); // Do by model
            $tag->description = $description;
            $tag->save();
        }
        else
        {
            \Log::error("[TagManagerService:create] TMSc01: La label est obligatoire", [
                'label' => $label
            ]);
        }

        return $tag;
    }
}
