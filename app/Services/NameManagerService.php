<?php
namespace App\Services;


use App\Models\Item;
use App\Models\Name;
use App\Traits\UtilService;
use Illuminate\Support\Str;

class NameManagerService
{
    use UtilService;

    public function create($label, $type, $id_or_model_item)
    {
        $item = $this->_instantiate_if_id($id_or_model_item, Item::class);
        $name = false;

        // check exist
        $already_exist = Name::where('slug', Str::slug($label))
            ->first();

        // create
        if (! $already_exist)
        {
            $name = new Name();
            $name->label = $label;
            //$name->slug = $slug; // to by model
            $name->type = $type;
            $name->item_id = $item->id;
            $name->item_type_id = $item->item_type_id;
            $name->save();
        }
        else
        {
            \Log::warning("[NameManager:create] IMc00: Le nom existe déjà", [
                'already_exist' => $already_exist
            ]);
        }

        return $name;
    }

    public function get_names_of_item_as_string($id_or_model)
    {
        return $this->get_names_of_item($id_or_model, 'string');
    }

    public function get_names_of_item_as_array($id_or_model)
    {
        return $this->get_names_of_item($id_or_model, 'array');
    }

    public function get_names_of_item($id_or_model_item, $format = false)
    {
        $item = $this->_instantiate_if_id($id_or_model_item, Item::class);

        $names = Name::where('item_id', $item->id)->get();

        if( $format )
        {
            if( $format === 'string')
                $formated = '';
            else
                $formated = [];

            foreach( $names as $key => $name )
            {
                if( $format === 'string')
                {
                    if( $key != 0 )
                        $formated .= ', ';
                    $formated .= $name->label;
                }
                else
                {
                    $formated[] = $name->toArray();
                }
            }
        }

        return ( $format ) ? $formated : $names;
    }
}
