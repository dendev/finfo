<?php
namespace App\Services;


class BrainManagerService
{
    public function quiz_is_available($entity, $mode)
    {
        $is_available = false;

        $available_entities = [
            'vocabulary' => ['random'],
            'name' => ['random'],
            'image' => ['random'],
            'family' => ['random']
        ];

        if( array_key_exists($entity, $available_entities) && in_array($mode, $available_entities[$entity] ) )
            $is_available = true;

        return $is_available;
    }
}
