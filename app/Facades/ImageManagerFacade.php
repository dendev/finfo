<?php
namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class ImageManagerFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'image_manager';
    }
}
