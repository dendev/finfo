<?php
namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class NameManagerFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'name_manager';
    }
}
