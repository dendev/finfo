<?php
namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class BrainManagerFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'brain_manager';
    }
}
