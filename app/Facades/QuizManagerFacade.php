<?php
namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class QuizManagerFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'quiz_manager';
    }
}
