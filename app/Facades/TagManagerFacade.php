<?php
namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class TagManagerFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'tag_manager';
    }
}
