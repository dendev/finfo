<?php
namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class ComponentManagerFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'component_manager';
    }
}
