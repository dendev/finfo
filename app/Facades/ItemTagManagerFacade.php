<?php
namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class ItemTagManagerFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'item_tag_manager';
    }
}
