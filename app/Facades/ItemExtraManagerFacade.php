<?php
namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class ItemExtraManagerFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'item_extra_manager';
    }
}
