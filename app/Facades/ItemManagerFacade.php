<?php
namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class ItemManagerFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'item_manager';
    }
}
