<?php
namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class VocabularyManagerFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'vocabulary_manager';
    }
}
