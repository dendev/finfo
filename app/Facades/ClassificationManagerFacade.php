<?php
namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class ClassificationManagerFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'classification_manager';
    }
}
