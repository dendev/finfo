import 'bootstrap/dist/css/bootstrap.min.css';
import "@fortawesome/fontawesome-free/css/all.min.css";
import "../resources/js/components/style.css"


export const parameters = {
    actions: {
        argTypesRegex: "^on[A-Z].*"
    },
    backgrounds: {
        default: 'finfo',
        values: [
            {
                name: 'twitter',
                value: '#00aced',
            },
            {
                name: 'facebook',
                value: '#3b5998',
            },
            {
                name: 'finfo',
                value: '#F0E2D0',
            },
        ],
    },
}
