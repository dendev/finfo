<?php

use App\Http\Controllers\PageController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('home', [PageController::class, 'home'])->name('pages.home');
Route::get('plant', [PageController::class, 'plant'])->name('pages.plant');
Route::get('quiz_vocabulary', [PageController::class, 'quiz_vocabulary'])->name('pages.quiz_vocabulary');
Route::get('quiz_name', [PageController::class, 'quiz_name'])->name('pages.quiz_name');
Route::get('quiz_image', [PageController::class, 'quiz_image'])->name('pages.quiz_image');
Route::get('quiz_family', [PageController::class, 'quiz_family'])->name('pages.quiz_family');
Route::get('vocabulary', [PageController::class, 'vocabulary'])->name('pages.vocabulary');

Route::get('/', function () {
    return view('welcome');
});

Route::get('/test', function () {
    return view('pages.test');
});

