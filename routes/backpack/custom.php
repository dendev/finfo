<?php

// --------------------------
// Custom Backpack Routes
// --------------------------
// This route file is loaded automatically by Backpack\Base.
// Routes you generate using Backpack\Generators will be placed here.

Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => array_merge(
        (array) config('backpack.base.web_middleware', 'web'),
        (array) config('backpack.base.middleware_key', 'admin')
    ),
    'namespace'  => 'App\Http\Controllers\Admin',
], function () { // custom admin routes
    Route::crud('classification', 'ClassificationCrudController');
    Route::crud('item', 'ItemCrudController');
    Route::crud('name', 'NameCrudController');
    Route::crud('vocabulary', 'VocabularyCrudController');
    Route::crud('item_type', 'ItemTypeCrudController');
    Route::crud('item_extra', 'ItemExtraCrudController');
    Route::crud('image', 'ImageCrudController');
    Route::crud('tag', 'TagCrudController');
    Route::crud('item_tag', 'ItemTagCrudController');
    Route::crud('item_extra', 'ItemExtraCrudController');
}); // this should be the absolute last line of this file
