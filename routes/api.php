<?php

use App\Http\Controllers\Api\ComponentApiController;
use App\Http\Controllers\Api\QuizApiController;
use App\Http\Controllers\Api\VocabularyApiController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('vocabulary/{slug}', [VocabularyApiController::class, 'show'])->name('vocabulary.show');
Route::get('vocabulary', [VocabularyApiController::class, 'index'])->name('vocabulary.index');

Route::get('quiz/{entity}/{type}', [QuizApiController::class, 'show'])->name('quiz.show');

Route::get('component/{entity}/{action}/{extras?}', [ComponentApiController::class, 'action'])->name('component.action');


