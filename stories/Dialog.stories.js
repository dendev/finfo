import React from 'react';
import VocabularyItem from '../resources/js/components/Dialogs/Vocabulary';
import ItemItem from '../resources/js/components/Dialogs/Item';
export default {
  title: 'Finfo/Dialog',
};

const TemplateVocabulary = (args) => <VocabularyItem {...args} />;
export const Vocabulary = TemplateVocabulary.bind({});
Vocabulary.args = {
    isOpen: true,
    onClose: () => console.log('close'),
    selected: {
        id: 3,
        label: "acidiphile",
        slug: "acidiphile",
        description: "se dit d'une plante croissant préférentiellement sur un substrat acide.",
        image_id: null,
        created_at: "2020-12-22T20:05:25.000000Z",
        updated_at: "2020-12-22T20:05:25.000000Z",
        vocabulary_words: []
    }
};

const TemplateItem = (args) => <ItemItem {...args} />;
export const Item = TemplateItem.bind({});
Item.args = {
    isOpen: true,
    onClose: () => console.log('close'),
    selected:
        {
            id: 1,
            label: "Viola reichenbachiana",
            slug: "viola-reichenbachiana",
            description: null,
            names: [
                {
                    id: 1,
                    label: "Violette des bois",
                    slug: "violette-des-bois",
                    type: "fr",
                    item_id: 1,
                    created_at: "2021-03-26T20:47:14.000000Z",
                    updated_at: "2021-03-26T20:47:14.000000Z"
                }
            ],
            images: [
                {
                    "original": "http:\/\/finfo.local\/storage\/images\/plants\/viola_reichenbachiana_01.jpeg",
                    "thumbnail": "http:\/\/finfo.local\/storage\/thumbnails\/default\/4zp\/94ly2jf8c44ssgc00ogkkw.jpeg?crop=120x120&p=%2Fstorage%2Fimages%2Fplants%2Fviola_reichenbachiana_01.jpeg&s=p"
                }
            ],
            classifications: [
                {
                    id: 21,
                    label: "Viola reichenbachiana",
                    slug: "viola-reichenbachiana",
                    description: null,
                    level: "species",
                    level_fr: "Espèce",
                    parent_id: 6,
                    lft: null,
                    rgt: null,
                    depth: null,
                    created_at: "2021-03-26T20:47:14.000000Z",
                    updated_at: "2021-03-26T20:47:14.000000Z"
                },
                {
                    id: 6,
                    label: "Viola",
                    slug: "viola",
                    description: "",
                    level: "genus",
                    level_fr: "Genre",
                    parent_id: 5,
                    lft: null,
                    rgt: null,
                    depth: null,
                    created_at: "2021-03-06T20:57:27.000000Z",
                    updated_at: "2021-03-06T20:57:27.000000Z"
                },
                {
                    id: 5,
                    label: "Violaceae",
                    slug: "violaceae",
                    description: "",
                    level: "family",
                    level_fr: "Famille",
                    parent_id: 4,
                    lft: null,
                    rgt: null,
                    depth: null,
                    created_at: "2021-03-06T20:57:27.000000Z",
                    updated_at: "2021-03-06T20:57:27.000000Z"
                },
                {
                    id: 4,
                    label: "Malpighiales",
                    slug: "malpighiales",
                    description: "",
                    level: "order",
                    level_fr: "Ordre",
                    parent_id: 3,
                    lft: null,
                    rgt: null,
                    depth: null,
                    created_at: "2021-03-06T20:57:27.000000Z",
                    updated_at: "2021-03-06T20:57:27.000000Z"
                },
                {
                    id: 3,
                    label: "Magnoliopsida",
                    slug: "magnoliopsida",
                    description: "",
                    level: "class",
                    level_fr: "Classe",
                    parent_id: 2,
                    lft: null,
                    rgt: null,
                    depth: null,
                    created_at: "2021-03-06T20:57:27.000000Z",
                    updated_at: "2021-03-06T20:57:27.000000Z"
                },
                {
                    id: 2,
                    label: "Magnoliophyta",
                    slug: "magnoliophyta",
                    description: "",
                    level: "phylum",
                    level_fr: "Embranchement",
                    parent_id: 1,
                    lft: null,
                    rgt: null,
                    depth: null,
                    created_at: "2021-03-06T20:57:27.000000Z",
                    updated_at: "2021-03-06T20:57:27.000000Z"
                },
                {
                    id: 1,
                    label: "Plantae",
                    slug: "plantae",
                    description: "",
                    level: "kingdom",
                    level_fr: "Règne",
                    parent_id: null,
                    lft: null,
                    rgt: null,
                    depth: null,
                    created_at: "2021-03-06T20:57:27.000000Z",
                    updated_at: "2021-03-06T20:57:27.000000Z"
                }
            ],
            group: "V",
            created_at: "2021-03-26T20:47:14.000000Z",
            updated_at: "2021-03-26T20:47:14.000000Z"
        }
};
