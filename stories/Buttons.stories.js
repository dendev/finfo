import React from 'react';

import StopItem from '../resources/js/components/Buttons/Stop';
import FlipItem from '../resources/js/components/Buttons/Flip';
import NextItem from '../resources/js/components/Buttons/Next';
import QuizItem from '../resources/js/components/Buttons/Quiz';

export default {
  title: 'Finfo/Buttons',
};


const TemplateStop = (args) => <StopItem {...args} />;
export const Stop = TemplateStop.bind({});
Stop.args = {
};

const TemplateFlip= (args) => <FlipItem {...args} />;
export const Flip = TemplateFlip.bind({});
Flip.args = {
};

const TemplateNext= (args) => <NextItem {...args} />;
export const Next = TemplateNext.bind({});
Next.args = {
};

const TemplateQuiz = (args) => <QuizItem {...args} />;
export const Quiz = TemplateQuiz.bind({});
Quiz.args = {
    stop_is_visible: true,
    flip_is_visible: true,
    next_is_visible: true,
};
