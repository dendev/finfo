import React from 'react';

import QuizItem from '../resources/js/components/Items/Quiz';
import ElementItem from '../resources/js/components/Items/Element';
import SearchItem from '../resources/js/components/Items/Search';
import AlphapaginationItem from "../resources/js/components/Items/Alphapagination";
import GalleryItem from "../resources/js/components/Items/Gallery";
import ExtraItem from '../resources/js/components/Items/Extra';

export default {
  title: 'Finfo/Items',
};

const TemplateQuiz = (args) => <QuizItem {...args} />;
export const Quiz = TemplateQuiz.bind({});
Quiz.args = {
    label: "Botanique",
    question: {
        label: "acidiphile",
        id: 3
    },
    responses: [
        {
            label: "se dit d'un organe floral continuant à croitre après la floraison ( ex: calice accrescent )",
            id: 2,
            is_correct_response: false
        },
        {
            label: "[TEST@accresente] texte [TEST@accresente] texte",
            id: 5,
            is_correct_response: false
        },
        {
            label: "se dit d'une feuille linéaire, rigide et à sommet aigu ( si sommet piquant, on parle alors d'\"aiguilles\"",
            id: 4,
            is_correct_response: false
        },
        {
            label: "se dit d'une plante croissant préférentiellement sur un substrat acide.",
            id: 3,
            is_correct_response: true
        }
    ]
};

const TemplateElement = (args) => <ElementItem {...args} />;
export const Element = TemplateElement.bind({});
Element.args = {
    selected: {
        id: 3,
        label: "acidiphile",
        slug: "acidiphile",
        description: "se dit d'une plante croissant préférentiellement sur un substrat acide.",
        image_id: null,
        created_at: "2020-12-22T20:05:25.000000Z",
        updated_at: "2020-12-22T20:05:25.000000Z",
        vocabulary_words: []
    },
    type: 'vocabulary'
};

const TemplateSearch = (args) => <SearchItem {...args} />;
export const Search = TemplateSearch.bind({});
Search.args = {
    type: 'vocabulary',
    items: [
        {
            id: 2,
            label: "accrescent(e)",
            slug: "accrescent",
            description: "se dit d'un organe floral continuant à croitre après la floraison ( ex: calice accrescent )",
            image_id: null,
            created_at: "2021-01-29T14:31:14.000000Z",
            updated_at: "2021-01-29T14:31:14.000000Z",
            group: "A",
            vocabulary_words: []
        },
        {
            id: 3,
            label: "acidiphile",
            slug: "acidiphile",
            description: "se dit d'une plante croissant préférentiellement sur un substrat acide.",
            image_id: null,
            created_at: "2021-01-29T14:31:14.000000Z",
            updated_at: "2021-01-29T14:31:14.000000Z",
            group: "A",
            vocabulary_words: []
        },
    ]
};

const TemplateAlphapagination = (args) => <AlphapaginationItem {...args} />;
export const Alphapagination = TemplateAlphapagination.bind({});
Alphapagination.args = {
    elements: [
        {
            label: 'A',
            anchor: 'a'
        },
        {
            label: 'B',
            anchor: 'b'
        },
    ]
}

const TemplateGallery = (args) => <GalleryItem {...args} />;
export const Gallery = TemplateGallery.bind({});
Gallery.args = {
    images: [
        {
            original: 'https://picsum.photos/id/1018/1000/600/',
            thumbnail: 'https://picsum.photos/id/1018/250/150/',
        },
        {
            original: 'https://picsum.photos/id/1015/1000/600/',
            thumbnail: 'https://picsum.photos/id/1015/250/150/',
        },
        {
            original: 'https://picsum.photos/id/1019/1000/600/',
            thumbnail: 'https://picsum.photos/id/1019/250/150/',
        },
    ]
}

const TemplateExtra = (args) => <ExtraItem {...args} />;
export const Extra = TemplateExtra.bind({});
Extra.args = {
    "extras": [
        {
            id: 19,
            label: "Caractéritstiques",
            slug: "characteristics",
            description: "Eléments à observer pour aider à l'identification",
            fields: [
                {
                    name: "characteristic",
                    type: "text",
                    label: "Caractéristique",
                    value: "Sans épines",
                    wrapper: {
                        class: "form-group col-md-4"
                    }
                },
                {
                    name: "characteristic",
                    type: "text",
                    label: "Caractéristique",
                    value: "Fruits noirs",
                    wrapper: {
                        class: "form-group col-md-4"
                    }
                }
            ],
            item_id: 28,
            created_at: "2021-04-15T09:34:48.000000Z",
            updated_at: "2021-04-15T09:34:48.000000Z"
        }
    ],
};
