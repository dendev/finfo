import React from 'react';


import QuestionItem from '../resources/js/components/Cards/Question';
import ResponseItem from '../resources/js/components/Cards/Response';
import VocabulariesItem from '../resources/js/components/Cards/Vocabularies';
import ItemsItem from '../resources/js/components/Cards/Items';

export default {
  title: 'Finfo/Cards',
};

const TemplateQuestion = (args) => <QuestionItem {...args} />;
export const Question = TemplateQuestion.bind({});
Question.args = {
    question: {
        label: "acidiphile",
        vocabulary_id: 3
    },
    responses: [
        {
            label: "se dit d'un organe floral continuant à croitre après la floraison ( ex: calice accrescent )",
            vocabulary_id: 2,
            is_correct_response: false
        },
        {
            label: "[TEST@accresente] texte [TEST@accresente] texte",
            vocabulary_id: 5,
            is_correct_response: false
        },
        {
            label: "se dit d'une feuille linéaire, rigide et à sommet aigu ( si sommet piquant, on parle alors d'\"aiguilles\"",
            vocabulary_id: 4,
            is_correct_response: false
        },
        {
            label: "se dit d'une plante croissant préférentiellement sur un substrat acide.",
            vocabulary_id: 3,
            is_correct_response: true
        }
    ]
};

const TemplateResponse = (args) => <ResponseItem {...args} />;
export const Response = TemplateResponse.bind({});
Response.args = {
    question: {
        label: "acidiphile",
        vocabulary_id: 3
    },
    response : {
        label: "se dit d'une plante croissant préférentiellement sur un substrat acide.",
        vocabulary_id: 3,
        is_correct_response: true
    },
    is_correct: false
};

const TemplateVocabularies = (args) => <VocabulariesItem {...args} />;
export const Vocabularies = TemplateVocabularies.bind({});
Vocabularies.args = {
    vocabularies: {
        "A": [
            {
                id: 3,
                label: "acidiphile",
                slug: "acidiphile",
                description: "se dit d'une plante croissant préférentiellement sur un substrat acide.",
                image_id: null,
                created_at: "2020-12-22T20:05:25.000000Z",
                updated_at: "2020-12-22T20:05:25.000000Z",
                vocabulary_words: []
            },
            {
                id: 21,
                label: "apex(un)",
                slug: "apexun",
                description: "sommet d'une tige ou d'un organe.",
                image_id: null,
                created_at: "2020-12-22T20:05:25.000000Z",
                updated_at: "2020-12-22T20:05:25.000000Z",
                vocabulary_words: []
            }
        ],
        "B": [
            {
                id: 37,
                label: "baie(une)",
                slug: "baie",
                description: "fruit charnu indéhiscent, à une ou plus gén. plusieurs graines libres ( 'pépins' ).",
                image_id: null,
                created_at: "2020-12-23T07:27:23.000000Z",
                updated_at: "2020-12-23T07:27:23.000000Z",
                vocabulary_words: []
            },
        ]
    }
};

const TemplateItems = (args) => <ItemsItem {...args} />;
export const Items = TemplateItems.bind({});
Items.args = {
    items: {
        "D": [
            {
                id: 3,
                label: "Draba verna",
                slug: "draba-verna",
                description: null,
                item_type_id: 1,
                classification_id: 23,
                group: "D",
                created_at: "2021-03-26T20:47:14.000000Z",
                updated_at: "2021-03-26T20:47:14.000000Z"
            }
        ],
        "E": [
            {
                id: 2,
                label: "Euphorbia amygdaloides",
                slug: "euphorbia-amygdaloides",
                description: null,
                item_type_id: 1,
                classification_id: 22,
                group: "E",
                created_at: "2021-03-26T20:47:14.000000Z",
                updated_at: "2021-03-26T20:47:14.000000Z"
            }
        ],
        "G": [
            {
                id: 4,
                label: "Geranium sanguineum",
                slug: "geranium-sanguineum",
                description: null,
                item_type_id: 1,
                classification_id: 20,
                group: "G",
                created_at: "2021-03-26T20:47:14.000000Z",
                updated_at: "2021-03-26T20:47:14.000000Z"
            }
        ],
        "V": [
            {
                id: 1,
                label: "Viola reichenbachiana",
                slug: "viola-reichenbachiana",
                description: null,
                item_type_id: 1,
                classification_id: 21,
                group: "V",
                created_at: "2021-03-26T20:47:14.000000Z",
                updated_at: "2021-03-26T20:47:14.000000Z"
            }
        ]
    }
}
