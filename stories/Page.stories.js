import React from 'react';

import QuizItem from "../resources/js/components/Pages/Quiz";
import VocabularyItem from "../resources/js/components/Pages/Vocabulary";
import PlantItem from "../resources/js/components/Pages/Plant";

export default {
  title: 'Finfo/Page',
};

const TemplateQuiz = (args) => <QuizItem {...args} />;
export const Quiz = TemplateQuiz.bind({});
Quiz.args = {
};

const TemplateVocabulary  = (args) => <VocabularyItem {...args} />;
export const Vocabulary = TemplateVocabulary.bind({});
Vocabulary.args = {
};

const TemplatePlant = (args) => <PlantItem {...args} />;
export const Plant = TemplatePlant.bind({});
Plant.args = {
};
