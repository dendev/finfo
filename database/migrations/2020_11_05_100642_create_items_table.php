<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->id();
            $table->string('label')->unique()->comment('latin name');
            $table->string('slug')->unique()->comment('slug of label');
            $table->text('description')->nullable();
            $table->foreignId('item_type_id')->constrained();
            $table->foreignId('classification_id')->constrained()->onDelete('cascade');
            $table->string('group');
            $table->string('group_2');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plants');
    }
}
