<?php

namespace Database\Seeders;

use App\Models\Item;
use App\Models\ItemType;
use App\Models\Name;
use App\Models\Plant;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class PlantSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Schema::disableForeignKeyConstraints();
        DB::table('items')->truncate(); // dangerous !
        DB::table('names')->truncate(); // dangerous ! // cross items !!
        DB::table('images')->truncate(); // dangerous ! // cross items !!
        \Schema::enableForeignKeyConstraints();


        $item_type = ItemType::where('slug', 'botanic')->first();
        $item_type_id = $item_type->id;

        // get datas
        $datas_files = [
            'plants_01.json',
            'plants_02.json',
            'plants_04.json',
        ];

        $datas = [];
        foreach( $datas_files as $file )
        {
            $file_path = base_path("/database/seeders/datas/$file");
            $file_data = file_get_contents($file_path);
            $file_data = json_decode($file_data, true);
            $datas = array_merge($datas, $file_data);
        }

        // create
        foreach ($datas as $data)
        {
            $images = array_key_exists('images', $data) ? $data['images'] : false;
            $tags = array_key_exists('tags', $data) ? $data['tags'] : false;

            \ItemManager::create($data['label'], $data['names'], $data['description'], $data['type'], $data['classifications'], $images, $tags);
        }
    }
}
