<?php

namespace Database\Seeders;

use App\Models\ItemType;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RoleSeeder::class);
        $this->call(AdminSeeder::class);
        $this->call(SettingSeeder::class);
        $this->call(ItemTypeSeeder::class);
        $this->call(VocabularySeeder::class);
        $this->call(ClassificationSeeder::class);
        $this->call(PlantSeeder::class); // is an Item
        $this->call(ItemExtraSeeder::class);
    }
}
