<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->delete();

        $datas = [
            ['name' => 'admin', 'guard_name' => 'web', 'permissions' => []],
        ];

        foreach ($datas as $data) {
            $role = new Role();
            $role->name = $data['name'];
            $role->guard_name = $data['guard_name'];
            $role->created_at = now();
            $role->updated_at = now();
            $role->save();

            $permissions = $data['permissions'];
            foreach( $permissions as $permission )
            {
                $role->givePermissionTo($permission);
            }
        }
    }
}
