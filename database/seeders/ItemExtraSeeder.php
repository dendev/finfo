<?php

namespace Database\Seeders;

use App\Models\Item;
use App\Models\ItemType;
use App\Models\Name;
use App\Models\Plant;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Role;

class ItemExtraSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Schema::disableForeignKeyConstraints();
        DB::table('item_extras')->truncate(); // dangerous !
        \Schema::enableForeignKeyConstraints();

        // get datas
        $datas_files = [
            'extras_01.json',
            'extras_02.json',
            'extras_04.json',
        ];

        $datas = [];
        foreach( $datas_files as $file )
        {
            $file_path = base_path("/database/seeders/datas/$file");
            $file_data = file_get_contents($file_path);
            $file_data = json_decode($file_data, true);
            $datas = array_merge($datas, $file_data);
        }

        // create
        foreach ($datas as $data)
        {
            $slug = array_key_exists('slug', $data) ? $data['slug'] : \Str::slug($data['label']);
            $item = Item::where('slug', Str::slug($data['item']))->first();

            \ItemExtraManager::create($data['label'], $slug, $data['description'], $data['fields'], $item);
        }
    }
}
