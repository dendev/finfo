<?php

namespace Database\Seeders;

use App\Models\Classification;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class ClassificationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Schema::disableForeignKeyConstraints();
        DB::table('classifications')->truncate();
        \Schema::enableForeignKeyConstraints();

        $datas = [
            // Kingdom
            [
                'label' => 'Plantae',
                'description' => '',
                'level' => 'kingdom',
                'parent_id' => null,
            ],
            // Phylum
            [
                'label' => 'Magnoliophyta',
                'description' => '',
                'level' => 'phylum',
                'parent_id' => 1,
            ],
            // Class
            [
                'label' => 'Magnoliopsida',
                'description' => '',
                'level' => 'Class',
                'parent_id' => 2,
            ],
            // Order
            [
                'label' => 'Malpighiales',
                'description' => '',
                'level' => 'order',
                'parent_id' => 3,
            ],
            // Family
            [
                'label' => 'Violaceae',
                'description' => '',
                'level' => 'family',
                'parent_id' => 4,
            ],
            // Genus
            [
                'label' => 'Viola',
                'description' => '',
                'level' => 'genus',
                'parent_id' => 5,
            ],
            // Species
            [
                'label' => 'Viola reichenbachiana',
                'description' => '',
                'level' => 'species',
                'parent_id' => 6,
            ],
        ];

        foreach ($datas as $data)
        {
            $classification = new Classification();
            $classification->label = $data['label'];
            $classification->description = $data['description'];
            $classification->level = $data['level'];
            $classification->parent_id= $data['parent_id'];
            $classification->save();
        }
    }
}
