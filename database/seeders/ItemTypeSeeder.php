<?php

namespace Database\Seeders;

use App\Models\ItemType;
use App\Models\Name;
use App\Models\Plant;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class ItemTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Schema::disableForeignKeyConstraints();
        DB::table('item_types')->truncate();
        \Schema::enableForeignKeyConstraints();


        $datas = [
            [
                'label' => 'Botanique',
                'slug' => 'botanic',
                'icon' => 'fas fa-seedling',
                'color' => '#008265',
                'description' => '',
            ],
            [
                'label' => 'Oiseaux',
                'slug' => 'bird',
                'icon' => 'fas fa-dove',
                'color' => '#008265',
                'description' => '',
            ],
            [
                'label' => 'Poissons',
                'slug' => 'fish',
                'icon' => 'fas fa-fish',
                'color' => '#008265',
                'description' => '',
            ],
            [
                'label' => 'Batraciens',
                'slug' => 'batracien',
                'icon' => 'fas fa-frog',
                'color' => '#008265',
                'description' => '',
            ],
            [
                'label' => 'Insectes',
                'slug' => 'bug',
                'icon' => 'fas fa-bug',
                'color' => '#008265',
                'description' => '',
            ],
            [
                'label' => 'Dinosaures',
                'slug' => 'dinosaure',
                'icon' => 'fas fa-dragon',
                'color' => '#008265',
                'description' => '',
            ],
        ];

        foreach ($datas as $data)
        {
            $item_type = new ItemType();
            $item_type->label  = $data['label'];
            $item_type->slug   = $data['slug'];
            $item_type->icon   = $data['icon'];
            $item_type->color  = $data['color'];
            $item_type->description = $data['description'];
            $item_type->save();
        }
    }
}
