<?php

namespace Database\Seeders;

use Backpack\Settings\app\Models\Setting;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('settings')->delete();

        //
        /*
        Setting::firstOrCreate([
            'key' => 'types_of_entry',
            'name' => 'Types possible des Fiches',
            'description' => 'Liste tous les types possible pour une fiches ( oiseaux, plantes, ... )',
            'value' => '[{"key": "bird", "value": "Oiseau" },{"key": "plants", "value": "Plante" }]',
            'field' => '{"name":"value","label":"Infos","type":"textarea"}',
            'active' => 1,
        ]);
        */
    }
}
