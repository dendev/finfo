<?php

namespace Database\Seeders;

use App\Models\ItemType;
use App\Models\Vocabulary;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;

class VocabularySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // remove
        Schema::disableForeignKeyConstraints();
        Vocabulary::truncate();
        Schema::enableForeignKeyConstraints();

        // datas
        $datas = [
            [
                'label' => "test",
                'description' => "se dit <b>test</b>d'une feuille [accrescente@accrescente] linéaire, [Accrescent@accrescente] rigide et à sommet aigu ( si sommet piquant, on parle alors d'\"aiguilles\"",
                'image' => false
            ],
            // A
            [
                'label' => "accrescent(e)",
                'description' => "se dit d'un organe floral continuant à croitre après la floraison ( ex: calice accrescent )",
                'image' => false
            ],
            [
                'label' => "acidiphile",
                'description' => "se dit d'une plante croissant préférentiellement sur un substrat acide.",
                'image' => false
            ],
            [
                'label' => "aciculaire",
                'description' => "se dit d'une feuille linéaire, rigide et à sommet aigu ( si sommet piquant, on parle alors d'\"aiguilles\"",
                'image' => false
            ],
            [
                'label' => "acuminé(e)",
                'description' => "organie terminé par une pointe étroite et progressivement effilée ( ex. feuille acuminée )",
                'image' => false
            ],
            [
                'label' => "adventice",
                'description' => "se dit d'une plante étrangère à la flore indigène et introduite involontairement ; dans le domaine agronomique, ce terme prend le sens péjoratif d'indésirable ( syn. 'mauvaise herbes' ).",
                'image' => false
            ],
            [
                'label' => "adventive",
                'description' => "se dit d'une racine se formatn postérieurement à la germination de la graine, sur une partie quelconque de la plante ( e. sur la tige ou plus rarement, à l'aisselle des feuilles ).",
                'image' => false
            ],
            [
                'label' => "aigrette(une)",
                'description' => "ensemble de poils ou de soies surmontant un fruit ou une graine ( ex. sur le fruit du pissenlit, Taraxacum sp. ).",
                'image' => false
            ],
            [
                'label' => "aiguille",
                'description' => "feuille aciculaire à sommet piquant.",
                'image' => false
            ],
            [
                'label' => "aiguillon(un)",
                'description' => "expansion piquante de l'épiderme ou de l'écorce d'une tige, se détachant aisément en laissant une cicatrice superficielle. Par extension, petites points piquantes portées par des organes tels que les fruits.",
                'image' => false
            ],
            [
                'label' => "aile(une)",
                'description' => "(i) expansion membraneuse, plus ou moins large de divers organes ( tige, fruit, graine ) ; (ii) pétales latéraux de la fleur des Fabacèes ; (iii) sépales latéraux de la fleur des Polygalacées.",
                'image' => false
            ],
            [
                'label' => "alène",
                'description' => "se dit d'une disposition de feuilles linéraies, rigides, arquées et effilées à l'extrémité.",
                'image' => false
            ],
            [
                'label' => "alternes",
                'description' => "se dit d'organes insérés isolément sur un axe, à des niveaux différents ( ex. feuilles alternes, ant. opposés ).",
                'image' => false
            ],
            [
                'label' => "amplexicaule",
                'description' => "se dit d'une feuille ou d'une bractée sessile dont la base élargie embrasse la tige du rameau.",
                'image' => false
            ],
            [
                'label' => "androcée(un)",
                'description' => "ensemble des étamines d'une fleur.",
                'image' => false
            ],
            [
                'label' => "annuel(le)",
                'description' => "se dit d'un végétal dont le cycle vital, de la germination à la dispersion des graines, se déroule au cours d'une même année, parfois en quelques semaines. Le système racinaire d'une plante annuelle est généralement plus grêle que celui d'une plante vivace qui sera gén. robuste.",
                'image' => false
            ],
            [
                'label' => "anguleux(se)",
                'description' => "se dit d'un organe présentant des angles, en oppositions à ceux cylindriques ( ex. tige anguleuse, lorsqu'on fait rouler la tige entre ses doigts, on sent des arêtes, ant. cylindrique ).",
                'image' => false
            ],
            [
                'label' => "anthère(une)",
                'description' => "partie apicle de l'étamine, constituée gén. par deux thèques ( unies par le connectif ) renfermant les sacs polliniques, contenant les grains de pollen.",
                'image' => false
            ],
            [
                'label' => "anthèse(une)",
                'description' => "moment correspondant à l'ouverture des boutons floraux ; par extension, à l'ouverture de la fleur.",
                'image' => false
            ],
            [
                'label' => "antiligule(une)",
                'description' => "petit appendice membraneux prolongeant la gaine foliaire et opposé à la ligule, présent chez les Poacées et Cypéracées.",
                'image' => false
            ],
            [
                'label' => "apex(un)",
                'description' => "sommet d'une tige ou d'un organe.",
                'image' => false
            ],
            [
                'label' => "apical(e)",
                'description' => "situé au sommet d'un organe.",
                'image' => false
            ],
            [
                'label' => "apiculé(e)",
                'description' => "se dit d'une organe dont le sommet se rétrécit brusquement en une pointe courte et peu aigüe.",
                'image' => false
            ],
            [
                'label' => "apomictique",
                'description' => "se dit d'une espèce ayant comme mode de reproduction l'apoximie.",
                'image' => false
            ],
            [
                'label' => "apomixie(une)",
                'description' => "mode de reproduction asexué caractérisé par la formation de graines sans qu'il y ait eu fécondation ( mitose de certaines cellules du sac embryonnaire ), les espèces sont dites alors 'apomictiques'.",
                'image' => false
            ],
            [
                'label' => "apprimé(e)",
                'description' => "se dit d'un organe appliqué contre un autre, mais non soudé à celui-ci ( ex. poils apprimés ).",
                'image' => false
            ],
            [
                'label' => "aquatique",
                'description' => "se dit d'une plante dont la plus grande partie des organes végétatifs sont soit submergés soit étalés à la surface de l'eau.",
                'image' => false
            ],
            [
                'label' => "arête(une)",
                'description' => "(i) longue pointe étroite et raide, prolongeant ou terminant un organe, (ii) anle saillant formé par deux faces contigües d'un organe.",
                'image' => false
            ],
            [
                'label' => "aristé(e)",
                'description' => "terminé par une arête.",
                'image' => false
            ],
            [
                'label' => "aromatique",
                'description' => "se dit d'une plante ou d'un organe contenant des composés volatiles qui lui conférent une odeur agréable.",
                'image' => false
            ],
            [
                'label' => "arqué(e)",
                'description' => "se dit d'un organe ou d'une portion d'organe courbée à la manière d'un arc ( ex. sur une feuille, nervure arquées ).",
                'image' => false
            ],
            [
                'label' => "article(un)",
                'description' => "portion d'organe comprise entre deux articulations, entre deux rétrécissements ou, éventuellement, entre deux parois.",
                'image' => false
            ],
            [
                'label' => "articulé(e)",
                'description' => "se dit d'un organe formé d'articles ( ex. fruits articulés, silique de la ravenelle, Raphanus raphanistrum ).",
                'image' => false
            ],
            [
                'label' => "ascendant(e)",
                'description' => "se dit d'un organe, gén. tige ou rameau, couché à la base et redressé plus haut à la verticale.",
                'image' => false
            ],
            [
                'label' => "asymétrique",
                'description' => "se dit d'un organe qui ne présente pas d'axe de sym. ( ex. feuille asymétrique ).",
                'image' => false
            ],
            [
                'label' => "auriculé(e)",
                'description' => "se dit d'une feuille pourvue d'oreillettes ou de deux lobes à la base.",
                'image' => false
            ],
            // B
            [
                'label' => "baie(une)",
                'description' => "fruit charnu indéhiscent, à une ou plus gén. plusieurs graines libres ( 'pépins' ).",
                'image' => false
            ],
            [
                'label' => "basilaire",
                'description' => "se dit d'un organe attaché à la base de la plante ou à la base d'un autre organe ( ex. feuilles basilaires ).",
                'image' => false
            ],
            [
                'label' => "bec(un)",
                'description' => "pointe terminale d'un fruit. Chez les Astéracées, partie filiforme qui surmonte l'akène et porte l'aigrette.",
                'image' => false
            ],
            [
                'label' => "bifide",
                'description' => "se dit d'un organe fendu en deux parties ( ex. stigmate bifide ).",
                'image' => false
            ],
            [
                'label' => "bilatéral(e)",
                'description' => "se dit d'un organe ne possédant qu'un seul axe de sym. ( ex. fleur à sym. bilatérale des Lamiacèes, Scrophulariacées, Orchidacées, ant. radiaire ).",
                'image' => false
            ],
            [
                'label' => "bipare",
                'description' => "se dit d'une cyme où les pédicelles latéraux ( développés en dessous de l'axe principal terminé par une fleur ) sont opposés deux à deux.",
                'image' => false
            ],
            [
                'label' => "bisannuel(le)",
                'description' => "se dit d'un végétal dont le cycle vital, de la germinantion à la dispersion des graines, nécessite duex années consécutives séparées par un hiver. Une telle plante ne fleurit donc pas au cours de la première année.",
                'image' => false
            ],
            [
                'label' => "boulaie(une)",
                'description' => "dorêt dominée par les bouleaux.",
                'image' => false
            ],
            [
                'label' => "bractée(une)",
                'description' => "organe foliacé, gén. réduit ( 'mini-feuille' ), parfois écailleux, sous-tendant une fleur isolée, le pédoncule d'une inflorescence ou le pédicelle d'une fleur faisant partie d'une inflorescence.",
                'image' => false
            ],
            [
                'label' => "bractéole(une)",
                'description' => "petite bractée insérée sur un pédicelle.",
                'image' => false
            ],
            [
                'label' => "bulbe(un)",
                'description' => "tige modifiée, gén. souterraine et très contractée, portant un ou plusieurs bourgeons enveloppés d'écailles ; celles-ci sont soit épaisses et charnues ( ex. bulbe tuniqué comme l'ognion), soit membraneuse et minces autour d'un exe tubérisé ( ex. bulbe solide, comme ches le crocus ).",
                'image' => false
            ],
            [
                'label' => "bulbille(une)",
                'description' => "bourgeon transformé en un petit bulbe se détachant de la plante-mère et destiné à la multiplication végétative ; elle peuvent se former à l'aisselle des feuilles, dans une inflorescence à la place des fleurs, ou sur des organes souterrains.",
                'image' => false
            ],
            // C
            [
                'label' => "caduc(que)",
                'description' => "se dit d'un organe qui se détache spontanément soit peu après sa formation ( ex. sépales caducs ), soit suivant un rythme annuel ( ex. feuilles caduques).",
                'image' => false
            ],
            [
                'label' => "calaminaire",
                'description' => "se dit d'un site ( gén. pelouse) sur sols riches en métaux lourds ( surtout plomb et zinc ).",
                'image' => false
            ],
            [
                'label' => "calcicole",
                'description' => "se dit d'une plante croissant uniquement sur un substrat riche en calcaire.",
                'image' => false
            ],
            [
                'label' => "calice(un)",
                'description' => "verticille externe du périanthe, formé par les sépale gén. verts qui peuvent être libres ou soudés.",
                'image' => false
            ],
            [
                'label' => "calicule(un)",
                'description' => "(i) ensemble des stipules des sépales doublant le calice à sa périphérie ( Rosacées) ; (ii) ches les Astéracées, ensemble de bractées plus petites que celle de l'involucre normal extérieures à celui-ci ( syn. préférable: involucelle ).",
                'image' => false
            ],
            [
                'label' => "callosité(une)",
                'description' => "petit renflement, gén. de consistance dure, formé par certains organes.",
                'image' => false
            ],
            [
                'label' => "campanulé(e)",
                'description' => "se dit d'un calice ou d'une corole en forme de cloche( ex. fleur de la campagnule à feuilles rondes, Campanula rotundifolia ).",
                'image' => false
            ],
            [
                'label' => "canaliculé(e)",
                'description' => "se dit d'un organe creusé d'un petit sillon.",
                'image' => false
            ],
            [
                'label' => "cannelé(e)",
                'description' => "se dit d'un tronc, d'une tige, d'un fruit portant des côtes longitudinales parallèles, séparées par des sillons ( ex. tronc du charme, Carpinus betulus ).",
                'image' => false
            ],
            [
                'label' => "capitule(un)",
                'description' => "inflorescence formée de fleurs sessiles ou subsessiles, serrées les unse contre les autres au niveau du sommet gén. élargie du pédoncule floral ( réceptacle pourvu d'un involucre de bractées ), simulant parfois une fleur unique ( ex. inflorescences des Astéracées ou Dipsacacées ).",
                'image' => false
            ],
            [
                'label' => "capsule(une)",
                'description' => "fruit sec déhiscent, provenant de la soudure de deux ou plusieurs carpelles, s'ouvrant par des gentes en deux ou plusieurs valves, ou bien par dse dents ou des pores, pour libérer les grains ( ex. capsule du coquelicot, Papaver rhoeas ).",
                'image' => false
            ],
            [
                'label' => "carène(une)",
                'description' => "(i) saillie de section triangulaire disposée longitudinalement au dos de certains organes; (ii) pièce de la corole des Fabacées, provennant de l'accolement, ou plus rarement de la soudure, des deux pétales inférieurs.",
                'image' => false
            ],
            [
                'label' => "caréné(e)",
                'description' => "se dit d'un organe pourvu d'une carène.",
                'image' => false
            ],
            [
                'label' => "carpelle(un)",
                'description' => "chacun des éléments de base du gynécée ou pistil ; un carpelle comporte typiquement un ovaire, un style et un stigmate.",
                'image' => false
            ],
            [
                'label' => "cartilagineux(se)",
                'description' => "se dit d'un organe qui a la consistance d'un cartilage ( ex. bordure cartilagineuse d'une feuille ).",
                'image' => false
            ],
            [
                'label' => "caryopse(un)",
                'description' => "fruit sec, indéhiscent, propre aux Poacées, dont la graine est soudée à la paroi interne du fruit.",
                'image' => false
            ],
            [
                'label' => "casque(un)",
                'description' => "pièce ou ensemble de pièces du périanhte dressée(s) et recourbée(s) de telle façon qu'elle(s) rappelle(nt) la forme d'un casque militaire.",
                'image' => false
            ],
            [
                'label' => "caulinaire",
                'description' => "se dit d'un organe inséré sur la tige ( ex. feuilles caulinaires ).",
                'image' => false
            ],
            [
                'label' => "cespiteuse",
                'description' => "se dit d'un végétal qui forme une touffe compacte ( ex. la canche cespiteuse, Deschampsia cespitosa ).",
                'image' => false
            ],
            [
                'label' => "chaméphyte(un)",
                'description' => "forme biologique de plantes dont les tiges aériennes portent des bourgeons persistant durant l'hiver, situés gén. à moins de 50 cm de haut ; chaméphyte frutescent : petit buisson à tiges lignifiées, plus ou moins dressées ( ex. a myrtille , Vaccinium myrtillus ); chaméphyte herbacée : organes aériens herbacés, souvent plus ou moins appliqués contre le substrat ( ex. la petite pervence, Vinca minor ). ",
                'image' => false
            ],
            [
                'label' => "charnu(e)",
                'description' => "se dit d'un organe dont le parenchyme gonflé d'eau confère à celui ci une épaisseur inhabituelle ( environ 2mm ) et une consistance à la fois ferme et tendre ( ex. feuilles charnues, syn. succulent ).",
                'image' => false
            ],
            [
                'label' => "chaton(un)",
                'description' => "inflorescence cylindrique, souvent pendante, formée d'un axe allongé portan des fleurs unisexuées sessiles, ou subsessiles, à l'aiselle de bractées ; les fleurs sont insérées isolément ( chaton simple ) ou par petits groupes ( chaton composé ).",
                'image' => false
            ],
            [
                'label' => "chênaie(une)",
                'description' => "forêt dominée par des chênes ( Quercus spp. ).",
                'image' => false
            ],
            [
                'label' => "cilié(e)",
                'description' => "se dit d'un organe bordé de poils dressés ( cils ) disposés sur un rang",
                'image' => false
            ],
            [
                'label' => "cladode(un)",
                'description' => "rameau modifié, aplati et vert au point de simuler une feuille dans son aspect et sa fonction ( ex. le fragon faux houx, Ruscus aculeatus)",
                'image' => false
            ],
            [
                'label' => "cloison(une)",
                'description' => "séparation correspondant à la soudure des caprelles et délimitant, dans l'ovaire, puis le fruit, un certain nombre de loges, contenant les ovules et puis les graines.",
                'image' => false
            ],
            [
                'label' => "collet(un)",
                'description' => "zone de jonction de la tige et de la racine.",
                'image' => false
            ],
            [
                'label' => "composée",
                'description' => "se dit d'une organe formé de plusieurs éléments semblables : feuille composée = divisée en folioles, l'axe ( = rachis ) se différencie et tous les segmetns du limbe sont des folioles ( ex. le robinier, Robinia pseudoacacia ) ; inflorescence composée = inflorescence constitutée d'inflorescences ( ex. ombelles composées de beaucoup d'Apiacées ).",
                'image' => false
            ],
            [
                'label' => "concolore",
                'description' => "se dit d'un organe de couleur uniforme sur les 2 faces.",
                'image' => false
            ],
            [
                'label' => "cône(un)",
                'description' => "organe, chez les gymnispermes, formé d'un axe sur lequel son insérées de nombreuses écailles imbriquées portant soit des sacs polliniques ( cônes mâles), soit des ovules et à maturité des graines ( cônes femelles ).",
                'image' => false
            ],
            [
                'label' => "connivents(tes)",
                'description' => "se dit d'organes rapprochés par leurs bords, mais non soudés ( ex. anthères conniventes chez la bourrache, Borago officinalis ).",
                'image' => false
            ],
            [
                'label' => "corbeille(une)",
                'description' => "type de disposition des feuilles chez les Ptéridophytes, partant d'une base et incurvées vers l'extérieur ( rappelant la forme d'une corbeille ).",
                'image' => false
            ],
            [
                'label' => "cordé(e)",
                'description' => "se dit d'un organe dont la base est échancrée en forme de coeur.",
                'image' => false
            ],
            [
                'label' => "coriace",
                'description' => "se dit d'un organe résitant, mais flexible dont la structure rappelle celle du cuir.",
                'image' => false
            ],
            [
                'label' => "corole(une)",
                'description' => "verticille interne du périanthe, formé par les pétales, qui peuvent être libres ou soudés.",
                'image' => false
            ],
            [
                'label' => "corymbe(un)",
                'description' => "inflorescence indéterminée dont les fleurs se situent à peu près dans le même plan horizontal, mais qui sont portées par des pédicelles insérés à des niveaux différents et par conséquent, de longueur inégale.",
                'image' => false
            ],
            [
                'label' => "côte(une)",
                'description' => "crête longitudinale obtuse portée par certains organes tels les tiges, les fruits ( ex. sur les fruits de nombreuses Apiacées).",
                'image' => false
            ],
            [
                'label' => "couronne(une)",
                'description' => "organe tubulaire formé par la soudure des appendices internes des tépales dans le genre Narcissus, désigné par certains autours sous le nom de 'paracorole'.",
                'image' => false
            ],
            [
                'label' => "crènelé(e)",
                'description' => "se dit d'un organe bordé de dents larges, obtuses ou arrondies.",
                'image' => false
            ],
            [
                'label' => "cultivar(un)",
                'description' => "unité taxonomique subordonnée à l'espèce, inconue à l'état spontané, sélectionnée par l'homme et cultivée par celui-ci parce qu'elle présente un intérêt agronomique ou horticule ( souvent syn. variété ).",
                'image' => false
            ],
            [
                'label' => "cunéé(e)",
                'description' => "se dit d'un organe dont la base a la forme d'un coin ou d'un triangle renversé.",
                'image' => false
            ],
            [
                'label' => "cunéiforme",
                'description' => "se dit d'organe de forme cunéée",
                'image' => false
            ],
            [
                'label' => "cupule(une)",
                'description' => "organe en forme de petite coupe écailleuse ou ligneuse enveloppant la base de certains fruits ( ex. cupule de la fain du hêtre, Fagus sylvatica).",
                'image' => false
            ],
            [
                'label' => "cyathium(un)",
                'description' => "inflorescence élémentaire chez les genre Euphoribia, comportant une fleur femelle réduite à un ovaire pédicellé et plusieurs fleurs mâles réduites à une étamine, entourées de bractées soudées en une coupe pourvue, dans sa partie supérieure, alternativement de dents et de glandes.",
                'image' => false
            ],
            [
                'label' => "cyme",
                'description' => "inflorescence dont le premier axe formé est terminé par une fleur et a donc une croissance limitée, le processus se répétant pour les rameaux latéraux ; une cyme peut être unipare ( chaque ramification donnant un axe secondaire unique ) ou bipare ( chque ramification onnant deux aces secondaires ).",
                'image' => false
            ],
            [
                'label' => "cynorrhodon(un)",
                'description' => "faux-fruit du genre Rosa formé par l'accroissement, après la floraison, du réceptacle floral creusé en forme d'outre et devenant charnu.",
                'image' => false
            ],
            // D
            [
                'label' => "décombant(e)",
                'description' => "se dit d'un organe d'abord dressé et puis retombant vers le sol.",
                'image' => false
            ],
            [
                'label' => "décurrent(e)",
                'description' => "se dit d'un organe se prolongeant sous son point d'insertion en se rétracissant et en se soudant à l'organe adjacent ( ex. limbe foliaire décurrent : le limbe se prolonge sur la tige).",
                'image' => false
            ],
            [
                'label' => "décussées",
                'description' => "se dit de feuilles opposées dont les paires successives sont décalées de 90° ( ex. chez les Lamiacées ).",
                'image' => false
            ],
            [
                'label' => "déhiscent(e)",
                'description' => "se dit d'un fruit s'ouvrant spontanément à maturité ; la capsule, la gousse, le follicule, la pyxide, la silique et la silicule sont des fruits déhiscents ( ant. indéhiscent ).",
                'image' => false
            ],
            [
                'label' => "denté(e)",
                'description' => "se dit d'un organe bordé d'échancrures triangulaires relativement larges ( dents aigües ), l'organe peut être doublement denté, lorsqu'il est bordé de dents qui sont elles-mêmes dentées.",
                'image' => false
            ],
            [
                'label' => "denticulé(e)",
                'description' => "se dit d'un organe bordé de très petites dents aigües.",
                'image' => false
            ],
            [
                'label' => "diakène(un)",
                'description' => "ensemble de deux akènes ( ex. fruits des Apiacées ).",
                'image' => false
            ],
            [
                'label' => "dichotomique",
                'description' => "qualifie un mode de ramification par division en deux parties d'égale importance, qui se subdivisent elles-mêmmes en deux parties, égales, et ainsi de suite ( ex. ramifications chez le gui, Viscum album).",
                'image' => false
            ],
            [
                'label' => "didyme",
                'description' => "se dit d'un fruit formé de deux parties globuleuses, saillantes et soudées entre elles.",
                'image' => false
            ],
            [
                'label' => "didyname",
                'description' => "se dit d'un androcée de 4 étamines dont 2 sont plus longues que les 2 autres.",
                'image' => false
            ],
            [
                'label' => "digité(e)",
                'description' => "se dit d'une inflorescence ( épi ) dont les divisions sont disposées comme les doigts d'une main.",
                'image' => false
            ],
            [
                'label' => "dimorphisme(un)",
                'description' => "faculté de présenter deux formes différentes chez une me espèce ( ex. dimporphisme foliaire chez de nombreuses plantes aquatiques ou du lierre, Hedera helix).",
                'image' => false
            ],
            [
                'label' => "dioïque",
                'description' => "se dit d'une plante dont les fleurs mâles et femelles sont portées par des individus différents ( ex. le houx, Ilex aquifolium).",
                'image' => false
            ],
            [
                'label' => "distal(e)",
                'description' => "se dit d'un organe éloigné, écarté de la base ou de son lieu d'insertion ( ant. proximal).",
                'image' => false
            ],
            [
                'label' => "distiques",
                'description' => "se dit d'organes disposées sur un axe commun, placés sur deux rangs dans le même plan.",
                'image' => false
            ],
            [
                'label' => "drageon(un)",
                'description' => "tige prenant naissance sur une racine souterraine.",
                'image' => false
            ],
            [
                'label' => "drupe(une)",
                'description' => "fruit charnu indéhiscent, à graine gén. unique enfermée dans l'endocarpe sclérifiée ( le 'noyau' ).",
                'image' => false
            ],
            [
                'label' => "drupéole(une)",
                'description' => "chacune des petites drupes agglomérées dont l'ensemble forme un fruit composé ( ex. la mure, chez Rubus sp. ).",
                'image' => false
            ],
            // E
            [
                'label' => "écaille(une)",
                'description' => "petit organe rudimentaire ( gén. feuille modifiée ), mince, charnu ou lignifié ( simples appendices, poils ou feuilles transformées ) accompagnant ou protégeant certains organes.",
                'image' => false
            ],
            [
                'label' => "échancré(e)",
                'description' => "se dit d'un organe peu profondément entaillé à son sommet.",
                'image' => false
            ],
            [
                'label' => "elliptique",
                'description' => "se dit d'un organe en forme d'ellipse, c-à-d ayant sa plus grande largeur vers le milieu du grand axe.",
                'image' => false
            ],
            [
                'label' => "émarginé(e)",
                'description' => "se dit d'un organe très légèrement échancré(e) au sommet.",
                'image' => false
            ],
            [
                'label' => "embrassant(e)",
                'description' => "se dit d'une feuille dépourvue de pétiole et ond la base du limbe entoure complètement l'axe sur lequel elle est insérée ( syn. amplexicaule ).",
                'image' => false
            ],
            [
                'label' => "engainant(e)",
                'description' => "se dit d'une feuille qui forme une gaine autour d'un organe ( ex. limbe folaire engainant des Poacées ).",
                'image' => false
            ],
            [
                'label' => "entier(ère)",
                'description' => "se dit d'un organe dont le bord ne présente absolument aucune découpure, si petite soit-elle.",
                'image' => false
            ],
            [
                'label' => "entrenoeud(un)",
                'description' => "partie d'une tige ou d'un rameau comprise entre deux noeuds successifs.",
                'image' => false
            ],
            [
                'label' => "envahissante",
                'description' => "se dit d'une espèce exotique qui étent sa distribution et/ou sa densité de manière concurrentielle dans son aire d'introduction ( ex. la balsamine de l'Himalaya, Impatiens glandulifera ).",
                'image' => false
            ],
            [
                'label' => "éperon(un)",
                'description' => "prolongement tubuleux ou conique d'une pièce du périanthe ( sépale ou pétale ) formant un appendice fermé à son extrémité et contenant souvent du nectar ( ex. la violette de Rivin, Viola riviniana ).",
                'image' => false
            ],
            [
                'label' => "épi(un)",
                'description' => "inflorescence où toutes les fleurs, sessiles ou subsessiles, sont insérées sans pédicelle le long d'un axe allongé central appelé rachis.",
                'image' => false
            ],
            [
                'label' => "épicalice(un)",
                'description' => "enveloppe florale doublan le calice à l'extérieur et correponsant à un ensemble de Bractées ( Malvacées).",
                'image' => false
            ],
            [
                'label' => "épichile(un)",
                'description' => "partie distale du labelle de certaines espèces d'Orchidacées, séparée de la partie basale ( hypochile ) par un étranglement.",
                'image' => false
            ],
            [
                'label' => "épillet(un)",
                'description' => "élément de base de l'épi des Poacées, constitué d'un axe portant à sa base des bractées appelées glumes, et sur lequel sont insérées une ou plusieurs fleurs protégées par des bractées appelées glumelles.",
                'image' => false
            ],
            [
                'label' => "épine(une)",
                'description' => "organe pointue et piquant, provenant de la modification d'organes végétatifs ( rameau, feuille, stipule ), indétachable de la tige ou du rameau contrairement à l'aiguillon, facile à détacher.",
                'image' => false
            ],
            [
                'label' => "épineux(se)",
                'description' => "se dit d'un organe tel que la feuille, la bractée ou le fruit muni d'épines.",
                'image' => false
            ],
            [
                'label' => "épiphyte(un)",
                'description' => "se dit d'une plante verte, croissant sur un autre végétal vivant sans parasiter celui-ci.",
                'image' => false
            ],

            [
                'label' => "érablière(une)",
                'description' => "forêt dominée par les érables( Acer spp. ).",
                'image' => false
            ],
            [
                'label' => "érodé(e)",
                'description' => "se dit d'un organe bordé de petites échancrures irrégulières comme s'il avait été rongé.",
                'image' => false
            ],
            [
                'label' => "étalé(e)",
                'description' => "se dit d'un organe dressé perpendiculairement à son support ( ex. poils étalés ).",
                'image' => false
            ],
            [
                'label' => "étamine(une)",
                'description' => "organe mâle de la fleur, constitué d'un filet et d'une anthère contenant les grins de pollen ( l'étamine est alors 'fertile', si l'anthère est absente, elle est qualifiée de 'stérile' ) ; l'ensemble des étamines forme l'androcée.",
                'image' => false
            ],
            [
                'label' => "étendard(un)",
                'description' => "pétales supérieur médian, gén. plus gran que les autres, de la corole des fleurs de Fabacées, ayant la forme d'une voile ouverte au vent.",
                'image' => false
            ],
            [
                'label' => "étoilé(e)",
                'description' => "se dit d'un organe divisé en segments rayonnants comme les branches d'une étoile ( ex. poils étoilés ).",
                'image' => false
            ],
            [
                'label' => "exotique",
                'description' => "se dit d'une espèce introduite dans un nouveau territoire ( aire d'introduction ) et donc hors de son aire de répartition naturelle, de manière intentionnelle ou accidentelle par des activités humaines ( syn. non indigène, xénophyte ; ant. indigène, autochtone ).",
                'image' => false
            ],
            [
                'label' => "eutrophe",
                'description' => "se dit d'un milieu très riche en éléments minéraux utilisables par la végétation.",
                'image' => false
            ],
            [
                'label' => "exondé",
                'description' => "se dit d'un terrain émergé après inondation.",
                'image' => false
            ],
            // F
            [
                'label' => "fasciculés(ées)",
                'description' => "se dit d'organes semblables, d'égale grandeur et réunis en faisceaux ou en touffes.",
                'image' => false
            ],
            [
                'label' => "faux-épi",
                'description' => "inflorescence en panicule très contractée et allongée, ressemblant ansi à un épi.",
                'image' => false
            ],
            [
                'label' => "faux-fruit",
                'description' => "organe composite gén. charnu formé par le déceloppement u réceptacle (ex. la fraise, la pomme ) ou d'un autre organe ( ex. pédoncule floral, bractée pour l'ananas ), en plus des carpelles qui se développent en 'vrais' fruits.",
                'image' => false
            ],
            [
                'label' => "femelle",
                'description' => "se dit d'une fleur avec un gynécée, mais dépourvue d'androcée fonctionnel.",
                'image' => false
            ],
            [
                'label' => "filet(un)",
                'description' => "partie ingérieure de l'étamine, portant l'anhtère.",
                'image' => false
            ],
            [
                'label' => "filiforme",
                'description' => "se dit d'un organe très long et fin comme un fil.",
                'image' => false
            ],
            [
                'label' => "flexueux(se)",
                'description' => "se dit d'un organe sinueux.",
                'image' => false
            ],
            [
                'label' => "florifère",
                'description' => "se dit d'un organe qui porte les fleurs ( ex. tige florifère ).",
                'image' => false
            ],
            [
                'label' => "foliacé(e)",
                'description' => "se dit d'un organe qui a l'apparence d'une feuille ( ex. bractée foliacée ).",
                'image' => false
            ],
            [
                'label' => "foliaire",
                'description' => "qui concerne les feuilles ( ex. gaine foliaire, segement foliaire ).",
                'image' => false
            ],
            [
                'label' => "foliole(une)",
                'description' => "division du limbe d'une feuille composée ; la foliole est constituée d'une portion de limbe identique, elle est toujours portée par un pétiole, parfois très court ( du latin foliolum, signifiant 'petites feuille' ), articulée sur un rachis et dépourvue de bourgeon axillaire ; l'ensemble des folioles et du rachis forme une feuille composée.",
                'image' => false
            ],
            [
                'label' => "follicule(un)",
                'description' => "fruit sec déhiscent, provenant d'un seul carpelle gén. à plusieurs graines et s'ouvrant par une seule fente longitudinale ( ex. l'aconit napel, Aconitum napellus ).",
                'image' => false
            ],
            [
                'label' => "frangé(e)",
                'description' => "se dit d'un organe dont la marge est pourvue de découpures profondes, fines, régulières et serrées les unes contre les autres.",
                'image' => false
            ],
            [
                'label' => "frênaie(une)",
                'description' => "forêt dominée par les frênes.",
                'image' => false
            ],
            [
                'label' => "fronde(une)",
                'description' => "'feuille' des Ptéridophytes portan les sporanges, le plus souvent à la face inf.",
                'image' => false
            ],
            [
                'label' => "fructifère",
                'description' => "se dit d'un organe qui porte un ou plusieurs fruits ( ex. tige fructifère ).",
                'image' => false
            ],
            [
                'label' => "fruit",
                'description' => "organe contenant les graines, provenant de la transformation d'un ou plusieurs ovaires après la fécondation.",
                'image' => false
            ],
            [
                'label' => "fusiforme",
                'description' => "en forme de fuseau, c-à-d renflé au milieu et effilé aux deux extrémités.",
                'image' => false
            ],
            // G
            [
                'label' => "gaine(une)",
                'description' => "portion inférieure élargie du pétiole d'une feuille, entourant plus ou moins étroitement la tige au niveau du noeud ; chez beaucoup de Monocodylédones, c'est le limbe foliaire qui forme une gaine sur une grande longueur de l'entrenoeud.",
                'image' => false
            ],
            [
                'label' => "genouillé(e)",
                'description' => "se dit d'un organe brusquement plié en forme de genou.",
                'image' => false
            ],
            [
                'label' => "géophyte(une)",
                'description' => "forme biologique des plantes dont les organes pérennes passent la siason défavorable dans le sol ; les géophytes peuvent être à bulbes, à tubercules ou à rhizomes ( ex. la tulipe sauvage, Tulipa sylvestris ; le muguet, Convallaria majalis )",
                'image' => false
            ],
            [
                'label' => "glabre",
                'description' => "se dit d'un organe complétement dépourvu de poils.",
                'image' => false
            ],
            [
                'label' => "glabrescent(e)",
                'description' => "se dit d'un organe qui devient glabre avec l'âge",
                'image' => false
            ],
            [
                'label' => "glande(une)",
                'description' => "petite organe uni- ou pluricellulaire sécrétant une substance chimique spécifique ( ex. essence, résine, nectar), situé sur divers organes ( ex. tige, feuille, pétale ).",
                'image' => false
            ],
            [
                'label' => "glanduleux(se)",
                'description' => "se dit d'un organe muni de glandes.",
                'image' => false
            ],
            [
                'label' => "glauque",
                'description' => "se dit d'un organe qui présente une teinte vert bleuâtre.",
                'image' => false
            ],
            [
                'label' => "glauquescent(e)",
                'description' => "se dit d'un organe qui présente avec l'âge une teinte bleuâtre.",
                'image' => false
            ],
            [
                'label' => "globuleux(se)",
                'description' => "se dit d'un organe à peu près rond ou sphérique.",
                'image' => false
            ],
            [
                'label' => "glomérule(un)",
                'description' => "groupe de fleurs subsessiles étroitement rapprochées.",
                'image' => false
            ],
            [
                'label' => "glume(une)",
                'description' => "chez les Poacées, chacune des deux bractées insérées à la base de l'axe de chaque épillet.",
                'image' => false
            ],
            [
                'label' => "glumelle(une)",
                'description' => "chez les Poacées, chacune des deux bractées insérées à la base du pédicelle de chaque fleur au sein de l'épillet ; la glumelle inf. est appelée 'lemme' et la sup. 'paléole'",
                'image' => false
            ],
            [
                'label' => "glutineux(se)",
                'description' => "se dit d'un organe gluant, visqeux ( ex. écailles des bourgeons de l'aulne glutineux, Alnus glutinosa ).",
                'image' => false
            ],
            [
                'label' => "gorge(une)",
                'description' => "partie supérieure du tube de la corole, chez une fleur à pétales soudés entre eux.",
                'image' => false
            ],
            [
                'label' => "gousse(une)",
                'description' => "fruit sec déhiscent des Fabacées, provenant d'un seul carpelle, gén. à graines nombreuses, s'ouvrant par deux fentes longitudinales ( rem. la 'gousse' d'ail est une appellation erronée ).",
                'image' => false
            ],
            [
                'label' => "graine(une)",
                'description' => "chez les Spermatophytes, ovule fécondé et renfermant l'embryon.",
                'image' => false
            ],
            [
                'label' => "grappe(une)",
                'description' => "inflorescence indérerminée formée d'un axe allongé portant à des niveaux différents, des fleurs plus ou moins longuement pédicellées ( syn. racème ).",
                'image' => false
            ],
            [
                'label' => "grêle",
                'description' => "se dit d'un organe fin et fragile ( ex. tige grêle ).",
                'image' => false
            ],
            [
                'label' => "grimpante",
                'description' => "qualifie une plante dont la tige s'élève en prenant appui sur son support, grâce à des crampons et à des vrilles ( ex. la clématite, Clematis vitalba ).",
                'image' => false
            ],
            [
                'label' => "gynécée(un)",
                'description' => "ensemble des organes femelles d'une fleur, c-à-d des carpelles chacun constitué d'un ovaire, d'un style et d'un stigmate.",
                'image' => false
            ],
            [
                'label' => "gynostème(un)",
                'description' => "chez les Orchidacées, pièce florale en forme de dolonne provenant de la soudure de l'androcée et du style, les thèques de l'unique étamine fertile occupant le sommet de cette colonne.",
                'image' => false
            ],
            // H
            [
                'label' => "halophile",
                'description' => "se dit d'une plante croissant de préférence dans un milieu riche en sel.",
                'image' => false
            ],
            [
                'label' => "hampe(une)",
                'description' => "axe dépourvue de feuilles et portant à son sommet une fleur ou une inflorescence.",
                'image' => false
            ],
            [
                'label' => "hasté(e)",
                'description' => "en forme de fer de hallebarde, c-à-d pourvu(e) à la base de deux lobes étalés ( ex. feuille hastée ).",
                'image' => false
            ],
            [
                'label' => "hélicoïde",
                'description' => "se dit d'une cyme unipare, chaque axe secondaire est formé alternativement à gauche et à droite ( hélicoïde ).",
                'image' => false
            ],
            [
                'label' => "héliophile",
                'description' => "se dit d'une plante croissant de préférence en pleine lumière ( ant. sciaphile ).",
                'image' => false
            ],
            [
                'label' => "hélophyte(un)",
                'description' => "forme biologique des plantes croissant enracinées dans la vase ; les bourgeons submergés sont protégés de la saison défavorable, mais les feuilles sont hors de l'eau ( ex. la salicaire, Lythrum salicaria ).",
                'image' => false
            ],
            [
                'label' => "hémicryptophyte(un)",
                'description' => "forme biologique des plantes dont les bourgeons persistant durant l'hiver sont situés au niveau du sol ( ex. nombreuses Poacées pérennes ).",
                'image' => false
            ],
            [
                'label' => "hémiparasite",
                'description' => "se dit d'une plante chlorophylienne se fixant sur une plante hôte pour y puiser ",
                'image' => false
            ],
            [
                'label' => "herbacé(e)",
                'description' => "se dit d'une plante ayant une consistance souple, par opposition à ligneux.",
                'image' => false
            ],
            [
                'label' => "hérissé(e)",
                'description' => "se dit d'un organe couvert de poils raides, gén. droits.",
                'image' => false
            ],
            [
                'label' => "hermaphrodite",
                'description' => "se dit d'une fleur munit à la fois d'un androcée et d'un gynécée, par opposition aux fleurs unisexuées.",
                'image' => false
            ],
            [
                'label' => "hétérostachyé",
                'description' => "se dit d'une inflorescence chez les Carex où les épis comprennant soit des fleurs femelles, soit des fleurs mâles ; les épis mâles sont gén. au sommet et les épis femelles situés plus bas sur la plante.",
                'image' => false
            ],
            [
                'label' => "hétérostylie(une)",
                'description' => "présence, au sein d'une même espèce, de fleurs à styles de longueurs différentes ( ex. la primevère officinale, Primula veris ).",
                'image' => false
            ],
            [
                'label' => "hêtraie(une)",
                'description' => "forêt dominée par les hêtres.",
                'image' => false
            ],
            [
                'label' => "hipside",
                'description' => "se dit d'un organe garni de poils raides et presque dressés ( ex. la bourrache, Borago officinalis ).",
                'image' => false
            ],
            [
                'label' => "holoparasite(un)",
                'description' => "végétal non chlorohyllien, dépendant obligatoirement d'une plante hôte pour y puisser grâce à des suçoirs, les substances nutritives nécessaires à sa croissance ( ex. les cuscutes, Suscuta sp ).",
                'image' => false
            ],
            [
                'label' => "homostachyé",
                'description' => "se dit d'une inflorescence chez les Carex où les épis comprennent à la fois des fleurs femelles et des fleurs mâles.",
                'image' => false
            ],
            [
                'label' => "hyalin(e)",
                'description' => "se dit d'un organe ou un tissu translucide.",
                'image' => false
            ],
            [
                'label' => "hybride(un)",
                'description' => "plante dont les deux parents appartiennent à des espèces ou parfois à des sous-espèces différentes, relevant habituellement du même genre, plus rar. de deux genres voisins ( ex. le colza, Brassica napus, espèce présentant de nombreux hybrides ).",
                'image' => false
            ],
            [
                'label' => "hypanthium(un)",
                'description' => "chez les Rosacées et Onagracées, réceptacle floral élargie, plan, concave ou convexe.",
                'image' => false
            ],
            [
                'label' => "hypochile(un)",
                'description' => "partie proximale du labelle de certaines Orchidacées, la partie distale étant l'épichile.",
                'image' => false
            ],
            // I
            [
                'label' => "imbriqué(e)s",
                'description' => "se dit d'organes se recouvrant partiellement comme les tuiles d'un toit ( ex. écailles imbriquées ).",
                'image' => false
            ],
            [
                'label' => "imparipennée",
                'description' => "se dit d'une feuille composée-pennée à rachis terminé par une foliole, le nombre de folioles est donc impair.",
                'image' => false
            ],
            [
                'label' => "incisé(e)",
                'description' => "se dit d'un organe présentant sur ses marges de profondes découpures, le plus souvent inégales.",
                'image' => false
            ],
            [
                'label' => "indéhiscent(e)",
                'description' => "se dit d'un fruit ne s'ouvrant pas spontanément à maturaité ( ex. akènes, caryopses, la plupart des fruits charnus ; ant. déhiscent ).",
                'image' => false
            ],
            [
                'label' => "indigène",
                'description' => "se dit d'une espèce qui a colonisé ou a évolué dans un territoire de manière naturelle, sans intervantion humaine ( syn. autochtone ).",
                'image' => false
            ],
            [
                'label' => "indusie(une)",
                'description' => "chez les Ptéridophtyes, organe formé par un replit de l'épiderme protégeant un groupe de sporanges.",
                'image' => false
            ],
            [
                'label' => "infère",
                'description' => "se dit d'un ovaire situé sous le point d'insertiojn du périanthe ( ant. supère ).",
                'image' => false
            ],
            [
                'label' => "inflorescence(une)",
                'description' => "ensemble de fleurs et de bractées non séparées par de vraies feuilles.",
                'image' => false
            ],
            [
                'label' => "involucelle(un)",
                'description' => "(i) chez les Apiacèes, ensemble de bractées insérées à la base d'une ombellule ; (ii) chez les Astéracées, verticille de bractées plus petites que celle de l'involucre et situées à l'éxtérieur de celui-ci",
                'image' => false
            ],
            [
                'label' => "involucre(un)",
                'description' => "ensemble de bractées insérées, souvent en verticille, à la base d'une ombelle, d'un capitule ou d'une fleur solitaire.",
                'image' => false
            ],
            // L
            [
                'label' => "labelle(un)",
                'description' => "tépale antérieur médian de la fleur des Orchidacées, gén. plus grand que les autres et différant aussi, le cas échéant, quant à sa forme et à sa coloration ; ce tépale modifié est dirigé vers le bas par suite de la torsion de 180° de l'ovaire ( syn. lèvre ).",
                'image' => false
            ],
            [
                'label' => "laineux(se)",
                'description' => "se dit d'un tomentum abondant, formé de longs poils très emmêlés ( ressemblant à de la laine ).",
                'image' => false
            ],
             [
                'label' => "lancéolé(e)",
                'description' => "se dit d'un organe en forme de fer de lance, c-à-d 3à 4 fois plus long que large et se rétracissant progressivement vers les extrémités qui peuvent être aigües.",
                'image' => false
            ],
             [
                'label' => "latex(un)",
                'description' => "liquide visqueux, souvent laiteux, sécrété par certaines plantes, mais ne se manifestant que lorsqu'on brise l'organe qui le produit ( ex. l'euphorbe réveille-matin, Euphorbia helioscopa ).",
                'image' => false
            ],
            [
                'label' => "lemme(une)",
                'description' => "glumelle inférieure dans l'épillet des Poacées.",
                'image' => false
            ],
            [
                'label' => "lenticelle(une)",
                'description' => "petite saillie se trouvant le plus souvent sur l'écorce jeune des plantes ligneuses, parfois sur d'autres organes, tels que les fruits, permettant la circulation de l'air.",
                'image' => false
            ],
            [
                'label' => "lèvre(une)",
                'description' => "portion du calice ou des la corole de certaines fleurs à sym. bilatérale ( Lamiacées, Plantaginacées, Scrophulariacées, ... ) comprenent deux ou plusieurs pièces soudées et formant un ou plusieurs lobes.",
                'image' => false
            ],
            [
                'label' => "liane(une)",
                'description' => "plante ligneuse grimpante à longues tiges flexibles prenant appui sur divers supports ( ex. le chèvrefeuille des bois, Lonicera periclymenum ).",
                'image' => false
            ],
            [
                'label' => "libre",
                'description' => "se dit d'un organe qui n'est pas soudé à un autre organe de même nature.",
                'image' => false
            ],
            [
                'label' => "liégeux(se)",
                'description' => "se dit d'un organe pouvur de liège, c-à-d d'un tissu protecteur secondaire, constitué de cellules mortes remplies d'air, donnant une certaine épaisseur à l'organe qu'il recouvre.",
                'image' => false
            ],
            [
                'label' => "ligule(une)",
                'description' => "(i) chez les Poacées et certaines Cypéracées, petite languette membraneuse apparaissant à la limite de la gaine folliaire et du limbe, à l'intérieur de celui-ci ; (ii) chez les Astéracées, pétales prolongeant unilatéralement le tube de la corole des fleurs à sym bilatérale.",
                'image' => false
            ],
            [
                'label' => "ligulée",
                'description' => "se dit d'une fleur dont le tube de la corole se prolonge en une ligule ( ex. certaines fleurs des Astéracées ).",
                'image' => false
            ],
            [
                'label' => "limbe(un)",
                'description' => "partie élargie, plate et étalée des feuilles ou des pétales.",
                'image' => false
            ],
            [
                'label' => "linéaire",
                'description' => "se dit d'un organe long et très étroit, à bords plus ou moins parallèles.",
                'image' => false
            ],
            [
                'label' => "lobe(un)",
                'description' => "division d'une feuille ou d'un autre organe plan, en principe lorsque l'échancrure n'atteint pas le milieu de chaque moitié du limbe.",
                'image' => false
            ],
            [
                'label' => "lobé(e)",
                'description' => "se dit d'un organe découpé par des échancrures arrondies et peu profondes, n'atteignant pas le milieu du limbe ( ex. limbe du chêne pédonculé, Quercus robur ).",
                'image' => false
            ],
            [
                'label' => "loge(une)",
                'description' => "(i) cavité de l'ovaire délimitée par la paroi d'un carpelle unique ou par les parois de plusieurs carpelles soudés entre eux ; (ii) cavité d'un thèque, résultant de la fusion des deux sacs polliniques avant la libération des grains de pollen.",
                'image' => false
            ],
            [
                'label' => "lyrée(e)",
                'description' => "se dit d'un organe découpe en segemnts dont le terminal est beaucoup plus grand que les latéraux.",
                'image' => false
            ],
            // M
            [
                'label' => "mâle",
                'description' => "se dit d'une lfeur avec un androcée et dépourvue de gynécée fonctionnel ( ant. femelle ).",
                'image' => false
            ],
            [
                'label' => "médifixe",
                'description' => "se dit d'un organe fixé par sa partie médiane.",
                'image' => false
            ],
            [
                'label' => "mère",
                'description' => "suffix employé à la suite d'un nombre pour désigner le nombre ( ou un multiple de ce nombre ) de sépales et de pétales ou de tépales ( ex. une fleur dite 4-mère, pourra avoir 4 sépales, 4 pétales ou 8 sépales, 4 pétales ou une autre combinaison multiple de 4 ).",
                'image' => false
            ],
            [
                'label' => "membraneux(se)",
                'description' => "mince et transparente comme une membrane.",
                'image' => false
            ],
            [
                'label' => "mésophile",
                'description' => "se dit d'une plante d'une communauté végétale croissant de préférence sur un substrat présentant un degré moyent d'humidité, c-à-d ni trop sec ni trop humide.",
                'image' => false
            ],
            [
                'label' => "mésotrophe",
                'description' => "se dit d'un milieu moyennement richer en éléments nutritifs assimilables.",
                'image' => false
            ],
            [
                'label' => "moder(un)",
                'description' => "type d'humus forestier mésotrophe, à litière moyennement épaisse.",
                'image' => false
            ],
            [
                'label' => "monoïque",
                'description' => "se dit d'une plante possédant des fleurs unisexuées mâles et femelles apparaissant sur un même individu, mais gén. à des niveaux différents ( ex. le noisetier, Corylus avellana ; ant. dioïque ).",
                'image' => false
            ],
            [
                'label' => "mor(un)",
                'description' => "type d'humus forestier oligotrohpe, inactif, très acide, à litière très épaisse.",
                'image' => false
            ],
            [
                'label' => "mucron(un)",
                'description' => "pointe raide très courte terminant un organe.",
                'image' => false
            ],
            [
                'label' => "mull(un)",
                'description' => "type d'humus forestier actif, alcalin à légérement acide, eutrophe à légèrement mésotrophe, caractérisé par une discontinuité brutale entre la litière de faible épaisseur et les horizons minéraux sous-jacents.",
                'image' => false
            ],
            [
                'label' => "naturalisé(e)",
                'description' => "se dit d'une espèce originaire d'une autre région que le territoire étudié et qui s'est répandue ensuite naturellement au point d'être intégrée à la flore indigène. On considère que les espèces introduites naturalisées sur une période de 100 ans sont devenues indigènes.",
                'image' => false
            ],
            [
                'label' => "nectaire(un)",
                'description' => "organe essentiellement floral ( mais aussi sur bourgeon, feuille ou pétiole ) glanduleux sécrétant et excrétant le nectar, liquide sucré attirant les pollinisateurs ( ex. nectaires du pétiole chez les Prunus ).",
                'image' => false
            ],
            [
                'label' => "nervation(une)",
                'description' => "disposition des nervures dans le limbe foliaire.",
                'image' => false
            ],
            [
                'label' => "nitrophile",
                'description' => "se dit d'une espèce croissant préférentiellement sur un substrat riche en composés azotés.",
                'image' => false
            ],
            [
                'label' => "noeud(un)",
                'description' => "partie, parfois renflée, d'un axe au niveau duquel s'insère(nt) une ou plusieurs feuilles.",
                'image' => false
            ],
            [
                'label' => "noyau(un)",
                'description' => "partie centrale des drupes et drupéoles, constituées de l'endocarpe sclérifié qui entoure la graine.",
                'image' => false
            ],
            // O
            [
                'label' => "ob",
                'description' => "préfixe signifiant 'à l'envers' ( ex. obovale, qualifie un limbe foliaire ovale mais dont la plus grande largeur est au-dessus du milieu ).",
                'image' => false
            ],
            [
                'label' => "oblong(ue)",
                'description' => "nettement plus long que large, à côtés plus ou moins parallèles.",
                'image' => false
            ],
            [
                'label' => "obtus(e)",
                'description' => "se dit d'un organe à sommer non aigu, plutôt arrondi.",
                'image' => false
            ],
            [
                'label' => "ochréa(un)",
                'description' => "chez les Polygonacées, gaine membraneuse entourant la tige au-dessus de chaque noeud, au niveau de l'insertion des feuilles et provenant de la soudure des stipules.",
                'image' => false
            ],
            [
                'label' => "oligotrophe",
                'description' => "se dit d'un milieu pauvre en nutriments assimilables.",
                'image' => false
            ],
            [
                'label' => "ombelle(une)",
                'description' => "inflorescence dont les fleurs sont portées par des pédicelles de même longeur insérés soit au sommet de la tige principale ( ombelle simple ), soit au sommet des rayons égaux prlongeant le pédoncule floral ( ombelle composée ) ( ex. chez les Apiacées ).",
                'image' => false
            ],
            [
                'label' => "ombellule(une)",
                'description' => "ombelle secondaire portée par l'un des rayons d'une ombelle composée.",
                'image' => false
            ],
            [
                'label' => "ondulé(e)",
                'description' => "se dit d'un organe présentant des ondulations ou des sinuosités.",
                'image' => false
            ],
            [
                'label' => "onglet(un)",
                'description' => "partie inférieure rétrécie d'un pétale.",
                'image' => false
            ],
            [
                'label' => "opposé(e)s",
                'description' => "se dit d'organes insérés au même niveau d'un axe l'un en face de l'autre ( ex. des feuilles opposées ).",
                'image' => false
            ],
            [
                'label' => "orbiculaire",
                'description' => "se dit d'un organe en forme de cercle.",
                'image' => false
            ],
            [
                'label' => "oreillette(une)",
                'description' => "expansion de la base du limbe au-delà et de part et d'autre du point d'insertion d'une feuille embrassante ( ex. certaines Poacées, Astéracées ).",
                'image' => false
            ],
            [
                'label' => "ovaire(un)",
                'description' => "partie inférieure gén. renflée du gynécée ( ou de chaque carpelle lorsque le gynécée est formé de carpelles libres ) creusée d'une ou de plusieurs cavités closes contenant un ou plusieurs ovules.",
                'image' => false
            ],
            [
                'label' => "ovale",
                'description' => "se dit d'une feuille dont la forme correspond à celle d'un oeuf en coupe longitudinale et dont la plus grande largeur est donc située du côté du point d'insertion.",
                'image' => false
            ],
            [
                'label' => "ovoïde",
                'description' => "se dit d'un organe ayant la forme d'un oeuf.",
                'image' => false
            ],
            [
                'label' => "ovule(un)",
                'description' => "organe naissant du placenta à l'intérieur de l'ovaire, abritant l'embryon après fécondation et évoluant en graine à maturité.",
                'image' => false
            ],
            // P
            [
                'label' => "paillette(une)",
                'description' => "petite écaille membraneuse plus ou moins translucide, insérée entre les fleurs sur le réceptacle de l'inflorescence de certaines Astéracées et Dipsacacées.",
                'image' => false
            ],
            [
                'label' => "paléole(une)",
                'description' => "glumelle supérieure dans l'épillet des Poacées.",
                'image' => false
            ],
            [
                'label' => "palmatifide",
                'description' => "se dit d'une feuille simple à nervation palmée et découpée de sorte que les divisions principales ( 'découpures' ou 'sinus' ) atteignent environ la moitiée de la longeur du limbe.",
                'image' => false
            ],
            [
                'label' => "palmatilobé(e)",
                'description' => "se dit d'une feuille simple à nervation palmée et découpée en lobes arrondis.",
                'image' => false
            ],
            [
                'label' => "palmatipartite",
                'description' => "se dit d'une feuille simple à nervation palmée et découpée de sorte que les divisions principales ( 'découpures' ou 'sinus' pour être exact ) atteignent environ la moitiée de la longueur du limbe ou la dépassent largement.",
                'image' => false
            ],
            [
                'label' => "palmatiséqué(e)",
                'description' => "se dit d'une feuille simple à nervation palmée et découpée de sorte que les divisions principales ( 'découpures' ou 'sinus' ) dépassent largement la moitiée de la longueur du limibe ; les segments foliaires sont alors à peine soudés entre eux à la base et paraissent, dans certains cas, complétement distincts ( attention de ne pas confondre une feuille simple palmatiséquée et une feuille composée : les segements d'une feuille palmatiséqueée sont distincts, mais pas articulés séparément comme les folioles d'une feuille composée ).",
                'image' => false
            ],
            [
                'label' => "palmée",
                'description' => "a) se dit de la nervation d'une feuille dont le limbe folaire présente des nervures primaires ( gén. 4-5 ) divergentes à partir du sommet du pétiole, c-à-d disposéees en éventail ; b) se dit aussi d'une feuille composée de folioles disposées comme les doigts de la main.",
                'image' => false
            ],
            [
                'label' => "panicule(une)",
                'description' => "inflorescence composée, constituée d'un axe allongé sur lequel sont insérées des inflorescences simples à fleurs pédicellées ; une panicule peut donc être une grappe de grappes, une grappe de cymes.",
                'image' => false
            ],
            [
                'label' => "papilionacée",
                'description' => "se dit de la corolle des Fabacées, composée de 5 pétales inégaux et pour la plupart libre entre eux, 1 étendard, 2 ailes et 1 carène formée de 2 pétales plus ou moins longuement soudés.",
                'image' => false
            ],
            [
                'label' => "papille(une)",
                'description' => "protubérance externe d'une cellule ou d'un groupe de cellules épidermique caractéristique de certains organes ( ex. des papilles épidermiques des pétales ches Rosa, papilles stigmatiques chez beaucoup d'Astéracées ).",
                'image' => false
            ],
            [
                'label' => "papilleux(se)",
                'description' => "couvert de papilles.",
                'image' => false
            ],
            [
                'label' => "pappus(un)",
                'description' => "chez certaines Astéracées, calice modifiée en une couronne d'arêtes, d'écailles ou de poils ( soies ) surmontant l'ovaire ; dans ce dernier cas, le pappus est accrescent et deviendra une aigrette surmontant l'akène à maturité.",
                'image' => false
            ],
            [
                'label' => "paracorole(une)",
                'description' => "ensemble des appendices, éventuellement soudés entre eux, insérés à la face interne de la corole ou du périgone et présentant parfois la forme d'un cylindre ou d'une cloche ( ex. la jonquille, Narcissus pseudonarcissus ).",
                'image' => false
            ],
            [
                'label' => "parallèle",
                'description' => "se dit d'une nervation dont les nervures, parallèles entre elles, ont pratiquemetn toutes la même importance.",
                'image' => false
            ],
            [
                'label' => "paripennée",
                'description' => "se dit d'une feuille composée-pennée à rachis terminé par un mucron ou par une vrille, ne possédant pas de foliole terminale, le nombre de folioles est donc pair.",
                'image' => false
            ],
            [
                'label' => "parthénocarpique",
                'description' => "se dit d'une plante capable de produire des fruits sans qu'il n'y ait eu fécondation des ovules ; les fruits ainsi produits ne continnent pas de graines.",
                'image' => false
            ],
            [
                'label' => "pectiné(e)",
                'description' => "se dit d'un organe présentant des divisions étroites et opposées sur deux rangs, comme les dents d'un peigne.",
                'image' => false
            ],
            [
                'label' => "pédalée",
                'description' => "se dit d'une nervation d'une feuille où 3 nervures rayonnent à partir du même point et sur les 2 nervures latérales se développent des ramifications toujours orientées vers le bas.",
                'image' => false
            ],
            [
                'label' => "pédicelle(un)",
                'description' => "petit axe d'une inflorescence portant une seule fleur à son sommet.",
                'image' => false
            ],
            [
                'label' => "pédicellé(e)",
                'description' => "se dit d'une lfeur pourvue d'un pédicelle.",
                'image' => false
            ],
            [
                'label' => "pédoncule(un)",
                'description' => "axe d'une inflorescence subdivisé en pédicelles, ou axe d'une fleur solitaire axillaire.",
                'image' => false
            ],
            [
                'label' => "pédonculé(e)",
                'description' => "se dit d'une inflorescence ou d'une fleur pourvue d'un pédoncule.",
                'image' => false
            ],
            [
                'label' => "pleté(e)",
                'description' => "se dit d'un organe orbiculaire fixé approximativement par le centre ( ex. feuille peltée de l'écuelle-d'eau, Hydrocotyle vulgaris ).",
                'image' => false
            ],
            [
                'label' => "pennatifide",
                'description' => "se dit d'une feuille simple à nervation pennée et découpée de sorte que les divisions principales ( 'découpures' ou 'sinus' ) atteignent environ la moitiée de la largeur de chaque demi-limbe.",
                'image' => false
            ],
            [
                'label' => "pennatilobée",
                'description' => "se dit d'une feuille simple dont le limbe est à nervation pennée et lobée de sorte que les lobes n'atteignent pas la moitiée de la largeur de chaque demi-limbe.",
                'image' => false
            ],
            [
                'label' => "pennatipartite",
                'description' => "se dit d'une feuille simple à nervation pennée et découpée de sorte que les divisions principales ( 'découpures' ou 'sinus' ) atteignent ou dépassent légèrement la moitié de la largeur de chaque demi-limbe.",
                'image' => false
            ],
            [
                'label' => "pennatiséquée",
                'description' => "se dit d'une feuille simple à nervation pennée et découpée de sorte que les divisions principales ( 'découpures' ou 'sinus' ) dépassent largement la moitiée de la largeur de chaque demi-limbe, voire atteignent la nervure centrale ; les segments foliaires peuvent être à peine soudés entre eux à la base, au niveau de la nervure principale et paraissent, dans certains cas, complétement distincts ( attention de ne pas confondre une feuille simple pennatiséquée et une feuille composée : pour rappel, les segments d'une feuille pennatiséquée sont distincts, mais pas articulés séparément sur l'axe principal comme les folioles d'une feuille composée ).",
                'image' => false
            ],
            [
                'label' => "penne(une)",
                'description' => "chez les Ptéridophytes, segment de primer ordre d'une fronde composée.",
                'image' => false
            ],
            [
                'label' => "pennée",
                'description' => "se dit d'une feuille simple ou composée dont les nervures secondaires ou les folioles sont disposées de chque côté de la nervure médiane comme les barbes d'une plume.",
                'image' => false
            ],
            [
                'label' => "pépin(un)",
                'description' => "graine d'une baie.",
                'image' => false
            ],
            [
                'label' => "pérenne",
                'description' => "se dit d'une plante qui peut vivre durant plusieurs années ( syn. vivace ).",
                'image' => false
            ],
            [
                'label' => "perfoliée",
                'description' => "se dit d'une feuille sessile embrassante la tige et paraissant traversée par celle-ci.",
                'image' => false
            ],
            [
                'label' => "perianthe(un)",
                'description' => "ensemble des pièces florales ( calice et corole ) qui entourent l'androcée et/ou le gynécée.",
                'image' => false
            ],
            [
                'label' => "péricarpe(un)",
                'description' => "paroi du fruit.",
                'image' => false
            ],
            [
                'label' => "périgone(un)",
                'description' => "enveloppe florale comprenant des pièces toutes semblables appelées tépales, sans distinction de pétales et sépales ( ex. chez de nombreuses monocotylédones ).",
                'image' => false
            ],
            [
                'label' => "persistant(e)",
                'description' => "se dit d'un organe restant en place au-delà du terme de la période de végétation ( calice, stipules... ) ; dans le cas de feuilles, désigne un feuillage 'toujours vert', c-à-d un feuillage se renouvelant de façon échelonnée tout au long de l'année.",
                'image' => false
            ],
            [
                'label' => "pétale(un)",
                'description' => "pièce du verticille interne du périanthe, élément de base de la corole, gén. vivement coloré.",
                'image' => false
            ],
            [
                'label' => "pétaloïde",
                'description' => "se dit d'organe ( gén. tépale ) ressemblant à un pétale, vivement coloré.",
                'image' => false
            ],
            [
                'label' => "pétiole(un)",
                'description' => "partie inférieur d'une feuille reliant le limbe à la tige.",
                'image' => false
            ],
            [
                'label' => "pétiolule(un)",
                'description' => "petit pétiole portant le limbe d'une foliole ches les Spermatophytes ou d'une pinnule chez les Ptéridophytes.",
                'image' => false
            ],
            [
                'label' => "pétiolulé(e)",
                'description' => "se dit d'un organe pourvu d'un pétiolule.",
                'image' => false
            ],
            [
                'label' => "phanérophyte(un)",
                'description' => "forme biologique des plantes dont les bourgeons persistant durant l'hiver sont portés à plus de 50 cm de hauteur.",
                'image' => false
            ],
            [
                'label' => "pilosité(une)",
                'description' => "manière dont un organe est couvert de poils.",
                'image' => false
            ],
            [
                'label' => "pinnule(une)",
                'description' => "chez les Ptéridophytes, subdivision d'une penne ou division secondaire.",
                'image' => false
            ],
            [
                'label' => "pionnier(ère)",
                'description' => "se dit d'une plante ou d'une végétation qui s'installe sur un substrat nu ( rochers, éboulis, terrains défrichés ou incendiés ).",
                'image' => false
            ],
            [
                'label' => "pistil(un)",
                'description' => "synonyme de gynécée.",
                'image' => false
            ],
            [
                'label' => "pivotante",
                'description' => "se dit d'une racine principale, nettemetn plus développée que les radicelles, s'enfonçant verticalement des le sol.",
                'image' => false
            ],
            [
                'label' => "plumeux(se)",
                'description' => "se dit d'un organe muni de poils fins disposés comme les barbes d'une plume.",
                'image' => false
            ],
            [
                'label' => "poilu(e)",
                'description' => "se dit d'un organe garni de poils plus ou moins allongés.",
                'image' => false
            ],
            [
                'label' => "pollen",
                'description' => "terme collectif désignant l'ensemble des corpuscules microscopiques ( 'grains' de pollen ) formés dans les anthères et assurant la fonction de gamète mâle dans le cycle de reproduction des Spermatophytes.",
                'image' => false
            ],
            [
                'label' => "pollinie(une)",
                'description' => "chez les Orchidacées, masse de pollen aggloméré qui peut être transportée en bloc par les insectes.",
                'image' => false
            ],
            [
                'label' => "pollinisation(une)",
                'description' => "transport du pollen depuis les anthères sur les stigmate d'une fleur chez les Angiospermes, d'un cône mâle à un cône femelle ches les Gymnospermes.",
                'image' => false
            ],
            [
                'label' => "ponctué(e)",
                'description' => "se dit d'un organe marqué de ponctuations ou de petites taches.",
                'image' => false
            ],
            [
                'label' => "préfoliaison",
                'description' => "disposition des feuilles dans le bourgeon ; chez les Poacées, la préfoliaison est dite (pliée' si la dernière feuille est pliée en deux à l'intérieur de l'avant-dernière feuille, elle est 'enroulée' si la dernière feuille est enroulée à l'intérieur de l'avant-dernière.",
                'image' => false
            ],
            [
                'label' => "proximal(e)",
                'description' => "se dit d'un organe proche de la base ou du lieu d'insertion d'un autre organe ( ant. distal, syn. basal ).",
                'image' => false
            ],
            [
                'label' => "pruineux(se)",
                'description' => "se dit d'un organe couvert d'un revêtement d'apparence farineuse s'enlevant facilement au toucher ( ex. ce qui recouvre certains fruits comme les prunes, les raisins ).",
                'image' => false
            ],
            [
                'label' => "pubescent(e)",
                'description' => "se dit d'un organe couvert de poils courts et souples formant un fin duvet.",
                'image' => false
            ],
            [
                'label' => "pyriforme",
                'description' => "se dit d'un organe en forme de poire.",
                'image' => false
            ],
            [
                'label' => "pyxide(une)",
                'description' => "fruit sec ( capsule ) déhiscent par une fente transversale circulaire délimitant deux parties, la supérieure ( opercule ) s'enlevant comme un couvercle.",
                'image' => false
            ],
            // R
            [
                'label' => "rachis(un)",
                'description' => "chez les Angiospermes, axe principal portant les folioles et paraissant prolonger le pétiole sur une feuille composée-pennée ; ches les Ptéridophytes, axe portant les pennes.",
                'image' => false
            ],
            [
                'label' => "racine(une)",
                'description' => "organe souterrain de la plante assurant l'ancrage dans le sol et la nutrition du végétal ( absorption d'eau et de sels minéraux grâce à des poils absorbants ).",
                'image' => false
            ],
            [
                'label' => "radiaire",
                'description' => "caractérise la structure d'un organe comportant au moins deux plans de sym. ( ant. bilatérale ).",
                'image' => false
            ],
[
                'label' => "radicant(e)",
                'description' => "se dit d'un organe produisant des racines adventives.",
                'image' => false
            ],
            [
                'label' => "rampant(e)",
                'description' => "se dit d'une plante ou d'un organe qui croit horizontalement sur le substrat.",
                'image' => false
            ],
            [
                'label' => "rayon(un)",
                'description' => "axe portant une ombellule dans une ombelle composée.",
                'image' => false
            ],
[
                'label' => "réceptacle(un)",
                'description' => "(i) extrémité de l'axe de la fleur, parfois très court, sur lequel sont insérées les pièces florales ; cet axe apparait souvent comme un élargissement du sommet du pédicelle floral ( Rosacées ) ; (ii) partie élargie du pédoncule portant les fleurs d'un capitule ( Astéracées ).",
                'image' => false
            ],
            [
                'label' => "réfléchi(e)",
                'description' => "se dit d'un organe recourbé vers le bas d'envirion 180° par rapport à la direction initiale.",
                'image' => false
            ],
            [
                'label' => "réfracté(e)",
                'description' => "se dit d'un organe, un pédicelle notamment, dirigé vers le bas.",
                'image' => false
            ],
[
                'label' => "rejet(un)",
                'description' => "pousse végétative apparaissant à la base d'une tige ou sur une souche.",
                'image' => false
            ],
            [
                'label' => "renflé(e)",
                'description' => "se dit d'un organe épaissie, élargi.",
                'image' => false
            ],
            [
                'label' => "réniforme",
                'description' => "se dit d'un organe en forme de rein.",
                'image' => false
            ],
[
                'label' => "réticulée",
                'description' => "se dit d'une nervation dont tous les éléments sont d'importance à peu près égale et forment un réseau serré.",
                'image' => false
            ],
            [
                'label' => "rhizome(un)",
                'description' => "tige souterraine portan des feuilles écailleuse, des racines adventives et émettant des poucces aériennes.",
                'image' => false
            ],
            [
                'label' => "rhomboïdal(e)",
                'description' => "se dit d'un organe en forme de losange.",
                'image' => false
            ],
[
                'label' => "rosette(une)",
                'description' => "groupe de feuille basilaires étalées sur le sol et insérées au niveau du collet.",
                'image' => false
            ],
            [
                'label' => "rostellum(un)",
                'description' => "chez les Orchidacèes, appendice en forme de bec ou parfois subglobuleux, situé vers le sommet de la colonne centrale de la fleur ( gynostème ) ; il proviendrait de la transformation d'un stigmate stérile.",
                'image' => false
            ],
            [
                'label' => "rudérale",
                'description' => "se dit d'une espèce ou d'une végétation croissant dans un site rudéralisé.",
                'image' => false
            ],
            [
                'label' => "rudéralisé",
                'description' => "se dit d'un site fortement désorganisé par une activité humaine ( décombres, terrains vagues, bords de chemins, terrils, ballast des voies ferrées... ).",
                'image' => false
            ],
            // S
            [
                'label' => "sac(un)",
                'description' => "excroissance bossue à la base de la corole ( ex. fleur de muflier, Antirrhinum majus ).",
                'image' => false
            ],
            [
                'label' => "sagitté(e)",
                'description' => "en forme de fer de flèche.",
                'image' => false
            ],
[
                'label' => "samare(une)",
                'description' => "akène muni d'une aile membraneuse ; disamare : 2 akènes pourvus tous deux d'une aile et plus ou moins longuement soudés entre eux ( ex. fruit de l'érable sycomore, Acer pseudoplatanus ).",
                'image' => false
            ],
            [
                'label' => "saprophyte",
                'description' => "plante dépourvue de chlorophylle, vivant aux dépens de matière organique en décomposition.",
                'image' => false
            ],
            [
                'label' => "saulaie(une)",
                'description' => "peuplement de saules ( syn. saussaie ).",
                'image' => false
            ],
[
                'label' => "scabre",
                'description' => "se dit d'un organe rude au toucher.",
                'image' => false
            ],
            [
                'label' => "scarieux(se)",
                'description' => "se dit d'un organe de structure membraneuse, souvent translucide ou transparent.",
                'image' => false
            ],
            [
                'label' => "schizocarpe(un)",
                'description' => "fruit sec déhiscent, provenant de carpelles soudés et se séparant à maturité en plusieurs parties.",
                'image' => false
            ],
[
                'label' => "sciaphile",
                'description' => "se dit d'une espèce tolérant un oombrage important ( ant. héliophile ).",
                'image' => false
            ],
            [
                'label' => "scorpioïde",
                'description' => "se dit d'une cyme unipare, chaque axe secondaire est formé dans le même sens, ce qui donne une forme de crosse ou de queue de scorpion.",
                'image' => false
            ],
            [
                'label' => "segment(un)",
                'description' => "division d'une limbe foliaire profondément découpé ; dans un limbe plusieurs fois penné, on donne le nom de segements foliaires aux dernières subdivisions reconnaissables.",
                'image' => false
            ],
            [
                'label' => "semi-infère",
                'description' => "se dit d'un ovaire dont seule la moitié inférieure est enfoncée dans le réceptacle et y adhère.",
                'image' => false
            ],
            [
                'label' => "sépale(un)",
                'description' => "pièce du verticille externe du périanthe, élément du calice.",
                'image' => false
            ],

            [
                'label' => "sépaloïde",
                'description' => "se dit d'un organe ( gén. tépale ) verdâtre, semblable à un sépale.",
                'image' => false
            ],
            [
                'label' => "sessile",
                'description' => "se dit d'un organe dépourvu de pétiole, de pédicelle ou de pédoncule.",
                'image' => false
            ],
            [
                'label' => "sétacé(e)",
                'description' => "se dit d'un organe ayant la consistance d'une soie de porc.",
                'image' => false
            ],
            [
                'label' => "silicule(une)",
                'description' => "silique courte, tout au plus 3 fois aussi longue que large.",
                'image' => false
            ],
            [
                'label' => "silique(une)",
                'description' => "fruit sec, plus de 3 fois aussi long que large, s'ouvrant en principe en deux valves séparées par une cloison membraneuse sur les bords de laquelle sont attachées les graines, caractéristique des Brassicacées.",
                'image' => false
            ],
            [
                'label' => "sillonné(e)",
                'description' => "se dit d'un organe creusé de sillons, de raies longitudinales.",
                'image' => false
            ],
            [
                'label' => "simple",
                'description' => "se dit d'une feuille non subdivisée en folioles, d'une tige non ramifiée, d'une inflorescence non composée ( ant. composé ).",
                'image' => false
            ],
            [
                'label' => "sinué(e)",
                'description' => "se dit d'une feuille dont le limbe présente des échancrures arrondies à peine marquées.",
                'image' => false
            ],
            [
                'label' => "sinus(un)",
                'description' => "échancrure séparant deux lobes ou deux parties saillantes.",
                'image' => false
            ],
            [
                'label' => "soie(une)",
                'description' => "poil long et raide.",
                'image' => false
            ],
            [
                'label' => "sore(un)",
                'description' => "chez les Ptéridophytes, groupe de sporanges bien délimité par rapport aux autres.",
                'image' => false
            ],
            [
                'label' => "souche(une)",
                'description' => "partie basale souterraine de la tige de plantes vivaces.",
                'image' => false
            ],
            [
                'label' => "sous-arbrisseau(un)",
                'description' => "arbrisseau nain dont la taille est inférieure à 50 cm environ.",
                'image' => false
            ],
            [
                'label' => "spadice(un)",
                'description' => "inflorescence des Aracées constituée d'un axe charnue, épais, portant des verticilles de fleurs sessiles ( femelels, mâles et stériles à deux niveaux différents ) et entourée par une spathe.",
                'image' => false
            ],
            [
                'label' => "spathe(une)",
                'description' => "grande bractée membraneuse ou foliacèe entourant complétement une fleur ou une inflorescence avant son épanouissement ( ex. la ciboulette, Allium schoenoprasum, le gouet tacheté, Arum maculatum ).",
                'image' => false
            ],
            [
                'label' => "spatulé(e)",
                'description' => "se dit d'un organe élargi au sommet et atténué vers le bas.",
                'image' => false
            ],
            [
                'label' => "spiciforme",
                'description' => "se dit d'une inflorescence dont la forme rappelle celle de l'èpi.",
                'image' => false
            ],
            [
                'label' => "spiralée",
                'description' => "se dit d'une fleur dont la pièces ne sont pas groupées en verticilles concentriques, mais insérées à des niveaux différents du réceptacle en des points reliés entre eux par une spirale.",
                'image' => false
            ],
            [
                'label' => "spontané(e)",
                'description' => "se dit d'une plante qui croit à l'état sauvage, dans le territoire considéré.",
                'image' => false
            ],
            [
                'label' => "sporange(un)",
                'description' => "chez les Ptéridophytes et les Bryophytes, organe dans lequel se forment les spores.",
                'image' => false
            ],
            [
                'label' => "spore(une)",
                'description' => "organe de la reproduction asexuée se formant dans un sporange, ches les Ptéridophytes.",
                'image' => false
            ],
            [
                'label' => "squamiforme",
                'description' => "se dit de feuilles en forme d'écailles, imbriquées les unes dans les autres.",
                'image' => false
            ],
            [
                'label' => "staminal(e)",
                'description' => "se dit d'un organe qui se rapporte aux étamines ( ex. tube staminal : organe résultant de la soudure latérale des filets des étamines ).",
                'image' => false
            ],
            [
                'label' => "staminode(un)",
                'description' => "petit organe floral écailleux ou pétaloïde interprété comme le résultat de l'avortement d'une étameine et de sa modification.",
                'image' => false
            ],
            [
                'label' => "station(une)",
                'description' => "site où croit une plante.",
                'image' => false
            ],
            [
                'label' => "stérile",
                'description' => "se dit d'une plante ou d'une lfeur qui ne produit pas de graines, ou encore d'une étamine qui ne produit pas de pollen ( ant. fertile ).",
                'image' => false
            ],
            [
                'label' => "stigmate(un)",
                'description' => "extrémité libre du gynécée ( carpelle ) ou du style, portant gén. des papilles, et plus ou moins visqueuse, disposée à recevoir les grains de pollen lors de la pollinisation.",
                'image' => false
            ],
            [
                'label' => "stipité(e)",
                'description' => "se dit d'un organe surélevé et porté par un petit pied ( ex. gynécée de la fleur de certains saules ).",
                'image' => false
            ],
            [
                'label' => "stipule(une)",
                'description' => "appendice foliaire gén. disposé en nombre pair, le plus souvent de nature foliacée ou membraneuse, situé de part et d'autre de la tige au niveau de l'insertion de la base du pétiole ( ou du limbe dans le cas de feuilles sessiles ) ou à la base des sépales ( Rosacées ).",
                'image' => false
            ],
            [
                'label' => "stipulé(e)",
                'description' => "se dit d'un organe pourvu de stipules.",
                'image' => false
            ],
            [
                'label' => "stolon(un)",
                'description' => "tige aréirinne rampante, apparaissant souvent comme un long rejet au départ d'une rosette de feuilles ou du collet et s'enracinant gén. au niveau des noeuds ( ex. le fraisier, Fragaria vesca ).",
                'image' => false
            ],
            [
                'label' => "stolonifère",
                'description' => "se dit d'un organe qui possède des stolons.",
                'image' => false
            ],
            [
                'label' => "stomate(un)",
                'description' => "couple de cellules épidermiques ménégeant entre elles une petite ouverture ( ostiole ) surmontant une cavité interne, l'ensemble contribuant à reguler les échanges gazeux entre l'atmosphère et les milieu interne de la feuille.",
                'image' => false
            ],
            [
                'label' => "strié(e)",
                'description' => "se dit d'un organe marqué de lignes parallèles entre elles ou creusé de petits sillons parallèles entre eux.",
                'image' => false
            ],
            [
                'label' => "style(un)",
                'description' => "partie du gynécée prolongeant l'ovaire et portant le(s) stigmate(s).",
                'image' => false
            ],
            [
                'label' => "sub",
                'description' => "préfixe signifiant presque ( ex. suborbiculaire, c-à-d presque circulaire ).",
                'image' => false
            ],
            [
                'label' => "subsessile",
                'description' => "se dit d'un organe à pétiole ou pédicelle très court.",
                'image' => false
            ],
            [
                'label' => "submergé(e)",
                'description' => "se dit d'un plante ou d'un organe croissant sous la surface de l'eau.",
                'image' => false
            ],
            [
                'label' => "subspontané(e)",
                'description' => "se dit d'une plante cultivée, échappée des jardins, des parcs ou des champs, ne persistant souvent que peu de temps dans ses stations ou du moins ne se propageant pas en se mêlant à la flore indigène.",
                'image' => false
            ],
            [
                'label' => "subulé(e)",
                'description' => "se dit d'un organe sui se termine insensiblement en pointe aigüe.",
                'image' => false
            ],
            [
                'label' => "succulent(e)",
                'description' => "se dit d'une plante ou d'un organe gorgé d'eau, qui donne l'impression d'être charnu, d'être épais ( ex. feuilles des Sedum, 'plantes grasses' ).",
                'image' => false
            ],
            [
                'label' => "supère",
                'description' => "se dit d'un ovaire situé au dessu du point d'insertion des enveloppes florales, libre du réceptacle sauf en soin point d'insertion basilaire ( ant. infère ).",
                'image' => false
            ],
            // T
            [
                'label' => "tannin(un)",
                'description' => "substance phénolique, fréquente chez les plantes, capabe de rendre les cuirs imputrescibles ( ex. écorce de chêne, Quercus spp. ).",
                'image' => false
            ],
            [
                'label' => "taxon(un)",
                'description' => "entité systèmatique concrète d'un rang quelconque ( variété, espèce, genre, famille, ordre ).",
                'image' => false
            ],
            [
                'label' => "tépale(un)",
                'description' => "pièce florale colorée ou non, consitutant l'unique enveloppe florale, celle-ci n'étant pas différenciée en calice et corole.",
                'image' => false
            ],
            [
                'label' => "ternée",
                'description' => "se dit d'une feuille simple divisée en 3 segments foliaires de dimension subégales.",
                'image' => false
            ],
            [
                'label' => "tétradyname",
                'description' => "se dit d'une androcée à 6 étamines dont 4 étamines sont plus longues que les 2 autres ( ex. chez les Brassicacées ).",
                'image' => false
            ],
            [
                'label' => "tétrakène(un)",
                'description' => "fruit sec constitué de 4 akènes plus ou moins soudés entre eux, se fragmentant à maturité ( ex. chez les Lamiacées ).",
                'image' => false
            ],
            [
                'label' => "thèque(une)",
                'description' => "partie de l'anthère contenant les sacs polliniques, chaque étamine comprend gén. 2 thèques.",
                'image' => false
            ],
            [
                'label' => "thermophile",
                'description' => "se dit d'une plante qui croit de préférence dans des sites chauds et ensoleillés.",
                'image' => false
            ],
            [
                'label' => "thérophyte(un)",
                'description' => "forme biologique des plantes dont le cycle de vie, epuis la germination de la graine jusqu'à la maturation des graines, dure moins d'un an ( syn. annuel ).",
                'image' => false
            ],
            [
                'label' => "tomenteux(se)",
                'description' => "se dit d'un organe couvert de poils longs, souples et entrecroisés, formant un feutrage plus ou moins épais à la surface de l'organe.",
                'image' => false
            ],
            [
                'label' => "tomentum(un)",
                'description' => "pubescence blanche constituée de poils longs, souples et entrecroisés, formant un feutrage plus ou moins épais à la surface de l'organe.",
                'image' => false
            ],
            [
                'label' => "touradon(un)",
                'description' => "buttes ou mottes arrondies, de 30 cm à 1 m de haut, formées par des plantes cespiteuses, en milieux très humides ou inondés qui se développpent sur leus anciennes racines et feuilles mortes ( ex. touradons de molinie, Molinia caerulea ).",
                'image' => false
            ],
            [
                'label' => "tourbière(une)",
                'description' => "zone marécageuse, colonisée par la végétation, dont les conditions écologiques particulières ont permis la formation d'un sol constitué de matière organique non totalement décomposée ( tourbe ).",
                'image' => false
            ],
            [
                'label' => "traçant(e)",
                'description' => "se dit d'une tige ou d'une racine qui s'étale sur le sol ou se développe horizontalement à faible profondeur.",
                'image' => false
            ],
            [
                'label' => "trifide",
                'description' => "se dit d'un organe fendu en 3 parties",
                'image' => false
            ],
            [
                'label' => "trifolié(e)",
                'description' => "se dit d'une feuille composée à 3 folioles.",
                'image' => false
            ],
            [
                'label' => "trigone",
                'description' => "se dit d'un organe présentant trois angles.",
                'image' => false
            ],
            [
                'label' => "tronqué(e)",
                'description' => "se dit d'une organe dont le sommet est comme coupé par une ligne ou un plan ( ex. une fuille tronquée ).",
                'image' => false
            ],
            [
                'label' => "tube(un)",
                'description' => "partie inférieure d'une corole, d'un calice ou d'un périogone, formée par la soudure des pétales, des sépales ou des tépales.",
                'image' => false
            ],
            [
                'label' => "tubercule(un)",
                'description' => "(i) renflement d'une tige souterraine emmagasinant des substances de réserve ; (ii) petite excroissance arrondie ( plus grande qu'une papille ) caractéristique de certains organes tels les graines.",
                'image' => false
            ],
            [
                'label' => "tubulée",
                'description' => "se dit d'une fleur composée d'un tuble cylindrique se terminant par 5 lobes ou dents, provenant de 5 pétales soudés ( ex. fleurs tubulées de certaines Astéracées ).",
                'image' => false
            ],
            [
                'label' => "turbion(un)",
                'description' => "jeune pousse naissant sur le rhizome ou sur la souche des plantes vivaces.",
                'image' => false
            ],
            // U
            [
                'label' => "unilatéral(e)",
                'description' => "se dit lorsqu'un ensemble d'organes est situé ou tourné d'un seul côté.",
                'image' => false
            ],
            [
                'label' => "unipare",
                'description' => "se dit d'un cyme dont chaque ramification donne un seul axe secondaire, soit toujours dans le même sens ( scorpioïde ), soit alternativement à gauche et à droite ( hélicoïde ).",
                'image' => false
            ],
            [
                'label' => "unisexuée",
                'description' => "se dit d'une fleur ou d'une plante portant les organes reproducteurs d'un seul sexe, soit un androcée soit un gynécée.",
                'image' => false
            ],
            [
                'label' => "urticant(e)",
                'description' => "se dit d'une plante ou d'un organe dont le contact provoque sur la peau une sensation de brulure ( ex. les poils urticants de l'ortie, Urtica dioica ).",
                'image' => false
            ],
            [
                'label' => "utricule(un)",
                'description' => "(i) organe en forme de petit sac entourant l'ovaire dont seuls les stigmates émergeant par le col sont visibles ( dans le genre Carex ) ; (ii) feuille aquatique modifiée, de l'ordre du millimètre, destinée à la capture de microorganismes, chez le genre Utricularia.",
                'image' => false
            ],
            // V
            [
                'label' => "valve(une)",
                'description' => "chacune des parties de la paroi d'un fruit sec déhiscent.",
                'image' => false
            ],
            [
                'label' => "variété(une)",
                'description' => "unité systématique de rang inférieur à l'espèce.",
                'image' => false
            ],
            [
                'label' => "velouté(e)",
                'description' => "qui a l'aspect du velours, c-à-d couvert de poils fins et courts, très denses, doux au toucher ( ex. chez certaines espèces du genre Ophrys le labelle est velouté ).",
                'image' => false
            ],
            [
                'label' => "velu(e)",
                'description' => "se dit d'un organe couvert de longs poils souples entremêlés.",
                'image' => false
            ],
            [
                'label' => "verticille(un)",
                'description' => "ensemble ( plus de 2 ) d'organes de même nature insérés en cercle au même niveau autour d'un axe ( ex. un verticielle de feuilles ).",
                'image' => false
            ],
            [
                'label' => "vésicule(une)",
                'description' => "organe renglé comme une petite vessie ; terme le plus souvent utilisé chez les algues ( organe servant de flotteur ).",
                'image' => false
            ],
            [
                'label' => "vivace",
                'description' => "se dit d'une plante dont le cycle vital dure plus de deux ans ( sym. pérenne ).",
                'image' => false
            ],
            [
                'label' => "vivipare",
                'description' => "se dit d'une plante ou d'un organe ( fleur, feuille... ) sur lequel ( ou au sein duquel à apparaissent des bourgeons végétatifs qui croissent et donnent une plantule pourvue le cas échéant de racines ; celle-ci peut se détacher et assurer la multiplication végétative.",
                'image' => false
            ],
            [
                'label' => "volubile",
                'description' => "se dit d'une plante dont la tige s'enroule en spirale en grimpant autour d'un support.",
                'image' => false
            ],
            [
                'label' => "vrille(une)",
                'description' => "organe modifié ( tige, feuille, foliole, inflorescence ), composé d'un ou de plusieurs filaments volubiles s'accrochant en s'enroulant en spirale autour d'un support ( ex. feuilles de plusieurs Fabacées ).",
                'image' => false
            ],
            // X
            [
                'label' => "xérophile",
                'description' => "se dit d'une plante ou d'une végétation croissant de préférence dans des sites secs.",
                'image' => false
            ],
        ];

        // type
        $item_type = ItemType::where('slug', 'botanic')->first();

        // create
        foreach( $datas as $data )
        {
            $vocabulary = new Vocabulary();
            $vocabulary->label = $data['label'];
            $vocabulary->description = $data['description'];
            $vocabulary->item_type_id = $item_type->id;

            if( $data['image'] )
                $vocabulary->label = $data['image'];

            $vocabulary->save();
        }
    }
}
