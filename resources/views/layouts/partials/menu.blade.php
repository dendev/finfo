<?php
use Illuminate\Support\Facades\Auth;
// type for items
$item_types = \App\Models\ItemType::all();
$url = \Illuminate\Support\Facades\Request::url();
$item_type_slug = (array_key_exists('type', $_GET) ) ? $_GET['type'] : 'botanic';

?>
<nav class="navbar navbar-expand-lg navbar-dark" id="menu">
    <!-- logo -->
    <span class="navbar-brand">
        <div class="d-none d-lg-block"> <!-- desktop -->
            <a class="navbar-brand" href="/">
                <img src="{{asset('images/logo.png')}}" id="menu-logo">
            </a>
            <a class="navbar-brand" href="/">
                <span id="menu-logo-name">Finfo</span>
            </a>
        </div>
        <div class="d-lg-none d-xl-none"> <!-- mobile -->
            <a class="navbar-brand" href="/">Finfo</a>
        </div>
    </span>

    <!-- name -->
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <!-- right menu -->
    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
        <ul class="navbar-nav ml-auto" id="menu-options">
            <!-- Item -->
            <li class="nav-item dropdown">
                <a class="nav-link" href="{{route('pages.plant')}}">Fiche</a>
            </li>
            <!-- quiz -->
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="nav-quiz" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Quiz
                </a>
                <div class="dropdown-menu" aria-labelledby="nav-quiz">
                    <a class="dropdown-item" href="{{route('pages.quiz_vocabulary')}}">Vocabulaire</a>
                    <a class="dropdown-item" href="{{route('pages.quiz_name')}}">Nom</a>
                    <a class="dropdown-item" href="{{route('pages.quiz_image')}}">Image</a>
                    <a class="dropdown-item" href="{{route('pages.quiz_family')}}">Famille</a>
                </div>
            </li>
            <!-- vocabularies -->
            <li class="nav-item">
                <a class="nav-link" href="{{route('pages.vocabulary')}}">Vocabulaires</a>
            </li>
            <!-- type // TODO  -->
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="nav-type" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    @foreach($item_types as $item_type)
                        @if($item_type->slug === $item_type_slug)
                            <i class="{{$item_type->icon}}"></i>
                        @endif
                    @endforeach
                </a>
                <div class="dropdown-menu" aria-labelledby="nav-type">
                    @foreach($item_types as $item_type)
                        <a href="{{$url . '?type=' . $item_type->slug}}" class="btn btn-primary dropdown-item">
                            <i class="{{$item_type->icon}}"></i>
                        </a>
                    @endforeach
                </div>
            </li>
        </ul>
    </div>
</nav>
@javascript('item_type', "$item_type_slug")
