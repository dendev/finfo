@extends('layouts.app')

@section('title', 'quiz famille')

@section('content')
    @javascript('quiz_entity_name', 'family')
    <div id="quiz"></div>
@endsection
