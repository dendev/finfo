@extends('layouts.app')

@section('title', 'quiz vocabulaire')

@section('content')
    @javascript('quiz_entity_name', 'vocabulary')
    <div id="quiz"></div>
@endsection
