<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('dashboard') }}"><i class="la la-home nav-icon"></i> {{ trans('backpack::base.dashboard') }}</a></li>

<!-- Users, Roles, Permissions -->
<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon la la-users"></i> Authentication</a>
    <ul class="nav-dropdown-items">
        <li class="nav-item"><a class="nav-link" href="{{ backpack_url('user') }}"><i class="nav-icon la la-user"></i> <span>Users</span></a></li>
        <li class="nav-item"><a class="nav-link" href="{{ backpack_url('role') }}"><i class="nav-icon la la-id-badge"></i> <span>Roles</span></a></li>
        <li class="nav-item"><a class="nav-link" href="{{ backpack_url('permission') }}"><i class="nav-icon la la-key"></i> <span>Permissions</span></a></li>
    </ul>
</li>
<hr/>

<!-- main menus -->
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('vocabulary') }}'><i class='nav-icon la la-font'></i> Vocabularies</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('classification') }}'><i class='nav-icon la la-random'></i> Classifications</a></li>
<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon la la-file-alt"></i> Items</a>
    <ul class="nav-dropdown-items">
        <li class="nav-item"><a class="nav-link" href="{{ backpack_url('item') }}"><span>List</span></a></li>
        <li class="nav-item"><a class="nav-link" href="{{ backpack_url('name') }}"><span>Names</span></a></li>
        <li class="nav-item"><a class="nav-link" href="{{ backpack_url('image') }}"><span>Images</span></a></li>
        <li class="nav-item"><a class="nav-link" href="{{ backpack_url('item_extra') }}"><span>Extras</span></a></li>
        <li class="nav-item"><a class="nav-link" href="{{ backpack_url('item_tag') }}"><span>Tags</span></a></li>
    </ul>
</li>


<!-- sub menus -->
<hr/>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('itemtype') }}'><i class='nav-icon la la-question'></i> ItemTypes</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('itemextra') }}'><i class='nav-icon la la-question'></i> ItemExtras</a></li>

<hr/>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('log') }}'><i class='nav-icon la la-terminal'></i> Logs</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('setting') }}'><i class='nav-icon la la-cog'></i> <span>Settings</span></a></li>





<li class='nav-item'><a class='nav-link' href='{{ backpack_url('tag') }}'><i class='nav-icon la la-question'></i> Tags</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('itemtag') }}'><i class='nav-icon la la-question'></i> ItemTags</a></li>
