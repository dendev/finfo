@php
    $extras = $entry->get_extras();
@endphp

<span class="d-inline-flex" style="width: 100%;">
@foreach($extras as $extra )
    <div class="accordion" id="accordion" style="width: 100%">
        <div class="card">
            <div class="card-header" id="heading-{{$extra->slug}}" >
                <h5 class="mb-0">
                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapse-{{$extra->slug}}" aria-expanded="true" aria-controls="collapse-{{$extra->slug}}" style="width: 100%; text-align: left;">
                        {{$extra->label}}
                    </button>
                </h5>
            </div>

            <div id="collapse-{{$extra->slug}}" class="collapse" aria-labelledby="heading-{{$extra->slug}}" data-parent="#accordion">
                <div class="card-body">
                    <ul>
                    @foreach($extra->fields as $field )
                            <li><p>{{ $field['value'] }}</p></li>
                    @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endforeach
</span>
