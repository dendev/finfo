@php
    $images_html = $entry->get_images_as_html();
    // https://guonanci.github.io/react-images-viewer/
@endphp
<span class="d-inline-flex">
    @foreach($images_html as $image_html)
        {!! $image_html !!}
    @endforeach
</span>

