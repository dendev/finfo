import axios from 'axios';

// config
const finfoApi = axios.create({
    baseURL: process.env.MIX_APP_API_URL
});

function get_config()
{
    let config = null;

    let user_token = window.user_token;
    let item_type = window.item_type;

    let token = ( user_token ) ? user_token : '';
    if( token )
    {
        config = {
            headers: {
                'Authorization': `Bearer ${token}`,
                'Access-Control-Allow-Origin': '*',
                'Item-Type': item_type, // FIXME not received by api !?
            }
        };
    }

    return config;
}
// Component
finfoApi.component_action = function(identity, action, extras, success_callback, no_content_callback, error_callback, unauthorized_callback)
{
    // choose get || post && make url
    let promise;

    let unauthorized_msg;
    let no_content_msg;
    let error_msg;
    if( action === 'save' )// TODO user save-action_name
    {
        let url = `/component/${identity}/${action}`;

        promise = finfoApi.post(url, { datas: extras }, get_config() );

        no_content_msg = `Aucune donnée trouvée pour le composant: ${identity}!`;
        unauthorized_msg = `Accès non authorisé, session expirée`;
        error_msg = `Erreur lors de l'envoi des données pour le composant: ${identity}`;
    }
    else
    {
        let url = `/component/${identity}/${action}`;
        if( extras )
            if( extras instanceof Object )
                url = `/component/${identity}/${action}/${JSON.stringify(extras)}`;
            else
                url = `/component/${identity}/${action}/${extras}`;

        promise = finfoApi.get(url, get_config() );

        no_content_msg = `Aucune donnée trouvée pour le composant: ${identity}!`;
        unauthorized_msg = `Accès non authorisé, session expirée`;
        error_msg = `Erreur lors de la récupération des données pour le composant: ${identity}`;
    }

    // execute
    promise.then(res => {
        console.log( 'RES API');
        console.log( res.data );
        if( res.status === 200 )
        {
            let item = {};

            let datas = res.data.data;
            for (const [key, value] of Object.entries(datas))
            {
                item[key] = value;
            }
            success_callback( item );
        }
        else
        {
            no_content_callback(no_content_msg);
            console.info('no datas found for request domains');
        }
    })
        .catch(function (error) {
            console.log( error);
            if( error.response.status === 303 )
            {
                let url = error.response.data.data.redirect_to;
                let win = window.open(url, '_self');
                win.focus();
            }
            else if( error.response.status === 401 )
            {
                console.error(error);
                unauthorized_callback(unauthorized_msg);
            }
            else
            {
                console.error(error);
                error_callback(error_msg);
            }
        });
};

export default finfoApi;
