import React from 'react';
import './page.css';
import finfoApi from "../../libs/FinfoApi";
import {Spinner, Alert } from "react-bootstrap";


export default class Page extends React.Component
{
    constructor(props, identity, extras )
    {
        super(props);
        this.identity = identity;
        this.extras = extras;
        this.state = {
            page: {},
            alert: {
                is_active: false,
                msg: '',
                type: '',
            },
            is_loading: true
        };

        this._success_callback = this._success_callback.bind(this);
        this._no_content_callback = this._no_content_callback.bind(this);
        this._error_callback = this._error_callback.bind(this);
        this._unauthorized_callback = this._unauthorized_callback.bind(this);
    }

    componentDidMount()
    {
        if( this.identity ) // ask action to api
        {
            finfoApi.component_action(this.identity, 'get_page', this.extras, this._success_callback, this._no_content_callback, this._error_callback, this._unauthorized_callback);
        }
        else // page without identity
        {
            console.error('Set page identity!');
            this.setState({ is_loading: false } );
        }
    }

    render()
    {
        if( this.state.is_loading )
            return this._render_loading();
        else if( this.state.alert.is_active )
            return this._render_alert();
        else
            return this._render_page();
    }

    _render_loading()
    {
        return (
            <div className="d-flex justify-content-around align-items-center" id="page-loading">
                <div role="status" id="page-loading-spinner">
                    <Spinner animation={"border"}/>
                </div>
            </div>
        );
    }

    _render_alert()
    {
        return (
            <div className="flex-center" id="page-alert">
                <Alert
                    variant={"warning"}
                    id="page-alert-msg"
                >
                {this.state.alert.msg}
                </Alert>
            </div>
        );
    }

    _get_data(name)
    {
        let data;

        let properties = name.split('.'); // object.property1 become array
        if( this.state.page  )
        {
            let object = this.state.page;

            for (let i = 0; i < properties.length; i++) // iterate on object and childs
            {
                let property = properties[i];
                if (object.hasOwnProperty(property)) // object has property
                {
                    data = object[property]; // multi writes until final property
                    object = object[property]; // child become root object
                }
                else
                {
                    break; // last property not found for object so bye
                }
            }
        }

        return data;
    }

    _success_callback(page)
    {
        console.log( '-PAGE success');
        let merged_page = Object.assign(this.state.page, page);
        this.setState({page: page, is_loading: false}, () => console.log( this.state));
    }

    _no_content_callback(msg)
    {
        this.setState({
            page: {},
            alert: {is_active: true, msg: msg, type: 'warning'},
            is_loading: false
        });
    }

    _error_callback(msg)
    {
        this.setState({
            page: {},
            alert: {is_active: true, msg: msg, type: 'danger'},
            is_loading: false
        });
    }

    _unauthorized_callback(msg)
    {
        this.setState({
            page: {},
            alert: {is_active: true, msg: msg, type: 'warning'},
            is_loading: false
        });

        setTimeout(function(){
            let full_url = window.location;
            let base_url = full_url.protocol + "//" + full_url.host;
            window.location.href = base_url;
        }, 1500);
    }
}

Page.propTypes = {
};

Page.defaultProps = {
};
