import React from 'react';
import ReactDOM from 'react-dom';
import {Col, Row} from "react-bootstrap";
import Page from "./Page";
import Search from "../Items/Search";
import Content from "../Cards/Vocabularies";
import finfoApi from "../../libs/FinfoApi";

export default class Vocabulary extends Page
{
    constructor(props )
    {
        super(props, 'vocabulary' );
    }

    _render_page()
    {
        let vocabularies_grouped = this._get_data('vocabularies');
        let vocabularies_ungrouped = this._get_vocabularies_ungrouped(vocabularies_grouped);

        return (
            <Row className={"justify-content-md-center"}>
                <Col>
                    <Search
                        items={vocabularies_ungrouped}
                        type={"vocabulary"}
                    />
                    <Content
                        vocabularies={vocabularies_grouped}
                    />
                </Col>
            </Row>
        );
    }

    _handle_click_next(event)
    {
        finfoApi.component_action(this.identity, 'get_next', this.extras, this._success_callback, this._no_content_callback, this._error_callback, this._unauthorized_callback);
    }

    // Utils
    _get_vocabularies_ungrouped(vocabularies)
    {
        let vocabularies_ungrouped = [];
        for( let i in vocabularies)
            vocabularies_ungrouped = vocabularies_ungrouped.concat(vocabularies[i])

        return vocabularies_ungrouped;
    }
}

Vocabulary.propTypes = {
};

Vocabulary.defaultProps = {
};

const root = document.getElementById('vocabulary');
if (root) {
    ReactDOM.render(<Vocabulary/>, root);
}
