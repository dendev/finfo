import React from 'react';
import ReactDOM from 'react-dom';
import {Col, Row} from "react-bootstrap";
import Page from "./Page";
import Content from "../Items/Quiz";
import finfoApi from "../../libs/FinfoApi";

export default class Quiz extends Page
{
    constructor(props )
    {
        let entity_name = (window.quiz_entity_name) ? window.quiz_entity_name : 'test';
        let extras = {
          entity_name: entity_name,
          mode: 'random',
        };
        super(props, 'quiz', extras );

        this._handle_click_next = this._handle_click_next.bind(this);
    }

    _render_page()
    {
        let label = this._get_data('label');
        let question = this._get_data('question');
        let responses = this._get_data('responses');
        let end_url = this._get_data('end_url');

        console.log( responses);
        return (

            <Row className={"justify-content-md-center"}>
                <Col>
                    <Content
                        label={label}
                        question={question}
                        responses={responses}
                        handle_click_next={this._handle_click_next}
                        end_url={end_url}
                    />
                </Col>
            </Row>
        );
    }

    _handle_click_next(event)
    {
        finfoApi.component_action(this.identity, 'get_next', this.extras, this._success_callback, this._no_content_callback, this._error_callback, this._unauthorized_callback);
    }
}

Quiz.propTypes = {
};

Quiz.defaultProps = {
};

const root = document.getElementById('quiz');
if (root) {
    ReactDOM.render(<Quiz/>, root);
}
