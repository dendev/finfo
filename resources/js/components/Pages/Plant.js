import React from 'react';
import ReactDOM from 'react-dom';
import { Row, Col } from "react-bootstrap";
import Page from "./Page";
import Search from "../Items/Search";
import Content from "../Cards/Items";
import finfoApi from "../../libs/FinfoApi";

export default class Plant extends Page
{
    constructor(props )
    {
        super(props, 'plant', {'group_by': 'group_2'} );
    }

    _render_page()
    {
        let items_grouped = this._get_data('items');
        let items_ungrouped = this._get_items_ungrouped(items_grouped);

        return (
            <Row className={"justify-content-md-center"}>
                <Col>
                <Search
                    items={items_ungrouped}
                    type={"botanic"}
                />
                <Content
                    items={items_grouped}
                />
                </Col>
            </Row>
        );
    }

    _handle_click_next(event)
    {
        finfoApi.component_action(this.identity, 'get_next', this.extras, this._success_callback, this._no_content_callback, this._error_callback, this._unauthorized_callback);
    }

    // Utils
    _get_items_ungrouped(items)
    {
        let items_ungrouped = [];
        for( let i in items)
            items_ungrouped = items_ungrouped.concat(items[i])

        return items_ungrouped;
    }
}

Plant.propTypes = {
};

Plant.defaultProps = {
};

const root = document.getElementById('plant');
if (root) {
    ReactDOM.render(<Plant/>, root);
}
