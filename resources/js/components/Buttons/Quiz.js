import React from 'react';
import PropTypes from 'prop-types';
import Stop from "../Buttons/Stop";
import Flip from "../Buttons/Flip";
import Next from "../Buttons/Next";
import './quiz.css';


export default class Quiz extends React.Component
{
    constructor(props)
    {
        super(props);
    }

    render()
    {
        let stop = this._render_stop(this.props.stop_is_visible, this.props.total_correct, this.props.total_question);
        let flip = this._render_flip(this.props.flip_is_visible, this.props.handle_click_flip );
        let next = this._render_next(this.props.next_is_visible, this.props.handle_click_next );

        return (
            <div id="response-buttons">
                {stop}
                {flip}
                {next}
            </div>
        );
    }

    _render_stop(is_visible, total_correct, total_question)
    {
        if( is_visible )
        {
            return (
                <Stop
                    total_correct={total_correct}
                    total_question={total_question}
                    handle_close={this.props.handle_click_end}
                />
            );
        }
    }

    _render_flip(is_visible, handle_fct)
    {
        if( is_visible )
        {
            return (
                <Flip
                    handle_click={handle_fct}
                />
            );
        }
    }

    _render_next( is_visible, handle_fct )
    {
        if( is_visible )
        {
            return (
                <Next
                    handle_click={handle_fct}
                />
            );
        }
    }
}

Quiz.propTypes = {
    /**
     * show stop button or not
     */
    stop_is_visible: PropTypes.bool,
    /**
     * show flip button or not
     */
    flip_is_visible: PropTypes.bool,
    /**
     * show flip button or not
     */
    next_is_visible: PropTypes.bool,
    /**
     * total of correct responses
     */
    total_correct: PropTypes.number,
    /**
     * total of questions
     */
    total_question: PropTypes.number,
    /**
     * fct to do when flip button is clicked
     */
    handle_click_flip: PropTypes.func,
    /**
     * fct to do when next button is clicked
     */
    handle_click_next: PropTypes.func,
};

Quiz.defaultProps = {
    stop_is_visible: true,
    flip_is_visible: false,
    next_is_visible: false,
    total_correct: 0,
    total_question: 0,
    handle_click_flip: () => console.log( 'default flip'),
    handle_click_next: () => console.log( 'default next'),
};
