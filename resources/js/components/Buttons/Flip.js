import React from 'react';
import PropTypes from 'prop-types';
import { Button } from "react-bootstrap";


export default class Flip extends React.Component
{
    constructor(props)
    {
        super(props);
    }

    render()
    {
        let id = `${this.props.type}-buttons-flip`;

        return (
            <Button
                id={id}
                onClick={this.props.handle_click}
            >
                <i className={this.props.icon}> {this.props.label}</i>
            </Button>
        );
    }
}

Flip.propTypes = {
    /**
     *  Text to display
     */
    label: PropTypes.string,
    /**
     * Icon
     */
    icon: PropTypes.string,
    /**
     * context of usage
     */
    type: PropTypes.string,
    /**
     * fct to do when stop button is clicked
     */
    handle_click: PropTypes.func,
};

Flip.defaultProps = {
    label: 'Flip',
    icon: 'fas fa-redo',
    type: 'response',
    handle_click: () => console.log( 'default repeat'),
};
