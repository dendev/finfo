import React from 'react';
import PropTypes from 'prop-types';
import {Button, ModalBody, Modal, ModalTitle, ModalFooter} from "react-bootstrap";
import ModalHeader from "react-bootstrap/ModalHeader";


export default class Stop extends React.Component
{
    constructor(props)
    {
        super(props);

        this.state = {
            is_open: false,
        };

        this._handle_click = this._handle_click.bind(this);
        this._handle_close = this._handle_close.bind(this);
    }

    render()
    {
        let id = `${this.props.type}-buttons-stop`;

        return (
            <>
                <Button
                    id={id}
                    onClick={this._handle_click}
                >
                    <i className={this.props.icon}> {this.props.label}</i>
                </Button>

                <Modal show={this.state.is_open}
                    onHide={this._handle_close}
                >
                    <ModalHeader>
                        <ModalTitle>Fin du Quiz</ModalTitle>
                    </ModalHeader>

                    <ModalBody>
                        <div>
                            <p>Résultat: {this.props.total_correct} / {this.props.total_question}</p>
                        </div>
                    </ModalBody>


                    <ModalFooter>
                        <Button variant="primary" onClick={this._handle_close}>Ok</Button>
                    </ModalFooter>
                </Modal>
            </>
        );
    }

    _handle_click(event)
    {
        let is_open = this.state.is_open;
        is_open = ! is_open;
        this.setState({is_open: is_open});
    }

    _handle_close(event)
    {
        this._handle_click(event);
        this.props.handle_close();
    }

}

Stop.propTypes = {
    /**
     *  Text to display
     */
    label: PropTypes.string,
    /**
     * Icon
     */
    icon: PropTypes.string,
    /**
     * context of usage
     */
    type: PropTypes.string,
    /**
     * nb of correct response
     */
    total_correct: PropTypes.number,
    /**
     * nb of questions
     */
    total_question: PropTypes.number,
    /**
     * fct to do when stop button is clicked
     */
    handle_click: PropTypes.func,
};

Stop.defaultProps = {
    label: 'Stop',
    icon: 'fas fa-stop',
    type: 'response',
    total_correct:  0,
    total_question: 0,
    handle_close: () => console.log( 'default stop'),
};
