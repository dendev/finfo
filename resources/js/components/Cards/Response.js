import React from 'react';
import PropTypes from 'prop-types';
import {Button, Card} from "react-bootstrap";
import './response.css';


/**
 * Primary UI component for user interaction
 */
export default class Response extends React.Component
{
    constructor(props)
    {
        super(props);

        this.state = {
        };
    }

    render()
    {
        let title = this._render_title(this.props.question.label);
        let response = this._render_response(this.props.response, this.props.is_correct, this.props.is_answered);

        return (
            <Card  className="text-center shadow rounded" id="quiz-response">
                <Card.Header>
                    {title}
                </Card.Header>
                <Card.Body>
                        {response}
                </Card.Body>
            </Card>
        );
    }

    _render_title(data)
    {
        if( data.toLowerCase().startsWith('http') )
        {
            return (
                <Card.Img variant="top" src={data} id={"quiz-question-image"} />
            );
        }
        else
        {
            return (
                <Card.Title className="mb-2">{data}</Card.Title>
            );
        }
    }

    _render_response(response, is_correct, is_answered)
    {
        let random_msg = this._render_random_msg(is_correct);

        if( is_correct && ! is_answered )
        {
            return (
                <>
                    {random_msg}
                    <p className="quiz-response-correct">{response.label}</p>
                </>
            );
        }
        else
        {
            return (
                <>
                    {random_msg}
                    <p className="quiz-response-incorrect">{response.label}</p>
                </>
            )
        }
    }

    _render_random_msg(is_correct)
    {
        const icon_names = {
            'success': [ 'fas fa-check', 'fas fa-thumbs-up', 'fas fa-smile'],
            'error': [ 'fas fa-times', 'fas fa-thumbs-down', 'fas fa-frown'],
        };

        let random_msg = ( Math.floor(Math.random() * Math.floor(11)) );
        let random_icon = ( Math.floor(Math.random() * Math.floor(10)) );
        let icon_name_index = random_icon % 2;

        let icon_name = ( is_correct ) ? icon_names.success[icon_name_index] : icon_names.error[icon_name_index];
        let icon_intent = ( is_correct ) ? 'quiz-response-correct' : 'quiz-response-incorrect';
        let icon_classes = icon_name + ' ' + icon_intent + ' ' + 'mb-2';

        if( (random_msg % 3) === 0 )
        {
            return (
                <i className={icon_classes}></i>
            );
        }
    }

}

Response.propTypes = {
    /**
     * Question asked
     */
    question: PropTypes.object,
    /**
     * Correction response
     */
    response: PropTypes.object,
    /**
     * User answered correctly
     */
    is_correct: PropTypes.bool,
};

Response.defaultProps = {
    question: {},
    response: {},
    is_correct: false,
};
