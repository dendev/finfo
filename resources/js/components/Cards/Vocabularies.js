import React from 'react';
import PropTypes from 'prop-types';
import { Button, Card } from "react-bootstrap";
import './vocabularies.css';
import Element from "../Items/Element";

export default class Vocabularies extends React.Component
{
    constructor(props)
    {
        super(props);

        this.state = {
        };

        this._handle_click_print = this._handle_click_print.bind(this);
    }

    render()
    {
        let vocabularies = this._render_groups(this.props.vocabularies);

        return (
            <Card>
                <Card.Body>
                    {vocabularies}
                </Card.Body>
            </Card>
        );
    }

    _render_groups(datas)
    {
        let groups = [];
        for( const [letter, vocabularies] of Object.entries(datas) )
            groups.push( this._render_group(letter, vocabularies) );
        return groups;
    }

    _render_group(letter, datas)
    {
        let group_id = 'group-' + letter.toLowerCase();
        let total = datas.length;
        let actions = this._render_actions(group_id);

        let vocabularies = this._render_vocabularies(datas);

        return (
            <div key={letter} id={group_id} className={"mb-4"}>
                <Card.Title>{letter} ({total}) <span className={"vocabularies-title-actions"}>{actions}</span></Card.Title>
                <ul className="list-unstyled">
                    {vocabularies}
                </ul>
            </div>
        );
    }

    _render_actions(target_id) {
        let refs = [
            {icon: 'fas fa-print', click: this._handle_click_print}
        ];

        let actions = [];

        for (let i = 0; i < refs.length; i++) {
            actions.push(this._render_action(refs[i], target_id, i))
        }

        return actions;
    }

    _render_action(ref, target_id, key)
    {
        return (
            <Button variant={"light"} value={target_id} onClick={ref.click} key={key}>
                <i className={ref.icon + ' ' + "vocabularies-title-action"}/>
            </Button>
        );
    }

    _render_vocabularies(datas)
    {
        let vocabularies = [];
        for( let i = 0; i < datas.length; i++)
            vocabularies.push(this._render_vocabulary(datas[i], i) );
        return vocabularies;
    }

    _render_vocabulary(data, key)
    {
        return (
            <li key={key}>
                <Element
                    selected={data}
                    type={"vocabulary"}
                />
            </li>
        );
    }

    // Handles
    _handle_click_print(event)
    {
        let group_id = event.currentTarget.value;
        let letter = group_id.replace('group-', '').toUpperCase();
        let group = this.props.vocabularies[letter];

        this._to_print(letter, group);
    }

    // Utils
    _to_print(title, datas)
    {
        var a = window.open('', '', 'height=500, width=500');
        a.document.write('<html>');
        a.document.write(`<body > <h1>${title} (${datas.length})<br>`);
        for( let i = 0; i < datas.length; i++ )
        {
            let data = datas[i];
            a.document.write(`<h4>${data.label}</h4>`);
            a.document.write(`<p>${data.description}</p>`);
        }
        a.document.write('</body></html>');
        a.document.close();
        a.print();
    }
}

Vocabularies.propTypes = {
    /**
     * List of grouped vocabularies
     */
    vocabularies: PropTypes.object
};

Vocabularies.defaultProps = {
    vocabularies: {}
};
