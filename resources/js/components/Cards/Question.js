import React from 'react';
import PropTypes from 'prop-types';
import { Card, CardTitle, CardBody, CardText } from "react-bootstrap";
import './question.css';


/**
 * Primary UI component for user interaction
 */
export default class Question extends React.Component
{
    constructor(props)
    {
        super(props);

        this.state = {
            hover_choice: false,
        };

        this._hover_choice_on= this._hover_choice_on.bind(this);
        this._hover_choice_off = this._hover_choice_off.bind(this);
    }

    render()
    {
        let title = this._render_title(this.props.question.label);
        let choices = this._render_choices(this.props.responses);

        return (
            <Card className="text-center shadow rounded">
                <Card.Header>
                    {title}
                </Card.Header>
                <Card.Body>
                        <ul className="quiz-choices list-unstyled">
                            {choices}
                        </ul>
                </Card.Body>
            </Card>
        );
    }

    _render_title(data)
    {
        if( data.toLowerCase().startsWith('http') )
        {
            return (
                <Card.Img variant="top" src={data} id={"quiz-question-image"} />
            );
        }
        else
        {
            return (
                <Card.Title className="mb-2">{data}</Card.Title>
            );
        }
    }

    _render_choices(datas)
    {
        let choices  = [];
        for( let i = 0; i < datas.length; i++)
            choices.push(this._render_choice(datas[i], i));
        return choices;
    }

    _render_choice(data, key)
    {
        return (
            <li id={data.id}
                key={key}
                className="quiz-choices-choice"
                onMouseEnter={this._hover_choice_on}
                onMouseLeave={this._hover_choice_off}
                onClick={this.props.handle_click_response}
            >
                {data.label}
            </li>
        )
    }

    // Hover
    _hover_choice_on(event)
    {
        if( ! this.props.is_answered )
        {
            let classes = event.target.className;
            classes = classes.replace('quiz-choices-choice', 'quiz-choices-choice-hover')
            event.target.className = classes;
            this.setState({hover_choice: true})
        }
    }

    _hover_choice_off(event)
    {
        if( ! this.props.is_answered ) {
            let classes = event.target.className;
            classes = classes.replace('quiz-choices-choice-hover', 'quiz-choices-choice')
            event.target.className = classes;
            this.setState({hover_choice: false})
        }
    }
}

Question.propTypes = {
    /**
     * Question to ask
     */
    question: PropTypes.object,
    /**
     * Possibles responses
     */
    responses: PropTypes.array,
    /**
     * fct to do when response is clicked
     */
    handle_click_response: PropTypes.func,
};

Question.defaultProps = {
    question: [],
    responses: [],
    handle_click_response: () => console.log( 'default'),
};
