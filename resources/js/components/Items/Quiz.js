import React from 'react';
import PropTypes from 'prop-types';
import Front from '../Cards/Question';
import Back from '../Cards/Response';
import Buttons from '../Buttons/Quiz';
import './quiz.css';


/**
 * Primary UI component for user interaction
 */
export default class Quiz extends React.Component
{
    constructor(props)
    {
        super(props);

        this.state = {
            show_front: true,
            is_correct: false,
            is_answered: false,
            total_correct: 0,
            total_error: 0,
            total_question: 0,
        };

        this._correct_response_id = false;

        this._handle_click_response = this._handle_click_response.bind(this);
        this._handle_click_end = this._handle_click_end.bind(this);
        this._handle_click_flip = this._handle_click_flip.bind(this);
        this._handle_click_next = this._handle_click_next.bind(this);
    }

    render()
    {
        let headline = this._render_headline(this.props.label);
        let front = this._render_front(this.props.question, this.props.responses);
        let back = this._render_back(this.props.question, this.props.responses);
        let buttons = this._render_buttons();

        if( this.state.show_front)
            return (
                <>
                    {headline}
                    {front}
                    {buttons}
                </>);
        else
            return (
                <>
                    {headline}
                    {back}
                    {buttons}
                </>
            );
    }

    _render_headline(label)
    {
        return(
            <div id="quiz-headline">
                <span>{label}</span>
                <span id="quiz-headline-total"><span id="quiz-headline-total-correct">{this.state.total_correct}</span> / {this.state.total_question}</span>
            </div>
        )
    }

    _render_front(question, responses)
    {
        return (
            <Front
                question={question}
                responses={responses}
                handle_click_response={this._handle_click_response}
                is_answered={this.state.is_answered}
            />
        );
    }

    _render_back(question, responses)
    {
        let response = this._get_correct_response(responses);

        return (
            <Back
                question={question}
                response={response}
                is_correct={this.state.is_correct}
            />
        );
    }

    _render_buttons()
    {
        if( this.state.is_answered )
        { // back is showed ( more actions )
            return (
                <Buttons
                    stop_is_visible={true}
                    flip_is_visible={true}
                    next_is_visible={true}
                    total_correct={this.state.total_correct}
                    total_question={this.state.total_question}
                    handle_click_end={this._handle_click_end}
                    handle_click_flip={this._handle_click_flip}
                    handle_click_next={this._handle_click_next}
                />
            );
        }
        else
        {
            // front is showed ( less actions )
            return (
                <Buttons
                    stop_is_visible={true}
                    flip_is_visible={false}
                    next_is_visible={false}
                    total_correct={this.state.total_correct}
                    total_question={this.state.total_question}
                    handle_click_end={this._handle_click_end}
                    handle_click_flip={this._handle_click_flip}
                    handle_click_next={this.props.handle_click_next}
                />
            );
        }
    }

    // Handles
    _handle_click_response(event)
    {
        if( ! this.state.is_answered )
        {
            let is_correct = (parseInt(event.target.id) === this._correct_response_id);

            let total_correct = this.state.total_correct;
            let total_error = this.state.total_error;
            let total_question = this.state.total_question;

            if (is_correct)
                total_correct++;
            else
                total_error++;

            total_question++;

            this.setState({
                show_front: false,
                is_correct: is_correct,
                is_answered: true,
                total_correct: total_correct,
                total_error: total_error,
                total_question: total_question,
            });
        }
    }

    _handle_click_end(event)
    {
        window.location.href = this.props.end_url;
    }

    _handle_click_flip(event)
    {
        let show_front = (!this.state.show_front);
        this.setState({
            show_front: show_front,
        });
    }

    _handle_click_next(event)
    {
        this.setState({
            show_front: true,
            is_answered: false,
            is_correct: false,
        });

        this.props.handle_click_next(event);
    }

    //
    _get_correct_response(responses)
    {
        let response = false;
        for( let i = 0; i < responses.length && !response; i++)
        {
            if( responses[i].is_correct_response)
            {
                response = responses[i]
                this._correct_response_id = response.id;
            }
        }
        return response;
    }
}

Quiz.propTypes = {
    /**
     * Question
     */
    question: PropTypes.object,
    /**
     * Responses
     */
    responses: PropTypes.array,
    /**
     * Url to redirect at the end
     */
    end_url: PropTypes.string,
    /**
     * Action when click on next button
     */
    handle_click_next: PropTypes.func,
};

Quiz.defaultProps = {
    question: {},
    responses: [],
    end_url: '',
    handle_click_next: () => console.log( 'default')
};
