import React from 'react';
import PropTypes from 'prop-types';
import './element.css';
import {Button} from "react-bootstrap";
import VocabularyDialog from "../Dialogs/Vocabulary";
import ItemDialog from "../Dialogs/Item";


export default class Element extends React.Component
{
    constructor(props)
    {
        super(props);

        this.state = {
            is_open: false,
        };

        this._handle_click = this._handle_click.bind(this);
        this._handle_close = this._handle_close.bind(this);
    }

    render()
    {
        let dialog = this._render_dialog(this.props.type);

        return (
            <>
                <Button
                    id={this.props.selected.slug}
                    onClick={this._handle_click}
                    className={"list-element-button"}
                >
                    {this.props.selected.label}
                </Button>
                {dialog}
            </>
        );
    }

    _render_dialog(type)
    {
        if( type === 'vocabulary' )
        {
            return (
                <VocabularyDialog
                    isOpen={this.state.is_open}
                    onClose={this._handle_click}
                    onClosed={this._handle_close}
                    selected={this.props.selected}
                />
            )
        }
        else
        {
            return (
                <ItemDialog
                    isOpen={this.state.is_open}
                    onClose={this._handle_click}
                    onClosed={this._handle_close}
                    selected={this.props.selected}
                />
            )
        }
    }

    _handle_click(event)
    {
        let is_open = this.state.is_open;
        is_open = ! is_open;
        this.setState({is_open: is_open});
    }

    _handle_close(event)
    {
    }
}

Element.propTypes = {
    /**
     * Content of element selected
     */
    selected: PropTypes.object,
    /**
     * type of element selected
     */
    type: PropTypes.string,
};

Element.defaultProps = {
    selected:{},
    type:''
};
