import React from 'react';
import PropTypes from 'prop-types';
import "./extra.css";

export default class Extra extends React.Component {
    constructor(props) {
        super(props);

        this.state = {};
    }

    render()
    {
        let extras = this._render_extras(this.props.extras);

        return (
            <>
                {extras}
            </>
        );
    }

    _render_extras(datas)
    {
        let extras = [];

        if (datas)
        {
            for (let i = 0; i < datas.length; i++)
            {
                let extra = this._render_extra(datas[i], i);
                extras.push(extra);
            }
        }

        return extras;
    }

    _render_extra(data, key)
    {
        let fields = [];
        if (data)
        {
            let title = this._render_extra_title(data, key);

            for (let i = 0; i < data.fields.length; i++)
            {
                let field = this._render_field(data.fields[i], i);
                fields.push(field);
            }

            return (
                <div key={key}>
                    {title}
                    {fields}
                </div>
            );
        }
    }

    _render_extra_title(data, key)
    {
        if (data)
        {
            return(
                <>
                    <h1>{data.label}</h1>
                    <h2 className={"text-muted extra-title-subtitle"}>{data.description}</h2>
                </>
            );
        }
    }

    _render_field(data, i)
    {
        if( data )
        {
            if (data.type === 'text')
            {
                return this._render_field_text(data);
            }
        }
    }

    _render_field_text(data)
    {
        if( data )
        {
            return (<p>{data.value}</p>)
        }
    }
}

Extra.propTypes = {
    /**
     * infos
     */
    extras: PropTypes.array
};

Extra.defaultProps = {
    extras: []
};
