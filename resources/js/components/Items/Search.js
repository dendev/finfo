import React from 'react';
import PropTypes from 'prop-types';
import { Form } from "react-bootstrap";
import Autosuggest from 'react-autosuggest';
import VocabularyDialog from "../Dialogs/Vocabulary";
import ItemDialog from "../Dialogs/Item";
import './search.css'
import finfoApi from "../../libs/FinfoApi";


export default class Search extends React.Component
{
    constructor(props)
    {
        super(props);

        this.state = {
            value: '',
            suggestions: [],
            isLoading: false,
            item_selected: {},
            modal_is_open: false,
        };

        this.lastRequestId = null;


        //this._render_item = this._render_item.bind(this);

        //this._handle_search_select = this._handle_search_select.bind(this);
        //this._handle_search_click = this._handle_search_click.bind(this);
        this._handle_modal_click = this._handle_modal_click.bind(this);

        this._getSuggestionValue = this._getSuggestionValue.bind(this);
        this._loadSuggestions = this._loadSuggestions.bind(this);

      //  this._predicate = this._predicate.bind(this);
    }



    render()
    {
        let dialog = this._render_dialog(this.props.type);

        const { value, suggestions, isLoading } = this.state;
        const inputProps = {
            placeholder: "Recherche...",
            value,
            onChange: this._onChange
        };

        return (
            <>
            <Autosuggest
                suggestions={suggestions}
                onSuggestionsFetchRequested={this._onSuggestionsFetchRequested}
                onSuggestionsClearRequested={this._onSuggestionsClearRequested}
                getSuggestionValue={this._getSuggestionValue}
                renderSuggestion={this._renderSuggestion}
                inputProps={inputProps} />
                {dialog}
            </>
        );
    }

    _render_dialog(type)
    {
        if( type === 'vocabulary' )
        {
            return (
                <VocabularyDialog
                    isOpen={this.state.modal_is_open}
                    onClose={this._handle_modal_click}
                    selected={this.state.item_selected}
                />
            )
        }
        else
        {
            return (
                <ItemDialog
                    isOpen={this.state.modal_is_open}
                    onClose={this._handle_modal_click}
                    selected={this.state.item_selected}
                />
            )
        }
    }

    // Handles
    _handle_modal_click(event)
    {
        let modal_is_open = this.state.modal_is_open;
        modal_is_open = ! modal_is_open;
        this.setState({modal_is_open: modal_is_open});
    }

    // Autosugest component
    _onChange = (event, { newValue }) => {
        this.setState({
            value: newValue
        });
    };

    _onSuggestionsFetchRequested = ({ value }) => {
        this._loadSuggestions(value);
    };

    _onSuggestionsClearRequested = () => {
        this.setState({
            suggestions: []
        });
    };

    _getSuggestionValue(suggestion)
    {
        let suggestion_id = suggestion[1].id;// TMP name -> id
        let suggestion_selected = this._get_suggestion_by_id(suggestion_id);

        this.setState({
            item_selected: suggestion_selected,
            modal_is_open: true,
        });


        return suggestion[1].label;
    }

    _renderSuggestion(suggestion)
    {
        return (
            <span>{suggestion[1].label}</span>
        );
    }

    _loadSuggestions(value) {
        // Cancel the previous request
        if (this.lastRequestId !== null) {
            clearTimeout(this.lastRequestId);
        }

        this.setState({
            isLoading: true
        });

        // request
        let success_callback = (data) => {
            let suggestions;
            if( this.props.type === 'vocabulary')
                suggestions = Object.entries(data.vocabularies);
            else
                suggestions = Object.entries(data.items);

            this.setState({
                suggestions: suggestions,
                is_loading: false,
                lastRequestId: 1,
            });
        };

        let no_content_callback = function () {
            console.warn('#Api get_plant: no content');
        };
        let error_callback = function () {
            console.error('#Api get_plant: error');
        };
        let unauthorized_callback = function () {
            console.error('#Api get_plant: unauthorized');
        };

        if( this.props.type === 'vocabulary')
            finfoApi.component_action('vocabulary', 'search', value, success_callback, no_content_callback, error_callback, unauthorized_callback);
        else
            finfoApi.component_action('plant', 'search_plants', value, success_callback, no_content_callback, error_callback, unauthorized_callback);
    }

    // Utils
    _get_suggestion_by_id(suggestion_id)
    {
        let suggestion_found = false;

        let y = true;
        for( let i = 0; i < this.state.suggestions.length && y; i++)
        {
            let suggestion = this.state.suggestions[i][1];
            if( parseInt(suggestion.id) === parseInt(suggestion_id) )
            {
                y = false;
                suggestion_found = suggestion;
            }
        }

        return suggestion_found;
    }
}

Search.propTypes = {
    /**
     * Items of choice
     */
    items: PropTypes.array
};

Search.defaultProps = {
    items: [],
    type: 'vocabulary'
};

/* ----------- */
/*    Utils    */
/* ----------- */

// https://developer.mozilla.org/en/docs/Web/JavaScript/Guide/Regular_Expressions#Using_Special_Characters
function escapeRegexCharacters(str) {
    return str.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
}


