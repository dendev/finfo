import React from 'react';
import PropTypes from 'prop-types';
import ImageGallery from 'react-image-gallery';
import "react-image-gallery/styles/css/image-gallery.css";



export default class Gallery extends React.Component
{
    constructor(props)
    {
        super(props);
    }

    render()
    {
        return <ImageGallery items={this.props.images} />;
    }
}

Gallery.propTypes = {
    /**
     * List of images
     */
    images: PropTypes.array,
};

Gallery.defaultProps = {
    images: [],
};
