import React from 'react';
import PropTypes from 'prop-types';
import {ButtonGroup, Button, Divider} from "react-bootstrap";
import './search.css'


export default class Alphapagination extends React.Component
{
    constructor(props)
    {
        super(props);

        this.state = {
        };
    }

    render()
    {
        let elements = this._render_elements(this.props.elements);

        return (
                <ButtonGroup minimal={true} vertical={false}>
                    {elements}
                </ButtonGroup>
        );
    }

    _render_elements(datas)
    {
        let elements = [];
        for( let i = 0; i < datas.length; i++)
        {
            elements.push(this._render_element(datas[i], i));
        }
        return elements;
    }

    _render_element(data, key)
    {
        return(
            <>
                <Button key={ 'b-' + key}>
                    {data.label}
                </Button>
            </>
        )
    }
}

Alphapagination.propTypes = {
    /**
     * All elements of pagination
     */
    elements: PropTypes.array
};

Alphapagination.defaultProps = {
    elements: [],
};
