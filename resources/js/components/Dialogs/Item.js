import React from 'react';
import PropTypes from 'prop-types';
import {Button, Modal, ModalBody, ModalTitle, Spinner, Badge} from "react-bootstrap";
import './item.css';
import finfoApi from "../../libs/FinfoApi";
import Gallery from "../Items/Gallery";
import ModalHeader from "react-bootstrap/ModalHeader";
import Extra from "../Items/Extra";

export default class Item extends React.Component
{
    constructor(props)
    {
        super(props);

        this.state = {
            is_loading: true,
            item: {}
        }

        this._handle_click_print = this._handle_click_print.bind(this);
        this._handle_click_external = this._handle_click_external.bind(this);
        this._api_get_item = this._api_get_item.bind(this);
    }

    render()
    {
        let loading = this._render_loading();

        let images = this._render_images(this.state.item.images);
        let names = this._render_names(this.state.item.names);
        let classifications = this._render_classifications(this.state.item.classifications);
        let extras = this._render_extras(this.state.item.extras);
        let tags = this._render_tags(this.state.item.tags);
        let actions = this._render_actions();

        return (
            <Modal
                size={"lg"}
                show={this.props.isOpen}
                onHide={this.props.onClose}
                onEnter={this._api_get_item}
                key={this.props.selected.slug}
            >
                <ModalHeader closeButton>
                    <ModalTitle>{this.props.selected.label}</ModalTitle>
                </ModalHeader>

                <ModalBody className={"item-dialog-body"}>
                    {loading}
                    {images}
                    <div id={"item-dialog-body-tags"}>{tags}</div>
                    {names}
                    {classifications}
                    {extras}
                    <footer className="text-muted" id={"item-dialog-body-footer"}>
                        {actions}
                    </footer>
                    <p>{this.props.selected.description}</p>
                </ModalBody>
            </Modal>
        )
    }

    //  //  loading
    _render_loading()
    {
        if( this.state.is_loading)
        {
            return (
                <div className="flex-center" id="item-loading">
                    <div role="status" id="item-loading-spinner">
                        <Spinner size={150}/>
                    </div>
                </div>);
        }
    }

    //  //  images
    _render_images(datas)
    {
        if( datas && datas.length > 0 )
        {
            return (
                <Gallery
                    images={datas}
                />
            );
        }
    }

    //  // names
    _render_names(datas)
    {
        let names = [];

        names.push( this._render_names_title() );
        if( datas && ! this.state.is_loading )
        {
            for (let i = 0; i < datas.length; i++)
            {
                let name = this._render_name(datas[i], i);
                names.push(name);
            }
        }
        return names;
    }

    _render_names_title()
    {
        if( ! this.state.is_loading )
            return (<h1 key={'name'}>Nom(s)</h1>);
    }

    _render_name(data, key)
    {
        return (<p key={key}>{data.label} ({data.type})</p>);
    }

    //  //  classifications
    _render_classifications(datas)
    {
        let classifications = [];

        classifications.push( this._render_classifications_title() );
        if( datas && ! this.state.is_loading )
        {
            for (let i = 0; i < datas.length; i++)
            {
                let classification = this._render_classification(datas[i], i);
                classifications.push(classification);
            }
        }
        return classifications;
    }

    _render_classifications_title()
    {
        if( ! this.state.is_loading )
            return (<h1 key={'classification'}>Classification</h1>);
    }

    _render_classification(data, key)
    {
        return (<p key={key}>{data.label} ({data.level_fr})</p>);
    }

    //  // extras
    _render_extras(datas)
    {
        return (
            <Extra
                extras={datas}
                />
        )

    }

    // tags
    _render_tags(datas)
    {
        let tags = [];

        if( datas && ! this.state.is_loading )
        {
            for (let i = 0; i < datas.length; i++)
            {
                let tag = this._render_tag(datas[i], i);
                tags.push(tag);
            }
        }
        return tags;
    }

    _render_tag(data, key)
    {
        return (
            <Badge pill variant="primary" key={key}>
                {data.label}
            </Badge>
            )
    }

    //  //  actions
    _render_actions() {
        let refs = [
            {icon: 'fas fa-print', click: this._handle_click_print},
            {icon: 'fas fa-external-link-alt', click: this._handle_click_external}
        ];

        let actions = [];

        for (let i = 0; i < refs.length; i++) {
            actions.push(this._render_action(refs[i], this.state.item.id, i))
        }

        return actions;
    }

    _render_action(ref, target_id, key)
    {
        return (
            <Button variant={"light"} value={target_id} onClick={ref.click} key={key}>
                <i className={ref.icon + ' ' + "item-actions"}/>
            </Button>
        );
    }

    // Handles
    _handle_click_print(event)
    {
        this._to_print(this.state.item);
    }

    _handle_click_external(event)
    {
        let type = ( this.state.item.type.slug === 'botanic' ) ? 'plant' : this.state.item.type.slug; // FIXME use same slug !
        let url = process.env.MIX_APP_API_URL + '/component/' + type + '/get_' + type + '/' + this.state.item.slug;
        window.open(url, '_blank');
    }

    // Utils
    _to_print(item)
    {
        var a = window.open('', '', 'height=500, width=500');
        a.document.write('<html>');
        a.document.write(`<body > <h1>${item.label}<br>`);

        if( item.description )
            a.document.write(`<p>${item.description}</p>`);

        a.document.write('</body></html>');
        a.document.close();
        a.print();
    }

    //
    _api_get_item()
    {
        let selected = this.props.selected;
        let is_open = this.props.isOpen;

        if( is_open  )
        {
            let success_callback = (data) => {
                var item = data.items;
                this.setState({
                    is_loading: false,
                    item: item,
                })
            };

            let no_content_callback = function () {
                console.warn('#Api get_plant: no content');
            };
            let error_callback = function () {
                console.error('#Api get_plant: error');
            };
            let unauthorized_callback = function () {
                console.error('#Api get_plant: unauthorized');
            };

            finfoApi.component_action('plant', 'get_plant', selected.slug, success_callback, no_content_callback, error_callback, unauthorized_callback)
        }
    }
}

Item.propTypes = {
    /**
     * boolean to define if modal is open or not
     */
    isOpen: PropTypes.bool,
    /**
     * function to close modal
     */
    onClose: PropTypes.func,
    /**
     * item selected
     */
    selected: PropTypes.object,
};

Item.defaultProps = {
    isOpen: false,
    onClose: () => console.log(''),
    selected: {},
};
