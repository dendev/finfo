import React from 'react';
import PropTypes from 'prop-types';
import {Modal, ModalBody, ModalTitle} from "react-bootstrap";
import ModalHeader from "react-bootstrap/ModalHeader";

export default class Vocabulary extends React.Component
{
    constructor(props)
    {
        super(props);
    }

    render()
    {
        return (
            <Modal
                size={"lg"}
                show={this.props.isOpen}
                onHide={this.props.onClose}
                key={this.props.selected.slug}
            >
                <ModalHeader closeButton>
                    <ModalTitle>{this.props.selected.label}</ModalTitle>
                </ModalHeader>

                <ModalBody>
                    <p>{this.props.selected.description}</p>
                </ModalBody>
            </Modal>
        )
    }
}

Vocabulary.propTypes = {
    /**
     * boolean to define if modal is open or not
     */
    isOpen: PropTypes.bool,
    /**
     * function to close modal
     */
    onClose: PropTypes.func,
    /**
     * item selected
     */
    selected: PropTypes.object,
};

Vocabulary.defaultProps = {
    isOpen: false,
    onClose: () => console.log(''),
    selected: {},
};
